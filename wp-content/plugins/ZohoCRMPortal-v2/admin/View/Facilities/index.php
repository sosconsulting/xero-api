
<!-- Begin Page Content -->
<div class="container-fluid">

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col-md-11"><h6 class="m-0 font-weight-bold text-primary">Loan Facilities</h6></div>
               <!-- <div class="col-md-1 text-right"><i id="syncFacilities" data-module="facilities" data-entity="<?/*=$data['entity_id']*/?>" class="fas fa-sync text-primary" style="cursor: pointer"></i></div>-->
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered dataTable"  width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Financier</th>
                        <th>Product</th>
                        <th style="text-align: right">Amount</th>
                        <th>Rate Type</th>
                        <th style="text-align: right">Interest Rate</th>
                        <th>Repayments</th>
                        <th>Loan Expiry</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(isset($_SESSION['facilities'])) : ?>
                        <?php foreach($_SESSION['facilities'] as $key => $value): ?>
                            <?php foreach($value as $key1 => $facility): ?>
                                <tr>
                                    <td><?=$facility['Financier']['name']?></td>
                                    <td><?=$facility['Product']['name']?></td>
                                    <td style="text-align: right">$<?=number_format($facility['Loan_Amount'],2)?></td>
                                    <td style="text-align: right"><?=$facility['Interest_Rate_Type1']?></td>
                                    <td style="text-align: right"><?=$value['Base_Rate']?></td>
                                    <td><?=$facility['Repayment_Type']?></td>
                                    <td><?=isset($facility['Expiry_Date']) ? date_format(date_create($facility['Expiry_Date']), "d/m/Y") : ""?></td>
                                </tr>
                            <?php  endforeach;?>
                        <?php  endforeach;?>
                    <?php endif;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>