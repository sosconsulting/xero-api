<?php
session_start();

class App
{
    protected $controller = 'Entities';
    protected $method = 'index';
    protected $params = [];

    public function __construct()
    {
        # code...
        $url = $this->parseUrl();
        if(!$_SESSION['login'] && $url[1] != 'Login' ){
           header('Location: ' . BASE_URL ."?Login");
        }
        if( $url[1] == 'Login' && $_SESSION['login']){
            header('Location: ' . BASE_URL);
        }
        $this->processURL($url);
    }

    public function parseUrl()
    {
        return explode('/',filter_var(trim(str_replace( '?','' ,$_SERVER['REQUEST_URI']) ,'/'),FILTER_SANITIZE_URL));
    }

    public function processURL($url)
    {
        unset($url[0]);
        if($url[1] == "Group"){
            $url[1] = "Borrowers";
        }
        if(file_exists(ADMIN_URL. '/Controller/'. $url[1].'.php'))
        {
            $this->controller = $url[1];
            unset($url[1]);
        }
        else {
            if (!empty($url[1])){
                $this->controller = 'Unknown';
                $this->method = 'pageNotFound';
                unset($url[1]);
                unset($url[2]);
            }
        }

        require ADMIN_URL. '/Controller/'.$this->controller.'.php';
        $this->controller = new $this->controller;

        if(isset($url[2]))
        {
            if(method_exists($this->controller, $url[2]))
            {
                $this->method = $url[2];
                unset($url[2]);
            }
            else {
                $this->method = 'pageNotFound';
                unset($url[2]);
            }
        }

        $this->params = $url ? array_values($url) : [];

        call_user_func_array([$this->controller,$this->method], $this->params);
    }

    public function redirect()
    {

    }
}