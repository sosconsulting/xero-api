<?php

namespace SOSZohoXeroIntegration;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
        'region',
        'country',
        'abbreviation'
    ];
}
