<?php

namespace SOSZohoXeroIntegration\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ZohoToXeroSyncReport extends Mailable
{
    use Queueable, SerializesModels;
    private $attachmentFileName = null;

    /**
     * Create a new message instance.
     *
     * ZohoToXeroSyncReport constructor.
     * @param $attachmentFileName
     */
    public function __construct($attachmentFileName)
    {
        $this->attachmentFileName = $attachmentFileName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('sos.xerozohointegration.mailer@gmail.com', 'SOS Consulting')
            ->subject('Zoho to Xero Sync Report')
            ->markdown('mails.zohotoxerosyncreport')
            ->with([
                'name' => env('SYNC_REPORT_MAIN_RECIPIENT_NAME', 'SOS'),
            ])->attach(storage_path("app/{$this->attachmentFileName}"));
    }
}
