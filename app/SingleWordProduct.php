<?php

namespace SOSZohoXeroIntegration;

use Illuminate\Database\Eloquent\Model;

class SingleWordProduct extends Model
{
    protected $fillable = [
        'mapping_word',
        'zoho_product_id'
    ];
}
