<?php

namespace SOSZohoXeroIntegration\SyncHelpers;

require 'vendor/autoload.php';

use SOSZohoXeroIntegration;
use XeroPHP\Models\Accounting\Contact;
use XeroPHP\Models\Accounting\Invoice;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class XeroToZohoAccountsPayable
{
    private $focus = '';
    private $log = [];
    private $supplierEntityId = false;
    private $supplierExist = false;
    private $contactExist = false;
    private $invoiceExist = false;
    private $xeroInvoice = null;
    private $xero = null;
    private $batchNumber = null;

    public function processData($data, $xero)
    {
        $this->focus = $data;
        $this->xero = $xero;
        $this->batchNumber = $data->batch_number;

        $accPayable = json_decode($data->record_details);

        if ($accPayable->Status == 'DELETED' || $accPayable->Status == 'VOIDED') {
            $this->log['info']['invoice'][] = 'Xero Invoice is already Deleted/Voided in Xero Side';
            $this->markZohoInvoiceAsDeletedOrVoided($accPayable);
            return $this->log;
        }

        \ZCRMRestClient::initialize();

        $supplierId = '';
        // Check for Supplier match in Zoho CRM by Xero Invoice Number and Name
        $contactByContactId = $this->searchBySupplierId($accPayable);
        if (!empty($contactByContactId)) {
            $this->supplierExist = true;
            $supplierId = $contactByContactId;
        } else {
            $contactByContactName = $this->searchBySupplierName($accPayable);
            if (!empty($contactByContactName)) {
                $this->supplierExist = true;
                $supplierId = $contactByContactName;
            } else {
                $supplierId = $this->createSupplierRecord($accPayable->Contact);
                if ($supplierId == 'Error in creating supplier') {
                    return $this->log;
                }
            }
        }

        $xeroContactQuery = DB::table('xero_contacts')->where('batch_number', $this->batchNumber)->where('contact_id', $accPayable->Contact->ContactID)->first();
        $xeroSupplier = !empty($xeroContactQuery->record_details) ? json_decode($xeroContactQuery->record_details) : '';

        $supplierFocus = \ZCRMRecord::getInstance("Vendors", $supplierId);
        $this->supplierEntityId = $supplierFocus->getEntityId();

        if ($this->supplierExist) {
            $supplierQuery = DB::table('ap_xero_to_zoho_zoho_vendors')->where('batch_number', $this->batchNumber)->where('xero_contact_id', $accPayable->Contact->ContactID)->first();
            if (!empty($supplierQuery->id)) {
            $supplier = !empty($supplierQuery->record_details) ? json_decode($supplierQuery->record_details) : '';

            if (isset($supplier->Disable_Invoice_Integration) && $supplier->Disable_Invoice_Integration) {
                $this->log['error']['supplier'][] = "Supplier is Invoice integration Disabled with ID: {$supplier->id}";
                return $this->log;
            }
        } else {
            $supplierQuery = \ZCRMModule::getInstance('Vendors')->getRecord($this->supplierEntityId);
            $supplier = $supplierQuery->getResponseJSON();
                if (isset($supplier['data'][0]['Disable_Invoice_Integration']) && $supplier['data'][0]['Disable_Invoice_Integration']) {
                    $this->log['error']['supplier'][] = "Supplier is Invoice integration Disabled with ID: {$supplier['data'][0]['id']}";
                    return $this->log;
                }
            }
        } else {
            $supplierQuery = \ZCRMModule::getInstance('Vendors')->getRecord($this->supplierEntityId);
            $supplier = $supplierQuery->getResponseJSON();
            if (isset($supplier['data'][0]['Disable_Invoice_Integration']) && $supplier['data'][0]['Disable_Invoice_Integration']) {
                $this->log['error']['supplier'][] = "Supplier is Invoice integration Disabled with ID: {$supplier['data'][0]['id']}";
                return $this->log;
            }
        }

        if (!isset($supplierId)) {
            $this->log['error']['supplier'][] = "Supplier record is not existing in Supplier module";
            return $this->log;
        }

        $contact = $this->processContactDetails($xeroSupplier);

        if (empty($contact['email']) && !empty($contact['id'])) {
            $this->log['error']['contact'][] = "Contacts email address is missing in Xero with ContactID : {$accPayable->Contact->ContactID}";
        }

        if(!empty($contact['id']) && !empty($contact['email'])) {
            // link contact to Supplier only if the supplier is newly created or resync has been triggered
            if (empty($this->supplierExist) || !empty($this->focus->ap_vendor_id)) {
                $supplierFocus->setFieldValue('Primary_Contact', !empty($contact['id']) ? $contact['id'] : '');
                try {
                    $updateRecordResponse = $supplierFocus->update();
                    if ($updateRecordResponse->getStatus() == 'success' && $updateRecordResponse->getMessage() == 'record updated') {
                        $this->log['info']['supplier'][] = "Primary Contact is successfully  added/updated to Supplier Record with id: {$supplierId}";
                    }
                } catch (\Exception $e) {
                    $this->log['fatal_error']['supplier'][] = "Encountered an error while setting the primary contact for Supplier, see details: {$e->getMessage()}";
                }
            }
        }

        // Create or Update zoho AP Invoice
        $this->xeroInvoice = $this->xero->loadByGUID(Invoice::class, $accPayable->InvoiceID);

        $apInvoiceId = $this->processInvoiceDetails($accPayable, $this->supplierEntityId);

        // Link attachment(s) to AP Invoices
        if (!empty($apInvoiceId) && !empty($accPayable->HasAttachments)) {
            $this->linkAttachmentsToApInvoices($apInvoiceId);
        }

        return $this->log;
    }

    private function searchBySupplierId($accPayable)
    {
        if(!empty($this->focus->ap_vendor_id)){
            return $this->focus->ap_vendor_id;
        }

        $vendorQuery = DB::table('ap_xero_to_zoho_zoho_vendors')->where('batch_number', $this->batchNumber)->where('xero_contact_id', $accPayable->Contact->ContactID)->first();
        if(!empty($vendorQuery->vendor_id)){
            return $vendorQuery->vendor_id;
        }

        return false;
    }

    private function searchBySupplierName($accPayable)
    {
        if(!empty($this->focus->ap_vendor_id)){
            return $this->focus->ap_vendor_id;
        }

        $vendorQuery = DB::table('ap_xero_to_zoho_zoho_vendors')->where('batch_number', $this->batchNumber)->where('vendor_name', $accPayable->Contact->Name)->first();
        if(!empty($vendorQuery->vendor_id)){
            return $vendorQuery->vendor_id;
        }

        return false;
    }


    private function createSupplierRecord($supplier)
    {
        $supplierId = '';
        $xeroSupplierQuery = $this->xero->loadByGUID(Contact::class, $supplier->ContactID);
        $xeroSupplier = $this->parseZohoData($xeroSupplierQuery);

        if (empty($supplier->ContactID)) {
            $this->log['fatal_error']['supplier'][] = 'Fatal: Xero Supplier not Existing in Xero. Will not proceed to sync';
            return 'Error in creating supplier';
        }

        $defaultPhone = isset($xeroSupplier->Phones) ? $this->parsePhoneDetails($xeroSupplier->Phones, 'DEFAULT') : [];
        $poBoxAddress = isset($xeroSupplier->Address) ? $this->parseAddress($xeroSupplier->Addresses, 'POBOX') : [];

        $newSupplier = \ZCRMRecord::getInstance("Vendors", null);
        $newSupplier->setFieldValue('Xero_Contact_ID', !empty($xeroSupplier->ContactID) ? $xeroSupplier->ContactID : '');
        $newSupplier->setFieldValue('Vendor_Name', !empty($xeroSupplier->Name) ? $xeroSupplier->Name : '');
        $newSupplier->setFieldValue('Website', !empty($xeroSupplier->Website) ? $xeroSupplier->Website : '');
        $newSupplier->setFieldValue('Phone', !empty($defaultPhone) ? $defaultPhone : '');
        $newSupplier->setFieldValue('Street', !empty($poBoxAddress->AddressLine1) ? $poBoxAddress->AddressLine1 : '');
        $newSupplier->setFieldValue('City', !empty($poBoxAddress->City) ? $poBoxAddress->City : '');
        $newSupplier->setFieldValue('State', !empty($poBoxAddress->Region) ? $poBoxAddress->Region : '');
        $newSupplier->setFieldValue('Country', !empty($poBoxAddress->Country) ? $poBoxAddress->Country : '');
        $newSupplier->setFieldValue('Zip_Code', !empty($poBoxAddress->PostalCode) ? $poBoxAddress->PostalCode : '');
        try {
            $createRecordResponse = $newSupplier->create();
            if ($createRecordResponse->getStatus() == 'success' && $createRecordResponse->getMessage() == 'record added') {
                $supplierDetails = $createRecordResponse->getResponseJSON();
                $supplierId = $supplierDetails['data'][0]['details']['id'];
                DB::table('xero_invoice_queue')->where('id', $this->focus->id)->update(['ap_vendor_id' => $supplierId]);
                $this->log['info']['supplier'][] = "New Supplier Record is created with id: {$supplierId}";
            }
        } catch (\Exception $e) {
            $this->log['fatal_error']['supplier'][] = "Fatal: Encountered error while creating Supplier. Will not proceed to sync, see Details: {$e->getMessage()}";
            return 'Error in creating supplier';
        }

        return $supplierId;
    }

    private function processContactDetails($xeroSupplier){
        if(!empty($this->focus->ap_contact_id)){
            $contactId = $this->focus->ap_contact_id;
            $contactQuery = \ZCRMModule::getInstance('Contacts')->getRecord($contactId);
            $contactDetails = $contactQuery->getResponseJSON();
            $contact['id']= $contactId;
            $contact['email'] = !empty($contactDetails['data']['0']['Email']) ? $contactDetails['data']['0']['Email'] : '';

            return $contact;
        }

        if(empty($xeroSupplier)){
            return false;
        }

        $zohoContactQuery = null;
        if(!empty($xeroSupplier->EmailAddress)){
            $zohoContactQuery = DB::table('ap_xero_to_zoho_zoho_contacts')->where('batch_number', $this->batchNumber)->where('email_address', $xeroSupplier->EmailAddress)->first();
        }

        $contact = [];
        if (!empty($zohoContactQuery->contact_id)) {
            $this->contactExist = true;
            $contact['id']= $zohoContactQuery->contact_id;
            $contact['email'] = $zohoContactQuery->email_address;
        } else {
            $directPhone = $this->parsePhoneDetails($xeroSupplier->Phones, 'DDI');
            $mobile = $this->parsePhoneDetails($xeroSupplier->Phones, 'MOBILE');

            $newContact = \ZCRMRecord::getInstance("Contacts", null);
            $newContact->setFieldValue('First_Name', !empty($xeroSupplier->FirstName) ? $xeroSupplier->FirstName : '');
            $newContact->setFieldValue('Last_Name', !empty($xeroSupplier->LastName) ? $xeroSupplier->LastName : '');
            $newContact->setFieldValue('Email', !empty($xeroSupplier->EmailAddress) ? $xeroSupplier->EmailAddress : '');
            $newContact->setFieldValue('Mobile', !empty($mobile) ? $mobile : '');
            $newContact->setFieldValue('Phone', !empty($directPhone) ? $directPhone : '');

            try {
                $createRecordResponse = $newContact->create();

                if ($createRecordResponse->getStatus() == 'success' && $createRecordResponse->getMessage() == 'record added') {
                    $contactDetails = $createRecordResponse->getResponseJSON();
                    $contact['id'] = $contactDetails['data'][0]['details']['id'];
                    $contact['email'] = !empty($xeroSupplier->EmailAddress) ? $xeroSupplier->EmailAddress : '';
                    DB::table('xero_invoice_queue')->where('id', $this->focus->id)->update(['ap_contact_id' => $contact['id']]);
                    $this->log['info']['contact'][] = "New Contacts Record is created with id: {$contactDetails['data'][0]['details']['id']}";
                }
            } catch (\Exception $e) {
                $this->log['fatal_error']['contact'][] = "Encountered an api error while creating the contact record, see details: {$e->getMessage()}";
            }
        }

        return $contact;
    }

    private function processInvoiceDetails($accPayable, $supplierId){
        $invoiceId = '';

        if (!empty($this->focus->ap_po_receipt_id)) {
            $this->invoiceExist = true;
            $invoiceId = $this->focus->ap_po_receipt_id;
        } else {
        // Search for existing AP invoice
        $invoiceQuery = DB::table('ap_xero_to_zoho_zoho_po_receipts')->where('batch_number', $this->batchNumber)->where('invoice_number', $accPayable->InvoiceNumber)->first();

        if(!empty($invoiceQuery->po_receipts_id)){
            $this->invoiceExist = true;
            $invoiceId = $invoiceQuery->po_receipts_id;
        }else{
            $lastDatePaid = $this->queryForLastDatePaid();

            $newInvoice = \ZCRMRecord::getInstance("PO_Receipts", null);

            $newInvoice->setFieldValue('Name', !empty($accPayable->InvoiceNumber) ? $accPayable->InvoiceNumber : '');
            $newInvoice->setFieldValue('Invoice_Number', !empty($accPayable->InvoiceNumber) ? $accPayable->InvoiceNumber : '');
            $newInvoice->setFieldValue('Currency', !empty($accPayable->CurrencyCode) ? $accPayable->CurrencyCode : '');
            $newInvoice->setFieldValue('Invoice_Supplier', !empty($supplierId) ? $supplierId : '');
            $newInvoice->setFieldValue('Invoice_Date', !empty($accPayable->Date) ? $accPayable->Date : '');
            $newInvoice->setFieldValue('Due_Date', !empty($accPayable->DueDate) ? $accPayable->DueDate : '');
            $newInvoice->setFieldValue('Total_Excluding_GST', !empty($accPayable->SubTotal) ? $accPayable->SubTotal : '');
            $newInvoice->setFieldValue('Total_Including_GST', !empty($accPayable->Total) ? $accPayable->Total : '');
            $newInvoice->setFieldValue('GST_Value', !empty($accPayable->TotalTax) ? $accPayable->TotalTax : '');
            $newInvoice->setFieldValue('Amount_Paid', !empty($accPayable->AmountPaid) ? $accPayable->AmountPaid : '');
            $newInvoice->setFieldValue('Invoice_Status_Xero', $this->parseInvoiceStatus($accPayable->Status));
            $newInvoice->setFieldValue('Last_Date_Paid', !empty($lastDatePaid) ? $lastDatePaid : '');
            try {
                $createRecordResponse = $newInvoice->create();
                if ($createRecordResponse->getStatus() == 'success' && $createRecordResponse->getMessage() == 'record added') {
                    $invoiceDetails = $createRecordResponse->getResponseJSON();
                    $invoiceId = $invoiceDetails['data'][0]['details']['id'];
                        DB::table('xero_invoice_queue')->where('id', $this->focus->id)->update(['ap_po_receipt_id' => $invoiceId]);
                    $this->log['info']['invoice'][] = "New AP Invoice Record is created with id: $invoiceId";
                    DB::table('zoho_invoice_update_timestamps')->insert(['invoice_number' => $accPayable->InvoiceNumber,]);
                }
            } catch (\Exception $e) {
                    $this->log['fatal_error']['invoice'][] = "Encountered and api error while creating the AP Invoice, see details: {$e->getMessage()}";
                DB::table('zoho_invoice_update_timestamps')->insert(['invoice_number' => $accPayable->InvoiceNumber,]);
                return false;
            }
        }
        }

        // Update Invoice if existing
        if ($this->invoiceExist) {
            $invoiceQuery = \ZCRMModule::getInstance('PO_Receipts')->getRecord($invoiceId);
            $invoiceDetails = $invoiceQuery->getResponseJSON();

            $lastDatePaid = $this->queryForLastDatePaid();

            $existingInvoice = \ZCRMRecord::getInstance("PO_Receipts", $invoiceId);
            $existingInvoice->setFieldValue('Name', !empty($accPayable->InvoiceNumber) ? $accPayable->InvoiceNumber : '');
            $existingInvoice->setFieldValue('Invoice_Number', !empty($accPayable->InvoiceNumber) ? $accPayable->InvoiceNumber : '');
            $existingInvoice->setFieldValue('Currency', !empty($accPayable->CurrencyCode) ? $accPayable->CurrencyCode : '');
            $existingInvoice->setFieldValue('Invoice_Date', !empty($accPayable->Date) ? $accPayable->Date : '');
            $existingInvoice->setFieldValue('Due_Date', !empty($accPayable->DueDate) ? $accPayable->DueDate : '');
            $existingInvoice->setFieldValue('Total_Excluding_GST', !empty($accPayable->SubTotal) ? $accPayable->SubTotal : '');
            $existingInvoice->setFieldValue('Total_Including_GST', !empty($accPayable->Total) ? $accPayable->Total : '');
            $existingInvoice->setFieldValue('GST_Value', !empty($accPayable->TotalTax) ? $accPayable->TotalTax : '');
            $existingInvoice->setFieldValue('Amount_Paid', !empty($accPayable->AmountPaid) ? $accPayable->AmountPaid : '');
            $existingInvoice->setFieldValue('Invoice_Status_Xero', $this->parseInvoiceStatus($accPayable->Status));
            $existingInvoice->setFieldValue('Last_Date_Paid', !empty($lastDatePaid) ? $lastDatePaid : '');
            if (empty($invoiceDetails['data'][0]['Invoice_Supplier']['id']) || $invoiceDetails['data'][0]['Invoice_Supplier']['id'] != $supplierId) {
                $existingInvoice->setFieldValue('Invoice_Supplier', !empty($supplierId) ? $supplierId : '');
            }

            try {
                $updateAPInvoiceResponse = $existingInvoice->update();

                if ($updateAPInvoiceResponse->getStatus() == 'success' && $updateAPInvoiceResponse->getMessage() == 'record updated') {
                    DB::table('xero_invoice_queue')->where('id', $this->focus->id)->update(['ap_po_receipt_id' => $invoiceId]);
                    $this->log['info']['invoice'][] = "AP Invoices Record is updated with id: $invoiceId";
                } else {
                    $this->log['error']['invoice'][] = "AP Invoices Record is not updated with id: $invoiceId";
                }
                DB::table('zoho_invoice_update_timestamps')->insert(['invoice_number' => $accPayable->InvoiceNumber,]);
            } catch (\Exception $e) {
                $this->log['fatal_error']['invoice'][] = "Encountered and api error while updating an existing AP Invoice, see details: {$e->getMessage()}";
                DB::table('zoho_invoice_update_timestamps')->insert(['invoice_number' => $accPayable->InvoiceNumber,]);
            }
        }

        return $invoiceId;
    }

    private function linkAttachmentsToApInvoices($apInvoiceId){
        $attachments = $this->xeroInvoice->getAttachments();

        $zohoExistingAttachments = [];
        $fileNamesForDelete = [];

        if(empty($attachments)){
            return false;
        }

        $zohoInvoice = \ZCRMRecord::getInstance('PO_Receipts', $apInvoiceId);

        try {
            // There is existing attachment(s) so check first if there is an attachment
            // with same filename, if there is then don't create attachment if none then create new attachement
            $zohoApAttachmentsQuery = $zohoInvoice->getAttachments(1, 50);
            $zohoApAttachments = $zohoApAttachmentsQuery->getData();
            foreach ($zohoApAttachments as $attachmentInfo) {
                $fileName = $attachmentInfo->getFileName();

                $zohoExistingAttachments[] = $fileName;
            }

            foreach ($attachments as $attachment) {
                $attachmentFileName = $attachment->getFileName();

                if (in_array($attachmentFileName, $zohoExistingAttachments)) {
                    continue;
                } else {
                    try {
                        Storage::disk('local')->put($attachment->getFileName(), $attachment->getContent());
                        $zohoInvoiceAddAttachment = $zohoInvoice->uploadAttachment('storage/app/' . $attachmentFileName);
                        $addAttachmentDetails = $zohoInvoiceAddAttachment->getResponseJSON();
                        if ($zohoInvoiceAddAttachment->getStatus() == 'success') {
                            $this->log['info']['attachment'][] = "New Attachment[{$addAttachmentDetails['data'][0]['details']['id']}] is added to AP Invoice[$apInvoiceId]";
                        }
                    } catch (\Exception $e) {
                        $this->log['fatal_error']['attachment'][] = "New Attachment with xero attachment filename [$attachmentFileName] is NOT linked to AP Invoice[$apInvoiceId], see details: {$e->getMessage()}";
                    }
                }
                $fileNamesForDelete[] = $attachmentFileName;
            }
        } catch (\Exception $e) {
            // There is NO existing attachments so create immediately the new attachments
            if ($e->getMessage() == 'No Content') {
                foreach ($attachments as $attachment) {
                    $attachmentFileName = $attachment->getFileName();

                    if (in_array($attachmentFileName, $zohoExistingAttachments)) {
                        continue;
                    } else {
                        try {
                            Storage::disk('local')->put($attachment->getFileName(), $attachment->getContent());
                            $zohoInvoiceAddAttachment = $zohoInvoice->uploadAttachment('storage/app/' . $attachmentFileName);
                            $addAttachmentDetails = $zohoInvoiceAddAttachment->getResponseJSON();
                            if ($zohoInvoiceAddAttachment->getStatus() == 'success') {
                                $this->log['info']['attachment'][] = "New Attachment[{$addAttachmentDetails['data'][0]['details']['id']}] is added to AP Invoice[$apInvoiceId]";
                            }
                        } catch (\Exception $e) {
                            $this->log['fatal_error']['attachment'][] = "New Attachment with xero attachment filename [$attachmentFileName] is NOT linked to AP Invoice[$apInvoiceId], see details: {$e->getMessage()}";
                        }
                    }

                    $fileNamesForDelete[] = $attachmentFileName;
                }
            }
        }

        if (!empty($fileNamesForDelete)) {
            Storage::delete($fileNamesForDelete);
        }
    }

    private function parseZohoData($record)
    {
        $jsonEncodedData = json_encode($record);
        return json_decode($jsonEncodedData);
    }

    private function parsePhoneDetails($phone, $detailType){
        foreach($phone  as $phoneDetail){
            if($phoneDetail->PhoneType == $detailType) {
                if(!empty($phoneDetail->PhoneCountryCode) || !empty($phoneDetail->PhoneAreaCode) || !empty($phoneDetail->PhoneNumber)){
                    $countryCode = (isset($phoneDetail->PhoneCountryCode)) ? $phoneDetail->PhoneCountryCode : '';
                    $areaCode = (isset($phoneDetail->PhoneAreaCode)) ? $phoneDetail->PhoneAreaCode : '';
                    $phoneNumber = (isset($phoneDetail->PhoneNumber)) ? $phoneDetail->PhoneNumber  : '';
                    $areaCodeSeparator = !empty($countryCode) ? '-' : '';
                    $phoneNumSeparator = (!empty($countryCode) || !empty($areaCode)) ? '-' : '';
                    $fullPhoneDetails= (!empty($countryCode) ? $countryCode : '') . (!empty($areaCode) ? $areaCodeSeparator . $areaCode  : '') . (!empty($phoneNumber) ? $phoneNumSeparator . $phoneNumber  : '');
                    return $fullPhoneDetails;
                }
            }
        }

        return '';
    }

    private function parseAddress($address, $addressType)
    {
        foreach($address as $addressDetail){
            if(!empty($addressDetail->AddressType) && $addressDetail->AddressType == $addressType){
                return $addressDetail;
            }
        }

        return [];
    }


    private function queryForLastDatePaid()
    {
        if(empty($this->xeroInvoice)){
            return '';
        }

        $lastPaidDate = '';
        $getPayments = $this->xeroInvoice->getPayments();
        $paymentsArray = $this->parseZohoData($getPayments);

        if (!empty($paymentsArray)) {
            $paymentDates = [];
            foreach ($paymentsArray as $payment) {
                if (empty($payment->Date)) {
                    continue;
                }
                $paymentDates[] = $payment->Date;
            }

            if (!empty($paymentDates)) {
                $max = max(array_map('strtotime', $paymentDates));
                $lastPaidDate = date('Y-m-d', $max);
            }
        }

        return $lastPaidDate;
    }


    private function parseInvoiceStatus($invoiceStatus)
    {
        switch ($invoiceStatus) {
            case 'DRAFT':
                $status = 'Draft';
                break;
            case 'SUBMITTED':
                $status = 'Awaiting Approval';
                break;
            case 'AUTHORISED':
                $status = 'Awaiting Payment';
                break;
            case 'PAID':
                $status = 'Paid in Full';
                break;
            case 'VOIDED':
                $status = 'Voided';
                break;
            default:
                $status = '';
                break;
        }

        return $status;
    }

    private function markZohoInvoiceAsDeletedOrVoided($accPayable){
        $invoiceQuery = DB::table('ap_xero_to_zoho_zoho_po_receipts')->where('batch_number', $this->batchNumber)->where('invoice_number', $accPayable->InvoiceNumber)->first();

        if(!empty($invoiceQuery->po_receipts_id)){
            $invoiceId = $invoiceQuery->po_receipts_id;
        }

        if (empty($invoiceId)) {
            return false;
        }

        try {
            $status = $this->parseInvoiceStatus($accPayable->Status);
            $existingInvoice = \ZCRMRecord::getInstance("PO_Receipts", $invoiceId);
            $existingInvoice->setFieldValue('Invoice_Status_Xero', $status);
            $existingInvoice->update();
            DB::table('zoho_invoice_update_timestamps')->insert(['invoice_number' => $accPayable->InvoiceNumber]);
        } catch (\Exception $e) {
            $this->log['fatal_error']['invoice'][] = "Error encountered in updating the invoice to Voided/Deleted: {$e->getMessage()}";
        }

        return true;
    }
}
