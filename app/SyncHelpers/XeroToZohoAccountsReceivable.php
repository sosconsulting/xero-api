<?php

namespace SOSZohoXeroIntegration\SyncHelpers;

require 'vendor/autoload.php';

use SOSZohoXeroIntegration;
use XeroPHP\Models\Accounting\Invoice;
use Illuminate\Support\Facades\DB;

class XeroToZohoAccountsReceivable
{
    private $focus = '';
    private $xeroInvoiceInstance = null;
    private $batchNumber = null;
    private $xero = null;
    private $log = [];
    private $companyExist = false;
    private $companyEntityId = false;
    private $contactExist = false;
    private $invoiceExist = false;
    private $singleWordTax = false;
    private $singleWordDiscount = false;

    /**
     * Process data from xero
     * @param $data
     * @param $xero
     * @return array
     * @throws \ZCRMException
     */
    public function processData($data, $xero)
    {
        $this->focus = $data;
        $this->xero = $xero;
        $this->batchNumber = $data->batch_number;

        $accReceivable = json_decode($data->record_details);

        if ($accReceivable->Status == 'DELETED' || $accReceivable->Status == 'VOIDED') {
            $this->log['info']['invoice'][] = 'Xero Invoice is already Deleted/Voided in Xero Side';
            $this->markZohoInvoiceAsDeletedOrVoided($accReceivable);
            return $this->log;
        }

        \ZCRMRestClient::initialize();

        // Check for Company match in Zoho CRM by Xero Contact ID and Contact Name
        $contactByContactId = $this->searchByXeroContactId($accReceivable);
        if (!empty($contactByContactId) && $contactByContactId != 'No match found' && $contactByContactId != 'API Error') {
            $this->companyExist = true;
            $companyId = $contactByContactId;
        } else {
            if ($contactByContactId == 'API Error') {
                return $this->log;
            }
            $contactByContactName = $this->searchByContactName($accReceivable);
            if (!empty($contactByContactName) && $contactByContactName != 'No match found' && $contactByContactName != 'API Error') {
                $this->companyExist = true;
                $companyId = $contactByContactName;
            } else {
                if ($contactByContactName == 'API Error') {
                    return $this->log;
                }
                $companyId = $this->createCompanyRecord($accReceivable->Contact);

                if($companyId == 'Error in creating Company'){
                    return $this->log;
                }
            }
        }

        $this->xeroInvoiceInstance =  $this->xero->loadByGUID(Invoice::class, $accReceivable->InvoiceID);

        $xeroContactQuery = DB::table('xero_contacts')->where('batch_number', $this->batchNumber)->where('contact_id', $accReceivable->Contact->ContactID)->first();
        $xeroContact = !empty($xeroContactQuery->record_details) ? json_decode($xeroContactQuery->record_details) : '';

        $companyFocus = \ZCRMRecord::getInstance("Accounts", $companyId);
        $this->companyEntityId = $companyFocus->getEntityId();

        $companyQuery = \ZCRMModule::getInstance('Accounts')->getRecord($this->companyEntityId);
        $company = $companyQuery->getResponseJSON();

        if (!isset($companyId)) {
            $this->log['error']['company'][] = "Company record is not existing in Companies module";
            return $this->log;
        }

        // Processing for contact
        $contactId = $this->processContactDetails($xeroContact);
        if (!empty($contactId)) {
            $contactQuery = \ZCRMModule::getInstance('Contacts')->getRecord($contactId);
            $contactDetails = $contactQuery->getResponseJSON();
        } else {
            $contactDetails = [];
        }

        if (!isset($contactDetails['data']['0']['Email']) || empty($contactDetails['data']['0']['Email'])) {
            $this->log['error']['contact'][] = "Contacts email address is missing with xero contact id : {$accReceivable->Contact->ContactID}";
        } else {
            // link to Company
            $companyToLink = $company['data'][0]['Primary_Contact'];
            if (empty($this->companyExist) && (!isset($companyToLink) || $companyToLink['id'] != $contactId)) {
                $companyFocus->setFieldValue('Primary_Contact', !empty($contactId) ? $contactId : '');

                try {
                    $updateRecordResponse = $companyFocus->update();
                    if ($updateRecordResponse->getStatus() == 'success' && $updateRecordResponse->getMessage() == 'record updated') {
                        $this->log['info']['company'][] = "Primary Contact is successfully  added/updated to Company Record with id: {$companyId}";
                    }
                } catch (\Exception $e) {
                    $this->log['fatal_error']['company'][] = "Encountered an error while setting the primary contact for Company, see details: {$e->getMessage()}";
                }
            }
        }

        // Processing for invoice
        $this->processInvoiceDetails($accReceivable, $this->companyEntityId, $contactId);

        return $this->log;
    }

    /**
     * Search account by Xero_Contact_ID
     * @param $accReceivable
     * @return array|string
     */
    private function searchByXeroContactId($accReceivable)
    {
        $companyIns = \ZCRMModule::getInstance("Accounts");
        $searchVal = $accReceivable->Contact->ContactID;
        $criteria = "(Xero_Contact_ID:equals:{$searchVal})";

        try {
            $bulkAPIResponse = $companyIns->searchRecordsByCriteria($criteria);
            $record = $bulkAPIResponse->getResponseJSON();
            return (isset($record['data'][0]['id']) || !empty($record['data'][0]['id'])) ? $record['data'][0]['id'] : [];
        } catch (\Exception $e) {
            if($e->getMessage() == 'No Content'){
                return 'No match found';
            }else{
                $this->log['fatal_error']['company'][] = "Encountered an api error while searching an existing Company by Name, see details: {$e->getMessage()}";
                return 'API Error';
            }
        }
    }

    /**
     * Search contact by name
     * @param $accReceivable
     * @return array|string
     */
    private function searchByContactName($accReceivable)
    {
        $companyIns = \ZCRMModule::getInstance("Accounts");
        $searchVal = $this->addSlashToSearchCriteria($accReceivable->Contact->Name);
        $criteria = "(Company_Name:equals:{$searchVal})";

        try {
            $bulkAPIResponse = $companyIns->searchRecordsByCriteria($criteria);
            $record = $bulkAPIResponse->getResponseJSON();
            return (isset($record['data'][0]['id']) || !empty($record['data'][0]['id'])) ? $record['data'][0]['id'] : [];
        } catch (\Exception $e) {
            if($e->getMessage() == 'No Content'){
                return 'No match found';
            }else{
                $this->log['fatal_error']['company'][] = "Encountered an api error while searching an existing Company by Name, see details: {$e->getMessage()}";
                return 'API Error';
            }
        }
    }

    /**
     * Search products by name
     * @param $productName
     * @return array|bool|string
     */
    private function searchServiceByName($productName){
        $productIns = \ZCRMModule::getInstance("Products");
        $searchVal = $this->addSlashToSearchCriteria($productName);
        $criteria = "(Product_Name:equals:{$searchVal})";

        try {
            $bulkAPIResponse = $productIns->searchRecordsByCriteria($criteria);
            $record = $bulkAPIResponse->getResponseJSON();
            return (isset($record['data'][0]['id']) || !empty($record['data'][0]['id'])) ? $record['data'][0]['id'] : [];
        } catch (\Exception $e) {
            if($e->getMessage() == 'No Content'){
                return 'No match found';
            }else{
                $this->log['fatal_error']['invoice'][] = "Encountered an api error while searching an existing Product by Name, see details: {$e->getMessage()}";
                return false;
            }
        }
    }

    /**
     * Create company record
     * @param $xeroContact
     * @return string
     */
    private function createCompanyRecord($xeroContact)
    {
        $companyId = '';
        $xeroContactQuery = DB::table('xero_contacts')->where('batch_number', $this->batchNumber)->where('contact_id',$xeroContact->ContactID)->first();
        $xeroContactDetails = !empty($xeroContactQuery->record_details) ? json_decode($xeroContactQuery->record_details) : '';

        if (empty($xeroContact->ContactID)){
            $this->log['fatal_error']['company'][] = 'Fatal: Xero Company not Existing in Xero Will not proceed to sync';
            return 'Error in creating Company';
        }

        $defaultPhone = isset($xeroContactDetails->Phones) ? $this->parsePhoneDetails($xeroContactDetails->Phones, 'DEFAULT') : [];
        $poBoxAddress = isset($xeroContactDetails->Addresses) ? $this->parseAddress($xeroContactDetails->Addresses, 'POBOX') : [];
        $shippingAddress = isset($xeroContactDetails->Addresses) ? $this->parseAddress($xeroContactDetails->Addresses, 'STREET') : [];

        $newCompany = \ZCRMRecord::getInstance("Accounts", null);

        $attributesMappingSubscription = DB::table('config')->where('category', 'Subscription')->where('name', 'company_attributes')->first();

        if(!empty($attributesMappingSubscription->value) && $attributesMappingSubscription->value == true){
            $companyAttributes = DB::table('company_attributes')->select('keyword')->get();
            $companyAttributes->toArray();

            foreach($companyAttributes as $attribute){
                if(strpos($xeroContact->Name, $attribute->keyword) !== false){
                    $attributeDetails = DB::table('company_attributes')->select('keyword, industry, channel, category')->first();
                    $newCompany->setFieldValue('Industry', !empty($attributeDetails->industry) ? $attributeDetails->industry : '');
                }
            }
        }

        $countryMappingSubscription = DB::table('config')->where('category', 'Subscription')->where('name', 'country_mapping')->first();

        $newCompany->setFieldValue('Xero_Contact_ID', !empty($xeroContactDetails->ContactID) ? $xeroContactDetails->ContactID : '');
        $newCompany->setFieldValue('Company_Name', !empty($xeroContactDetails->Name) ? $xeroContactDetails->Name : '');
        $accountNumber = !empty($xeroContactDetails->AccountNumber) ?  $xeroContactDetails->AccountNumber . ': ' : '';
        $accountName = !empty($xeroContactDetails->Name) ? $xeroContactDetails->Name : '';
        $newCompany->setFieldValue('Account_Name', $accountNumber . $accountName);
        // $newCompany->setFieldValue('Website', !empty($xeroContactDetails->Website) ? $xeroContactDetails->Website : '');
        $newCompany->setFieldValue('Phone', !empty($defaultPhone) ? $defaultPhone : '');
        $newCompany->setFieldValue('Billing_Street', !empty($poBoxAddress->AddressLine1) ? $poBoxAddress->AddressLine1 : '');
        $newCompany->setFieldValue('Billing_City', !empty($poBoxAddress->City) ? $poBoxAddress->City : '');
        $newCompany->setFieldValue('Billing_Code', !empty($poBoxAddress->PostalCode) ? $poBoxAddress->PostalCode : '');

        $newCompany->setFieldValue('Shipping_Street', !empty($shippingAddress->AddressLine1) ? $shippingAddress->AddressLine1 : '');
        $newCompany->setFieldValue('Shipping_City', !empty($shippingAddress->City) ? $shippingAddress->City : '');
        $newCompany->setFieldValue('Shipping_Code', !empty($shippingAddress->PostalCode) ? $shippingAddress->PostalCode : '');

        // Check if Country and State mapping is enabled
        if(!empty($countryMappingSubscription->value) && $countryMappingSubscription->value == true){
            $newCompany->setFieldValue('Billing_State', $this->processCompanyState($poBoxAddress));
            $newCompany->setFieldValue('Billing_Country', $this->processCompanyCountry($poBoxAddress));

            $newCompany->setFieldValue('Shipping_State', $this->processCompanyState($shippingAddress));
            $newCompany->setFieldValue('Shipping_Country', $this->processCompanyCountry($shippingAddress));
        }else{
            $newCompany->setFieldValue('Billing_State', !empty($poBoxAddress->Region) ? $poBoxAddress->Region : '');
            $newCompany->setFieldValue('Billing_Country', !empty($poBoxAddress->Country) ? $poBoxAddress->Country : '');

            $newCompany->setFieldValue('Shipping_State', !empty($shippingAddress->Region) ? $shippingAddress->Region : '');
            $newCompany->setFieldValue('Shipping_Country', !empty($shippingAddress->Country) ? $shippingAddress->Country : '');
        }

        try{
            $createRecordResponse = $newCompany->create();
            if($createRecordResponse->getStatus() == 'success' && $createRecordResponse->getMessage() == 'record added'){
                $supplierDetails = $createRecordResponse->getResponseJSON();
                $companyId = $supplierDetails['data'][0]['details']['id'];
                $this->log['info']['company'][] = "New Company Record is created with id: {$companyId}";
            }
        }catch(\Exception $e){
            $this->log['fatal_error']['company'][] = "Fatal: Encountered error while creating Company. Will not proceed to sync, see Details: {$e->getMessage()}";
            return 'Error in creating Company';
        }

        return $companyId;
    }

    /**
     * Process contact details
     * @param $xeroContact
     * @return string
     */
    private function processContactDetails($xeroContact){
        $contactId = '';

        if(empty($xeroContact)){
            return $contactId;
        }
        // Search for existing contact
        try {
            $contactIns = \ZCRMModule::getInstance("Contacts");
            $email = $xeroContact->EmailAddress;
            $contactQueryResponse = $contactIns->searchRecordsByEmail($email);
            $record = $contactQueryResponse->getResponseJSON();
            if (!empty($record['data'][0]['id'])) {
                $this->contactExist = true;
                $contactId = $record['data'][0]['id'];
            }
        } catch (\Exception $e) {
            // Create new contact since it doesn't exist
            $directPhone = $this->parsePhoneDetails($xeroContact->Phones, 'DDI');
            $mobile = $this->parsePhoneDetails($xeroContact->Phones, 'MOBILE');

            if ($e->getMessage() == 'No Content') {
                $newContact = \ZCRMRecord::getInstance("Contacts", null);
                $newContact->setFieldValue('First_Name', !empty($xeroContact->FirstName) ? $xeroContact->FirstName : '');
                $newContact->setFieldValue('Last_Name', !empty($xeroContact->LastName) ? $xeroContact->LastName : '');
                $newContact->setFieldValue('Email', !empty($xeroContact->EmailAddress) ? $xeroContact->EmailAddress : '');
                $newContact->setFieldValue('Mobile', !empty($mobile) ? $mobile : '');
                $newContact->setFieldValue('Phone', !empty($directPhone) ? $directPhone : '');
                try {
                    $createRecordResponse = $newContact->create();

                    if($createRecordResponse->getStatus() == 'success'  && $createRecordResponse->getMessage() == 'record added'){
                        $contactDetails = $createRecordResponse->getResponseJSON();
                        $contactId = $contactDetails['data'][0]['details']['id'];
                        $this->log['info']['contact'][] = "New Contacts Record is created with id: $contactId";
                    }
                } catch (\Exception $e) {
                    if(trim($e->getMessage()) == "required field not found"){
                        $this->log['error']['contact'][] = "Required field not populated. This could be the email address.";
                    }else{
                        $this->log['fatal_error']['contact'][] = "Encountered an api error while creating the contact record, see details: {$e->getMessage()}";
                    }
                }
            } else {
                if(empty($email)){
                    $this->log['error']['contact'][] = "Xero Contact has no email. It is required in zoho. Zoho contact not created";
                }
            }
        }

        return $contactId;
    }

    /**
     * Process invoice details
     * @param $accReceivable
     * @param $companyId
     * @param $contactId
     * @return string
     * @throws \ZCRMException
     */
    private function processInvoiceDetails($accReceivable, $companyId, $contactId){
        $datetime = new \DateTime('UTC');
        $syncedDate =  $datetime->format(\DateTime::ATOM);


        $invoiceId = '';
        $xeroContact = $accReceivable->Contact;
        // Search for existing Invoice
        try {
            $apInvoiceIns = \ZCRMModule::getInstance("Invoices");
            $criteria = "(Invoice_No1:equals:{$accReceivable->InvoiceNumber})";
            $invoiceQueryResponse = $apInvoiceIns->searchRecordsByCriteria($criteria);
            $record = $invoiceQueryResponse->getResponseJSON();
            if (!empty($record['data'][0]['id'])) {
                $this->invoiceExist = true;
                $invoiceId = $record['data'][0]['id'];
            }
        } catch (\Exception $e) {
            // Create new invoice since it doesn't exist
            if ($e->getMessage() == 'No Content') {
                $lastDatePaid = $this->queryForLastDatePaid();
                $lastCreditDatePaid = $this->queryForLastCreditDatePaid();

                $newInvoice = \ZCRMRecord::getInstance("Invoices", null);

                $subtotal = !empty($accReceivable->SubTotal) ? $accReceivable->SubTotal : 0;
                $totalDiscount = !empty($accReceivable->TotalDiscount) ? $accReceivable->TotalDiscount : 0;
                $totalIncludingDiscount = (double)$subtotal + (double)$totalDiscount;

                $newInvoice->setFieldValue('Subject', !empty($accReceivable->InvoiceNumber) ? $accReceivable->InvoiceNumber : '');
                $newInvoice->setFieldValue('Invoice_Type', 'Accounts');
                $newInvoice->setFieldValue('Invoice_No1', !empty($accReceivable->InvoiceNumber) ? $accReceivable->InvoiceNumber : '');
                $newInvoice->setFieldValue('Currency', !empty($accReceivable->CurrencyCode) ? $accReceivable->CurrencyCode : '');
                $newInvoice->setFieldValue('Account_Name', !empty($companyId) ? $companyId : '');
                $newInvoice->setFieldValue('Contact_Name', !empty($contactId) ? $contactId : '');
                $newInvoice->setFieldValue('Invoice_Date', !empty($accReceivable->Date) ? $accReceivable->Date : '');
                $newInvoice->setFieldValue('Due_Date', !empty($accReceivable->DueDate) ? $accReceivable->DueDate : '');
                $newInvoice->setFieldValue('Amount_Paid', !empty($accReceivable->AmountPaid) ? $accReceivable->AmountPaid : 0);
                $newInvoice->setFieldValue('Xero_Invoice_Status', $this->parseInvoiceStatus($accReceivable->Status));
                $newInvoice->setFieldValue('Date_Paid', !empty($lastDatePaid) ? $lastDatePaid : '');
                $newInvoice->setFieldValue('Expected_Payment_Date', !empty($accReceivable->ExpectedPaymentDate) ? $this->formatDateToYmd($accReceivable->ExpectedPaymentDate) : '');
                $newInvoice->setFieldValue('quotecalculation__Sub_Total_Amount', !empty($accReceivable->SubTotal) ? $accReceivable->SubTotal : '');
                $newInvoice->setFieldValue('quotecalculation__Discount_Amount', !empty($accReceivable->TotalDiscount) ? $accReceivable->TotalDiscount : '');
                $newInvoice->setFieldValue('quotecalculation__Total_Including_Discount', !empty($totalIncludingDiscount) ? $totalIncludingDiscount : '');
                $newInvoice->setFieldValue('Xero_Grand_Total', !empty($accReceivable->Total) ? $accReceivable->Total : '');
                $newInvoice->setFieldValue('Xero_Tax_Type', !empty($accReceivable->LineAmountTypes) ? 'Tax ' . $accReceivable->LineAmountTypes : '');
                $newInvoice->setFieldValue('Xero_Exchange_Rate', !empty($accReceivable->CurrencyRate) ? $accReceivable->CurrencyRate : '');
                $newInvoice->setFieldValue('Total_Amount_Credited', !empty($accReceivable->AmountCredited) ? $accReceivable->AmountCredited : '');
                $newInvoice->setFieldValue('Last_Credit_Note_Date', !empty($lastCreditDatePaid) ? $lastCreditDatePaid : '');

                $xeroContactQuery = DB::table('xero_contacts')->where('batch_number', $this->batchNumber)->where('contact_id',$xeroContact->ContactID)->first();
                $xeroContactDetails = !empty($xeroContactQuery->record_details) ? json_decode($xeroContactQuery->record_details) : '';

                $poBoxAddress = isset($xeroContactDetails->Addresses) ? $this->parseAddress($xeroContactDetails->Addresses, 'POBOX') : [];
                $shippingAddress = isset($xeroContactDetails->Addresses) ? $this->parseAddress($xeroContactDetails->Addresses, 'STREET') : [];

                $newInvoice->setFieldValue('Billing_Street', !empty($poBoxAddress->AddressLine1) ? $poBoxAddress->AddressLine1 : '');
                $newInvoice->setFieldValue('Billing_City', !empty($poBoxAddress->City) ? $poBoxAddress->City : '');
                $newInvoice->setFieldValue('Billing_Code', !empty($poBoxAddress->PostalCode) ? $poBoxAddress->PostalCode : '');

                $newInvoice->setFieldValue('Shipping_Street', !empty($shippingAddress->AddressLine1) ? $shippingAddress->AddressLine1 : '');
                $newInvoice->setFieldValue('Shipping_City', !empty($shippingAddress->City) ? $shippingAddress->City : '');
                $newInvoice->setFieldValue('Shipping_Code', !empty($shippingAddress->PostalCode) ? $shippingAddress->PostalCode : '');

                // Check if Country and State mapping is enabled
                $countryMappingSubscription = DB::table('config')->where('category', 'Subscription')->where('name', 'country_mapping')->first();

                if(!empty($countryMappingSubscription->value) && $countryMappingSubscription->value == true){
                    $newInvoice->setFieldValue('Billing_State', $this->processCompanyState($poBoxAddress));
                    $newInvoice->setFieldValue('Billing_Country', $this->processCompanyCountry($poBoxAddress));

                    $newInvoice->setFieldValue('Shipping_State', $this->processCompanyState($shippingAddress));
                    $newInvoice->setFieldValue('Shipping_Country', $this->processCompanyCountry($shippingAddress));
                }else{
                    $newInvoice->setFieldValue('Billing_State', !empty($poBoxAddress->Region) ? $poBoxAddress->Region : '');
                    $newInvoice->setFieldValue('Billing_Country', !empty($poBoxAddress->Country) ? $poBoxAddress->Country : '');

                    $newInvoice->setFieldValue('Shipping_State', !empty($shippingAddress->Region) ? $shippingAddress->Region : '');
                    $newInvoice->setFieldValue('Shipping_Country', !empty($shippingAddress->Country) ? $shippingAddress->Country : '');
                }

                $xeroInvoice = $this->xeroInvoiceInstance;

                $xeroLineItemsQuery = $xeroInvoice->getLineItems();
                $xeroLineItems = $this->parseData($xeroLineItemsQuery);

                $xeroPaymentsQuery = $xeroInvoice->getPayments();
                $xeroPayments = $this->parseData($xeroPaymentsQuery);

                $productMappingSubscription = DB::table('config')->where('category', 'Subscription')->where('name', 'product_mapping')->first();

                // Process Line Items
                if(!empty($xeroLineItems)){
                    if(!empty($productMappingSubscription->value) && $productMappingSubscription->value == true){
                        $newInvoice = $this->processLineItemsByMapping($accReceivable, $xeroLineItems, $newInvoice);
                    } else {
                        $newInvoice = $this->processLineItemsByBasic($accReceivable, $xeroLineItems, $newInvoice);
                    }
                }

                $datetime = new \DateTime();
                $syncedDate =  $datetime->format(\DateTime::ATOM);

                // Check if there is no issue from invoice create. Dont continue if it has issue
                if(!empty($this->log['fatal_error']['invoice'])){
                    return false;
                }

                try{
                    $createRecordResponse = $newInvoice->create();

                    if($createRecordResponse->getStatus() == 'success'  && $createRecordResponse->getMessage() == 'record added'){
                        $invoiceDetails = $createRecordResponse->getResponseJSON();
                        $invoiceId = $invoiceDetails['data'][0]['details']['id'];
                        $newInvoice = \ZCRMRecord::getInstance("Invoices", $invoiceId);

                        $invEntityApi = \EntityAPIHandler::getInstance($newInvoice);

                        // Dont Remove: Fetch data to access getFieldValue method in $newInvoice
                        $invoiceRecQuery = $invEntityApi->getRecord();

                        $xeroGrandTotal = $newInvoice->getFieldValue('Xero_Grand_Total');
                        $zohoGrandTotal = round($newInvoice->getFieldValue('Grand_Total'), 2);

                        if($xeroGrandTotal != $zohoGrandTotal){
                            $adjustment = $xeroGrandTotal - $zohoGrandTotal;
                            $adjustment = !empty($adjustment) ? round($xeroGrandTotal - $zohoGrandTotal,2) : '';
                            $newInvoice->setFieldValue('Adjustment', $adjustment);
                        }

                        $newInvoice->setFieldValue('Xero_Status', 'Synced');
                        $newInvoice->setFieldValue('Xero_Sync_Time', $syncedDate);
                        $newInvoice->setFieldValue('From_Xero', true);
                        $newInvoice->setFieldValue('Xero_Error', "");
                        $newInvoice->update();
                        DB::table('zoho_invoice_update_timestamps')->insert(['invoice_number' => $accReceivable->InvoiceNumber]);
                        $this->log['info']['invoice'][] = "New Invoice Record is created with id: $invoiceId";
                    }
                }catch(\Exception $e){
                    $this->log['fatal_error']['invoice'][] = "Encountered and api error while creating the Invoice, see details: {$e->getMessage()}";
                    return false;
                }
            }else{
                $this->log['fatal_error']['invoice'][] = "Encountered and api error while searching existing Invoice, see details: {$e->getMessage()}";
            }
        }

        // Update Invoice if existing
        if($this->invoiceExist){
            $invoiceQuery = \ZCRMModule::getInstance('Invoices')->getRecord($invoiceId);
            $invoiceDetails = $invoiceQuery->getResponseJSON();

            // Sync only if currency code is the same.
            if($accReceivable->CurrencyCode == $invoiceDetails['data'][0]['Currency']){
                $lastDatePaid = $this->queryForLastDatePaid();
                $lastCreditDatePaid = $this->queryForLastCreditDatePaid();

                $existingInvoice = \ZCRMRecord::getInstance("Invoices", $invoiceId);
                $invEntityApi = \EntityAPIHandler::getInstance($existingInvoice);
                $invoiceRecQuery = $invEntityApi->getRecord();

                $invoiceSync = $existingInvoice->getFieldValue('Invoice_Sync');
                if($invoiceSync == 'Disabled'){
                    $this->log['fatal_error']['invoice'][] = 'Will not proceed to sync this invoice since Invoice Sync is Disabled';
                    return false;
                }

                $subtotal = !empty($accReceivable->SubTotal) ? $accReceivable->SubTotal : 0;
                $totalDiscount = !empty($accReceivable->TotalDiscount) ? $accReceivable->TotalDiscount : 0;
                $totalIncludingDiscount = (double)$subtotal + (double)$totalDiscount;

                // $existingInvoice->setFieldValue('Subject', !empty($accReceivable->InvoiceNumber) ? $accReceivable->InvoiceNumber : '');
                $existingInvoice->setFieldValue('Invoice_No1', !empty($accReceivable->InvoiceNumber) ? $accReceivable->InvoiceNumber : '');
                $existingInvoice->setFieldValue('Currency', !empty($accReceivable->CurrencyCode) ? $accReceivable->CurrencyCode : '');
                $existingInvoice->setFieldValue('Contact_Name', !empty($contactId) ? $contactId : '');
                $existingInvoice->setFieldValue('Invoice_Date', !empty($accReceivable->Date) ? $accReceivable->Date : '');
                $existingInvoice->setFieldValue('Due_Date', !empty($accReceivable->DueDate) ? $accReceivable->DueDate : '');
                $existingInvoice->setFieldValue('Amount_Paid', !empty($accReceivable->AmountPaid) ? $accReceivable->AmountPaid : '');
                $existingInvoice->setFieldValue('Xero_Invoice_Status', $this->parseInvoiceStatus($accReceivable->Status));
                $existingInvoice->setFieldValue('Date_Paid', !empty($lastDatePaid) ? $lastDatePaid : '');
                $existingInvoice->setFieldValue('Expected_Payment_Date', !empty($accReceivable->ExpectedPaymentDate) ? $this->formatDateToYmd($accReceivable->ExpectedPaymentDate) : '');
                $existingInvoice->setFieldValue('quotecalculation__Sub_Total_Amount', !empty($accReceivable->SubTotal) ? $accReceivable->SubTotal : '');
                $existingInvoice->setFieldValue('quotecalculation__Discount_Amount', !empty($accReceivable->TotalDiscount) ? $accReceivable->TotalDiscount : '');
                $existingInvoice->setFieldValue('quotecalculation__Total_Including_Discount', !empty($totalIncludingDiscount) ? $totalIncludingDiscount : '');
                $existingInvoice->setFieldValue('Xero_Grand_Total', !empty($accReceivable->Total) ? $accReceivable->Total : '');
                $existingInvoice->setFieldValue('Xero_Tax_Type', !empty($accReceivable->LineAmountTypes) ? 'Tax ' . $accReceivable->LineAmountTypes : '');
                $existingInvoice->setFieldValue('Xero_Exchange_Rate', !empty($accReceivable->CurrencyRate) ? $accReceivable->CurrencyRate : '');
                $existingInvoice->setFieldValue('Total_Amount_Credited', !empty($accReceivable->AmountCredited) ? $accReceivable->AmountCredited : '');
                $existingInvoice->setFieldValue('Last_Credit_Note_Date', !empty($lastCreditDatePaid) ? $lastCreditDatePaid : '');
                $existingInvoice->setFieldValue('Adjustment', 0);
                $existingInvoice->setFieldValue('Discount', 0);
                $existingInvoice->setFieldValue('From_Xero', true);
                if(empty($invoiceDetails['data'][0]['Account_Name']['id']) || $invoiceDetails['data'][0]['Account_Name']['id'] != $companyId){
                    $existingInvoice->setFieldValue('Account_Name', !empty($companyId) ? $companyId : '');
                }

                // Update Shipping and Billing Address
                $xeroContactQuery = DB::table('xero_contacts')->where('batch_number', $this->batchNumber)->where('contact_id',$xeroContact->ContactID)->first();
                $xeroContactDetails = !empty($xeroContactQuery->record_details) ? json_decode($xeroContactQuery->record_details) : '';

                $poBoxAddress = isset($xeroContactDetails->Addresses) ? $this->parseAddress($xeroContactDetails->Addresses, 'POBOX') : [];
                $shippingAddress = isset($xeroContactDetails->Addresses) ? $this->parseAddress($xeroContactDetails->Addresses, 'STREET') : [];

                $existingInvoice->setFieldValue('Billing_Street', !empty($poBoxAddress->AddressLine1) ? $poBoxAddress->AddressLine1 : '');
                $existingInvoice->setFieldValue('Billing_City', !empty($poBoxAddress->City) ? $poBoxAddress->City : '');
                $existingInvoice->setFieldValue('Billing_Code', !empty($poBoxAddress->PostalCode) ? $poBoxAddress->PostalCode : '');

                $existingInvoice->setFieldValue('Shipping_Street', !empty($shippingAddress->AddressLine1) ? $shippingAddress->AddressLine1 : '');
                $existingInvoice->setFieldValue('Shipping_City', !empty($shippingAddress->City) ? $shippingAddress->City : '');
                $existingInvoice->setFieldValue('Shipping_Code', !empty($shippingAddress->PostalCode) ? $shippingAddress->PostalCode : '');

                // Check if Country and State mapping is enabled
                $countryMappingSubscription = DB::table('config')->where('category', 'Subscription')->where('name', 'country_mapping')->first();

                if(!empty($countryMappingSubscription->value) && $countryMappingSubscription->value == true){
                    $existingInvoice->setFieldValue('Billing_State', $this->processCompanyState($poBoxAddress));
                    $existingInvoice->setFieldValue('Billing_Country', $this->processCompanyCountry($poBoxAddress));

                    $existingInvoice->setFieldValue('Shipping_State', $this->processCompanyState($shippingAddress));
                    $existingInvoice->setFieldValue('Shipping_Country', $this->processCompanyCountry($shippingAddress));
                }else{
                    $existingInvoice->setFieldValue('Billing_State', !empty($poBoxAddress->Region) ? $poBoxAddress->Region : '');
                    $existingInvoice->setFieldValue('Billing_Country', !empty($poBoxAddress->Country) ? $poBoxAddress->Country : '');

                    $existingInvoice->setFieldValue('Shipping_State', !empty($shippingAddress->Region) ? $shippingAddress->Region : '');
                    $existingInvoice->setFieldValue('Shipping_Country', !empty($shippingAddress->Country) ? $shippingAddress->Country : '');
                }


                $xeroInvoice = $this->xeroInvoiceInstance;

                $xeroLineItemsQuery = $xeroInvoice->getLineItems();
                $xeroLineItems = $this->parseData($xeroLineItemsQuery);

                $xeroPaymentsQuery = $xeroInvoice->getPayments();
                $xeroPayments = $this->parseData($xeroPaymentsQuery);

                //Updates from xero must not touch the line items anymore
                //$productMappingSubscription = DB::table('config')->where('category', 'Subscription')->where('name', 'product_mapping')->first();

                // Process Line Items
                //if(!empty($xeroLineItems)){
                    //if(!empty($productMappingSubscription->value) && $productMappingSubscription->value == true){
                        //$existingInvoice = $this->processLineItemsByMapping($accReceivable, $xeroLineItems, $existingInvoice);
                    //} else {
                        //$existingInvoice = $this->processLineItemsByBasic($accReceivable, $xeroLineItems, $existingInvoice);
                    //}
                //}

                // Check if there is no issue from invoice update. Dont continue if it has issue
                if(!empty($this->log['fatal_error']['invoice'])){
                    return false;
                }

                try{
                    $updateAPInvoiceResponse = $existingInvoice->update();

                    if ($updateAPInvoiceResponse->getStatus() == 'success' && $updateAPInvoiceResponse->getMessage() == 'record updated') {
                        $invEntityApi = \EntityAPIHandler::getInstance($existingInvoice);

                        $zohoLineItems = $existingInvoice->getLineItems(); //Retrieves the original zoho list for the bug below

                        // Don't Remove: Fetch data to access getFieldValue method in $newInvoice
                        $invoiceRecQuery = $invEntityApi->getRecord(); //Needed for the grand total updated value

                        //BUG: Removes the duplicate line items due to the getRecord() method above
                        //<!--
                        $duplicateLineItems = $existingInvoice->getLineItems();
                        foreach($duplicateLineItems as $zohoLineItem){ //Removes all the line items due to duplicate
                            $lineItemId = $zohoLineItem->getId();
                            $existingInvoice->removeLineItem($lineItemId);
                        }
                        foreach($zohoLineItems as $lineItem){ //Insert original line item list before duplicate
                            $existingInvoice->addLineItem($lineItem);
                        }
                        //-->

                        $xeroGrandTotal = $existingInvoice->getFieldValue('Xero_Grand_Total');
                        $zohoGrandTotal = round($existingInvoice->getFieldValue('Grand_Total'), 2);

                        if($xeroGrandTotal != $zohoGrandTotal){
                            $adjustment = $xeroGrandTotal - $zohoGrandTotal;
                            //Since line items wouldn't be updated anymore, so is the adjustment
                            //$existingInvoice->setFieldValue('Adjustment', $adjustment);
                        }


                        if($xeroGrandTotal == $zohoGrandTotal){
                           //$existingInvoice->setFieldValue('Adjustment', 0);
                        }

                        $existingInvoice->setFieldValue('Xero_Status', 'Synced');
                        $existingInvoice->setFieldValue('Xero_Sync_Time', $syncedDate);
                        $existingInvoice->setFieldValue('Xero_Error', "");
                        $existingInvoice->update();
                        DB::table('zoho_invoice_update_timestamps')->insert(['invoice_number' => $accReceivable->InvoiceNumber]);
                        $this->log['info']['invoice'][]  = "Invoice Record is updated with id: $invoiceId";
                    } else {
                        $existingInvoice->setFieldValue('Xero_Status', 'Error');
                        $existingInvoice->setFieldValue('Xero_Sync_Time', $syncedDate);
                        $existingInvoice->setFieldValue('Xero_Error', $updateAPInvoiceResponse->getMessage());
                        $existingInvoice->update();
                        DB::table('zoho_invoice_update_timestamps')->insert(['invoice_number' => $accReceivable->InvoiceNumber]);
                        $this->log['fatal_error']['invoice'][]  = "Invoice Record is not updated with id: $invoiceId";
                    }
                }catch(\Exception $e){
                    $errorMessage = $e->getMessage();
                    $existingInvoice->setFieldValue('Xero_Status', 'Error');
                    $existingInvoice->setFieldValue('Xero_Sync_Time', $syncedDate);
                    $existingInvoice->setFieldValue('Xero_Error', $errorMessage);
                    try{
                        $existingInvoice->update();
                    }catch(\Exception $e){
                        $this->log['fatal_error']['invoice'][] = "Encountered and api error while saving an error during invoice update: {$e->getMessage()}";
                    }
                    DB::table('zoho_invoice_update_timestamps')->insert(['invoice_number' => $accReceivable->InvoiceNumber]);
                    $this->log['fatal_error']['invoice'][] = "Encountered and api error while updating an existing Invoice, see details: {$e->getMessage()}";
                }
            }else{
                $this->log['error']['invoice'][] = "Invoice update will not proceed due to Currency code mismatch in Invoice";
            }
        }

        return $invoiceId;
    }

    private function parseData($record)
    {
        $jsonEncodedData = json_encode($record);
        return json_decode($jsonEncodedData);
    }

    /**
     * Parse phone details
     * @param $phone
     * @param $detailType
     * @return string
     */
    private function parsePhoneDetails($phone, $detailType){
        foreach($phone  as $phoneDetail){
            if($phoneDetail->PhoneType == $detailType) {
                if(!empty($phoneDetail->PhoneCountryCode) || !empty($phoneDetail->PhoneAreaCode) || !empty($phoneDetail->PhoneNumber)){
                    $countryCode = (isset($phoneDetail->PhoneCountryCode)) ? $phoneDetail->PhoneCountryCode : '';
                    $areaCode = (isset($phoneDetail->PhoneAreaCode)) ? $phoneDetail->PhoneAreaCode : '';
                    $phoneNumber = (isset($phoneDetail->PhoneNumber)) ? $phoneDetail->PhoneNumber  : '';
                    $areaCodeSeparator = !empty($countryCode) ? '-' : '';
                    $phoneNumSeparator = (!empty($countryCode) || !empty($areaCode)) ? '-' : '';
                    $fullPhoneDetails= (!empty($countryCode) ? $countryCode : '') . (!empty($areaCode) ? $areaCodeSeparator . $areaCode  : '') . (!empty($phoneNumber) ? $phoneNumSeparator . $phoneNumber  : '');
                    return $fullPhoneDetails;
                }
            }
        }

        return '';
    }

    /**
     * Parse address details
     * @param $address
     * @param $addressType
     * @return array
     */
    private function parseAddress($address, $addressType){
        foreach($address as $addressDetail){
            if(!empty($addressDetail->AddressType) && $addressDetail->AddressType == $addressType){
                return $addressDetail;
            }
        }

        return [];
    }

    /**
     * Get last date paid
     * @return false|string
     */
    private function queryForLastDatePaid(){
        $lastPaidDate = '';
        $xeroInvoiceQuery = $this->xeroInvoiceInstance;
        $getPayments = $xeroInvoiceQuery->getPayments();
        $paymentsArray = $this->parseData($getPayments);

        if(!empty($paymentsArray)){
            $paymentDates = [];
            foreach($paymentsArray as $payment){
                if(empty($payment->Date)){
                    continue;
                }
                $paymentDates[] = $payment->Date;
            }

            if(!empty($paymentDates)){
                $max = max(array_map('strtotime', $paymentDates));
                $lastPaidDate = date('Y-m-d', $max);
            }
        }

        return $lastPaidDate;
    }

    private function queryForLastCreditDatePaid(){
        $lastPaidDate = '';
        $xeroInvoiceQuery = $this->xeroInvoiceInstance;
        $getCreditNotes = $xeroInvoiceQuery->getCreditNotes();
        $creditsArray = $this->parseData($getCreditNotes);

        if(!empty($creditsArray)){
            $creditDates = [];
            foreach($creditsArray as $credit){
                if(empty($credit->Date)){
                    continue;
                }
                $creditDates[] = $credit->Date;
            }

            if(!empty($creditDates)){
                $max = max(array_map('strtotime', $creditDates));
                $lastPaidDate = date('Y-m-d', $max);
            }
        }

        return $lastPaidDate;
    }

    /**
     * Process line items by directly checking product in product modules not by product tables mapping
     *
     * @param $accReceivable
     * @param $xeroLineItems
     * @param $invCrmRecIns
     * @return mixed
     * @throws \ZCRMException
     */
    private function processLineItemsByBasic($accReceivable, $xeroLineItems, $invCrmRecIns){
        if(empty($xeroLineItems)){
            return $invCrmRecIns;
        }

        $zohoLineItems = $invCrmRecIns->getLineItems();

        $existingZohoInvLineItemsProductLabels= [];
        foreach($zohoLineItems as $zohoLineItem){
            $product = $zohoLineItem->getProduct();
            $description = !empty($product->getLookupLabel()) ? (string)$product->getLookupLabel() : '';

            $existingZohoInvLineItemsProductLabels[$zohoLineItem->getId()] = strtolower(trim($description));
        }

        $existingZohoInvLineItemsLineItemDescs = [];
        foreach($zohoLineItems as $zohoLineItem){
            $description = $this->parseZohoLineItemDescription($zohoLineItem);
            $existingZohoInvLineItemsLineItemDescs[$zohoLineItem->getId()] = strtolower(trim($description));
        }

        // Remove line items that are not in xero
        $presentLineItemsInXero = [];
        foreach($xeroLineItems as $xeroLineItem){
            $xeroLineItemDesc = strtolower(trim($xeroLineItem->Description));
            if(in_array($xeroLineItemDesc, $existingZohoInvLineItemsProductLabels)){
                $presentLineItemsInXero[]=array_search($xeroLineItemDesc, $existingZohoInvLineItemsProductLabels);
            }

            if(in_array($xeroLineItemDesc, $existingZohoInvLineItemsLineItemDescs)){
                $presentLineItemsInXero[]=array_search($xeroLineItemDesc, $existingZohoInvLineItemsLineItemDescs);
            }
        }

        foreach($zohoLineItems as $zohoLineItem){
            $lineItemId = $zohoLineItem->getId();
            if(!in_array($lineItemId, $presentLineItemsInXero)){
                $invCrmRecIns->removeLineItem($lineItemId);
            }
        } // End remove of line item that are not in xero


        $presentLineItemsInXero = [];
        foreach($xeroLineItems as $xeroLineItem){
            $xeroLineItemDesc = strtolower(trim($xeroLineItem->Description));
            if(in_array($xeroLineItemDesc, $existingZohoInvLineItemsProductLabels)){
                $presentLineItemsInXero[]=array_search($xeroLineItemDesc, $existingZohoInvLineItemsProductLabels);
            }

            if(in_array($xeroLineItemDesc, $existingZohoInvLineItemsLineItemDescs)){
                $presentLineItemsInXero[]=array_search($xeroLineItemDesc, $existingZohoInvLineItemsLineItemDescs);
            }
        }

        $lineAmountTaxTypes = $accReceivable->LineAmountTypes;
        foreach($xeroLineItems as $xeroLineItem){
            $xeroLineItemName = $xeroLineItem->Description;

            //Skips through empty quantity items as this is a mandatory field on zoho
            if(!isset($xeroLineItem->Quantity) || empty((float)$xeroLineItem->Quantity)){
                continue;
            }

            $productId = '';
            $searchByProductName = $this->searchServiceByName($xeroLineItemName);
            if (!empty($searchByProductName) && $searchByProductName != 'No match found') {
                $productId = $searchByProductName;
            } else {
                if(empty($searchByProductName)){
                    continue;
                }
                $newProduct = \ZCRMRecord::getInstance("Products", null);
                $newProduct->setFieldValue('Product_Name', !empty($xeroLineItemName) ? $xeroLineItemName : '');
                $newProduct->setFieldValue('Unit_Price', !empty($xeroLineItem->UnitAmount) ? $xeroLineItem->UnitAmount : '');
                $createRecordResponse = $newProduct->create();

                if ($createRecordResponse->getStatus() == 'success' && $createRecordResponse->getMessage() == 'record added') {
                    $productDetails = $createRecordResponse->getResponseJSON();
                    $productId = $productDetails['data'][0]['details']['id'];
                }
            }

            if(!empty($productId)){
                $productInstance = \ZCRMRecord::getInstance("Products", $productId);

                $lineItem = \ZCRMInventoryLineItem::getInstance($productInstance);

                // Zoho API version does not have an ability to update the line item without removing it first
                // Remove first line items that has same product name from xero
                foreach ($existingZohoInvLineItemsProductLabels as $id => $zohoLineItemName) {
                    if (strtolower(trim($zohoLineItemName)) == strtolower(trim($xeroLineItemName))) {
                        $invCrmRecIns->removeLineItem($id);
                        $lineItem->setId($id);
                    }
                }

                foreach ($existingZohoInvLineItemsLineItemDescs as $id => $zohoLineItemName) {
                    if (strtolower(trim($zohoLineItemName)) == strtolower(trim($xeroLineItemName))) {
                        $invCrmRecIns->removeLineItem($id);
                        $lineItem->setId($id);
                    }
                } // End removal of line items

                $discountRate = !empty($xeroLineItem->DiscountRate) ? (double)$xeroLineItem->DiscountRate : 0;
                $quantity = !empty($xeroLineItem->Quantity) ? (double)$xeroLineItem->Quantity : 0.00;
                $taxAmount = !empty($xeroLineItem->TaxAmount) ? $xeroLineItem->TaxAmount : 0.00;
                $lineAmount = !empty($xeroLineItem->LineAmount) ? $xeroLineItem->LineAmount : 0.00;
                $discountAmount = 0;
                $taxType = !empty($xeroLineItem->TaxType) ? $xeroLineItem->TaxType : '';
                $taxDetails = $this->getLineItemTaxDetails($taxType);
                $zohoTaxName = !empty($taxDetails->zoho_tax_name) ? $taxDetails->zoho_tax_name : 'GST Free Income';
                $zohoTaxValue = !empty($taxDetails->zoho_tax_value) ? $taxDetails->zoho_tax_value : 0;

                if (strtolower($lineAmountTaxTypes) == 'inclusive') { // Computations for tax inclusive invoice
                    $xeroDiscountRate = !empty($xeroLineItem->DiscountRate) ? $xeroLineItem->DiscountRate : 0;
                    $xeroUnitAmount = !empty($xeroLineItem->UnitAmount) ? (double)$xeroLineItem->UnitAmount : 0;

                    if (!empty($xeroDiscountRate) && !empty($quantity) && !empty($xeroUnitAmount)) {
                        $discountAmount = round(((($xeroUnitAmount * $quantity) * ($xeroDiscountRate / 100)) / (100 + $zohoTaxValue)) * 100, 2);
                    }

                    if (!empty($quantity) && !empty($xeroUnitAmount)) {
                        $listPrice = ($lineAmount - $taxAmount + $discountAmount) / $quantity;
                    } else {
                        if(!empty($lineAmount)){
                            $listPrice = ($lineAmount - $taxAmount + $discountAmount);
                        }else{ // If there is no lineAmount then we cant compute properly the listprice so set it to zero
                            $listPrice = 0.00;
                        }
                    }

                    $lineItem->setQuantity($quantity);
                    $lineItem->setListPrice(round($listPrice, 2));
                    $lineItem->setDiscount((double)$discountAmount);
                    $lineItem->setDiscountPercentage($discountRate);

                    $taxInstance = \ZCRMTax::getInstance($zohoTaxName);
                    $taxInstance->setPercentage($zohoTaxValue);
                    $taxInstance->setValue((double) $taxAmount);
                    $lineItem->addLineTax($taxInstance);
                    $lineItem->setTaxAmount((double)$taxAmount);
                } else { // Tax Exclusive computations
                    $unitPricePerItem = !empty($xeroLineItem->UnitAmount) ? (double) $xeroLineItem->UnitAmount : 0.00;
                    $total = !empty($xeroLineItem->LineAmount) ? (double) $xeroLineItem->LineAmount : 0.00;

                    $lineItem->setQuantity(!empty($xeroLineItem->Quantity) ? (double) $xeroLineItem->Quantity : 0.00);
                    $lineItem->setListPrice($unitPricePerItem);
                    $lineItem->setDiscountPercentage(!empty($xeroLineItem->DiscountRate) ? (double) $xeroLineItem->DiscountRate : 0.00);

                    $unitAmount = !empty($xeroLineItem->UnitAmount) ? (double) $xeroLineItem->UnitAmount : 0.00;
                    $discountRate = !empty($xeroLineItem->DiscountRate) ? (double) $xeroLineItem->DiscountRate : 0.00;
                    $discountAmount = $unitAmount * ($discountRate * 0.01);

                    $lineItem->setDiscount((double) $discountAmount);
                    $lineItem->setTotal($total);
                    $lineItem->setTotalAfterDiscount(!empty($xeroLineItem->LineAmount) ? (double) $total : 0.00);
                    $taxInstance = \ZCRMTax::getInstance($zohoTaxName);
                    $taxInstance->setPercentage($zohoTaxValue);
                    $taxInstance->setValue((double) $taxAmount);
                    $lineItem->addLineTax($taxInstance);
                    $lineItem->setTaxAmount((double)$taxAmount);
                }

                $description = !empty($xeroLineItem->Description) ? $xeroLineItem->Description : '';
                $lineItem->setDescription($description);
                $invCrmRecIns->addLineItem($lineItem);
            }
        }

        return $invCrmRecIns;
    }

    /**
     * Process line items using product mapping
     * @param $accReceivable
     * @param $xeroLineItems
     * @param $invCrmRecIns
     * @return mixed
     */
    private function processLineItemsByMapping($accReceivable, $xeroLineItems, $invCrmRecIns){
        if(empty($xeroLineItems)){
            return $invCrmRecIns;
        }

        $zohoLineItems = $invCrmRecIns->getLineItems();

        $existingZohoInvLineItemsProductLabels= [];
        foreach($zohoLineItems as $zohoLineItem){
            $product = $zohoLineItem->getProduct();
            $description = !empty($product->getLookupLabel()) ? (string)$product->getLookupLabel() : '';

            $existingZohoInvLineItemsProductLabels[$zohoLineItem->getId()] = strtolower(trim($description));
        }

        $existingZohoInvLineItemsLineItemDescs = [];
        foreach($zohoLineItems as $zohoLineItem){
            $description = $this->parseZohoLineItemDescription($zohoLineItem);
            $existingZohoInvLineItemsLineItemDescs[$zohoLineItem->getId()] = strtolower(trim($description));
        }

        // Remove line items that are not in xero
        $presentLineItemsInXero = [];
        foreach($xeroLineItems as $xeroLineItem){
            $marker = strpos($xeroLineItem->Description, "||") ;
            $xeroLineItemDesc = strtolower(trim(substr($xeroLineItem->Description, ($marker != false) ? $marker + 2 : 0)));
            if(in_array($xeroLineItemDesc, $existingZohoInvLineItemsProductLabels)){
                $presentLineItemsInXero[]=array_search($xeroLineItemDesc, $existingZohoInvLineItemsProductLabels);
            }

            if(in_array($xeroLineItemDesc, $existingZohoInvLineItemsLineItemDescs)){
                $presentLineItemsInXero[]=array_search($xeroLineItemDesc, $existingZohoInvLineItemsLineItemDescs);
            }
        }

        foreach($zohoLineItems as $zohoLineItem){
            $lineItemId = $zohoLineItem->getId();
            if(!in_array($lineItemId, $presentLineItemsInXero)){
                $invCrmRecIns->removeLineItem($lineItemId);
            }
        } // End remove of line item that are not in xero

        $lineAmountTaxTypes = $accReceivable->LineAmountTypes;

        // Process each xero lineitems
        foreach($xeroLineItems as $xeroLineItem){
            //Gets the description after the || character
            $marker = strpos($xeroLineItem->Description, "||") ;
            $xeroLineItemName = trim(substr($xeroLineItem->Description, ($marker != false) ? $marker + 2 : 0));

            $this->singleWordDiscount = false;
            $this->singleWordTax = false;

            // Don't add Rounding line item from xero
            if(!empty($xeroLineItemName)){
                if(strtolower(trim($xeroLineItemName)) == 'rounding'){
                    continue;
                }
            }

            //Skips through empty quantity items as this is a mandatory field on zoho
            if(!isset($xeroLineItem->Quantity) || empty((float)$xeroLineItem->Quantity)){
                continue;
            }

            $productId = '';

            // Get product in zoho
            $searchByProductName = $this->searchServiceByName(($marker != false) ? trim(substr($xeroLineItem->Description, 0 , $marker)) : $xeroLineItemName);
            if (!empty($searchByProductName) && $searchByProductName != 'No match found') {
                $productId = $searchByProductName;
            } else {

                $xeroLineItemNameLowerCase = $this->sanitizeData(trim(strtolower($xeroLineItemName)));
		        //Makes sure that only characters present on the keyboard are accepted
                $xeroLineItemNameLowerCase = preg_replace('/[^a-zA-Z0-9,\.\/<>\?;\': ""[\]\\{}\|`~!@#\$%\^&\*()-_=\+\n]*/', '', $xeroLineItemNameLowerCase);
                // Check if product is recognised
                $recognisedProductId = DB::table('recognised_match_products')->select('zoho_product_id')
                    ->whereRaw("TRIM(LOWER(product_name)) = '{$xeroLineItemNameLowerCase}'")
                    ->first();

                if(!empty($recognisedProductId->zoho_product_id)){
                    $productId = $recognisedProductId->zoho_product_id;
                }else{
                    // If not recognised check if in single word mapping
                    $singleWordProduct = $this->queryInSingleWordMapping($xeroLineItemNameLowerCase);
                    if(!empty($singleWordProduct)){
                        switch($singleWordProduct->mapping_word){
                            case 'Discount':
                                $this->singleWordDiscount = true;
                                break;
                            case 'Tax':
                                $this->singleWordTax = true;
                                break;
                        }
                        $productId = $singleWordProduct->zoho_product_id;
                    }else{
                        // If not in single word mapping fall back to default which is miscellaneous
                        $miscProductId = DB::table('single_word_products')->select('zoho_product_id')
                            ->whereRaw("LOWER(mapping_word) = 'miscellaneous'")
                            ->first();
                        if(!empty($miscProductId->zoho_product_id)){
                            $productId = $miscProductId->zoho_product_id;
                        }
                    }
                }
            }

            // Actual processing/mapping of xero invoice lineitem to zoho lineitem
            if(!empty($productId)){
                //Converts the product id from string to a natural numeric data
                $productInstance = \ZCRMRecord::getInstance("Products", $productId + 0);

                $lineItem = \ZCRMInventoryLineItem::getInstance($productInstance);

                // Zoho API version does not have an ability to update the line item without removing it first
                // Remove first line items that has same product name from xero
                foreach ($existingZohoInvLineItemsProductLabels as $id => $zohoLineItemName) {
                    if (strtolower(trim($zohoLineItemName)) == strtolower(trim($xeroLineItemName))) {
                        $invCrmRecIns->removeLineItem($id);
                        $lineItem->setId($id);
                    }
                }

                foreach ($existingZohoInvLineItemsLineItemDescs as $id => $zohoLineItemName) {
                    if (strtolower(trim($zohoLineItemName)) == strtolower(trim($xeroLineItemName))) {
                        $invCrmRecIns->removeLineItem($id);
                        $lineItem->setId($id);
                    }
                } // End removal of line items

                if($this->singleWordTax == true){
                    $lineItem->setTaxAmount(!empty($xeroLineItem->UnitAmount) ? (double) $xeroLineItem->UnitAmount : null);
                    $lineItem->setQuantity(1.00);
                    $lineItem->setListPrice(0.00);
                    $lineItem->setTotal(0.00);
                }

                if($this->singleWordDiscount == true){
                    if(!empty($xeroLineItem->UnitAmount) && (double) $xeroLineItem->UnitAmount > 0){
                        $lineItem->setDiscount(!empty($xeroLineItem->UnitAmount) ? (double) $xeroLineItem->UnitAmount : null);
                    }
                    $lineItem->setQuantity(1.00);
                    $lineItem->setListPrice(!empty($xeroLineItem->UnitAmount) ? (double) $xeroLineItem->UnitAmount : null);
                    $lineItem->setTotal(0.00);
                }

                if(empty($this->singleWordDiscount) && empty($this->singleWordTax)){
                    $discountRate = !empty($xeroLineItem->DiscountRate) ? (double)$xeroLineItem->DiscountRate : 0;
                    $quantity = !empty($xeroLineItem->Quantity) ? (double)$xeroLineItem->Quantity : 0.00;
                    $taxAmount = !empty($xeroLineItem->TaxAmount) ? $xeroLineItem->TaxAmount : 0.00;
                    $lineAmount = !empty($xeroLineItem->LineAmount) ? $xeroLineItem->LineAmount : 0.00;
                    $discountAmount = 0;
                    $taxType = !empty($xeroLineItem->TaxType) ? $xeroLineItem->TaxType : '';
                    $taxDetails = $this->getLineItemTaxDetails($taxType);

                    // If xero tax rate is not found in the zoho tax rate mapping table then halt the sync process for the current invoice
                    if(empty($taxDetails->zoho_tax_name) && !empty($taxType)){
                        $this->log['fatal_error']['invoice'][] = 'Syncing stopped. Xero Invoice line item tax rate not found in zoho tax rate mapping.';
                        return false;
                    }

                    $zohoTaxName = !empty($taxDetails->zoho_tax_name) ? $taxDetails->zoho_tax_name : 'GST Free Income';
                    $zohoTaxValue = !empty($taxDetails->zoho_tax_value) ? $taxDetails->zoho_tax_value : 0;

                    if (strtolower($lineAmountTaxTypes) == 'inclusive') { // Computations for tax inclusive invoice
                        $xeroDiscountRate = !empty($xeroLineItem->DiscountRate) ? $xeroLineItem->DiscountRate : 0;
                        $xeroUnitAmount = !empty($xeroLineItem->UnitAmount) ? (double)$xeroLineItem->UnitAmount : 0;

                        if (!empty($xeroDiscountRate) && !empty($quantity) && !empty($xeroUnitAmount)) {
                            $discountAmount = round(((($xeroUnitAmount * $quantity) * ($xeroDiscountRate / 100)) / (100 + $zohoTaxValue)) * 100, 2);
                        }

                        if (!empty($quantity) && !empty($xeroUnitAmount)) {
                            $listPrice = ($lineAmount - $taxAmount + $discountAmount) / $quantity;
                        } else {
                            if(!empty($lineAmount)){
                                $listPrice = ($lineAmount - $taxAmount + $discountAmount);
                            }else{ // If there is no lineAmount then we cant compute properly the listprice so set it to zero
                                $listPrice = 0.00;
                            }
                        }

                        // Handling for tradegecko issue in xero line item having OUTPUT tax rate but tax is 0 value
                        // Override tax details and list price when xero line item is GST(OUTPUT) but tax has 0 value
                        if(empty((double)$taxAmount) && $taxType == 'OUTPUT'){
                            $taxDetails = $this->getLineItemTaxDetails('EXEMPTOUTPUT');
                            $zohoTaxName = !empty($taxDetails->zoho_tax_name) ? $taxDetails->zoho_tax_name : 'GST Free';
                            $zohoTaxValue = !empty($taxDetails->zoho_tax_value) ? $taxDetails->zoho_tax_value : 0;
                            $listPrice = !empty($xeroLineItem->UnitAmount) ? (double) $xeroLineItem->UnitAmount : 0.00;
                        }

                        if((double)$taxAmount != 0 && is_numeric($taxAmount) && $taxType == 'EXEMPTOUTPUT'){
                            $taxDetails = $this->getLineItemTaxDetails('OUTPUT');
                            $zohoTaxName = !empty($taxDetails->zoho_tax_name) ? $taxDetails->zoho_tax_name : 'GST Free';
                            $zohoTaxValue = !empty($taxDetails->zoho_tax_value) ? $taxDetails->zoho_tax_value : 0;
                            $listPrice = !empty($xeroLineItem->UnitAmount) ? (double) $xeroLineItem->UnitAmount : 0.00;
                        } // Handling for tradegecko issue end

                        $lineItem->setQuantity($quantity);
                        $lineItem->setListPrice(round($listPrice, 2));
                        $lineItem->setDiscount((double)$discountAmount);
                        $lineItem->setDiscountPercentage($discountRate);

                        $taxInstance = \ZCRMTax::getInstance($zohoTaxName);
                        $taxInstance->setPercentage($zohoTaxValue);
                        $taxInstance->setValue((double) $taxAmount);
                        $lineItem->addLineTax($taxInstance);
                        $lineItem->setTaxAmount((double)$taxAmount);
                    } else { // Tax Exclusive computations
                        $listPrice = !empty($xeroLineItem->UnitAmount) ? (double) $xeroLineItem->UnitAmount : 0.00;
                        $total = !empty($xeroLineItem->LineAmount) ? (double) $xeroLineItem->LineAmount : 0.00;

                        // Handling for tradegecko issue in xero line item having OUTPUT tax rate but tax is 0 value
                        // Override tax details and list price when xero line item is GST(OUTPUT) but tax has 0 value
                        if((double)$taxAmount == 0 && $taxType == 'OUTPUT'){
                            $taxDetails = $this->getLineItemTaxDetails('EXEMPTOUTPUT');
                            $zohoTaxName = !empty($taxDetails->zoho_tax_name) ? $taxDetails->zoho_tax_name : 'GST Free';
                            $zohoTaxValue = !empty($taxDetails->zoho_tax_value) ? $taxDetails->zoho_tax_value : 0;
                            $listPrice = !empty($xeroLineItem->UnitAmount) ? (double) $xeroLineItem->UnitAmount : 0.00;
                        }

                        if((double)$taxAmount != 0 && is_numeric($taxAmount) && $taxType == 'EXEMPTOUTPUT'){
                            $taxDetails = $this->getLineItemTaxDetails('OUTPUT');
                            $zohoTaxName = !empty($taxDetails->zoho_tax_name) ? $taxDetails->zoho_tax_name : 'GST Free';
                            $zohoTaxValue = !empty($taxDetails->zoho_tax_value) ? $taxDetails->zoho_tax_value : 0;
                            $listPrice = !empty($xeroLineItem->UnitAmount) ? (double) $xeroLineItem->UnitAmount : 0.00;
                        } // Handling for tradegecko issue end

                        $lineItem->setQuantity(!empty($xeroLineItem->Quantity) ? (double) $xeroLineItem->Quantity : 0.00);
                        $lineItem->setListPrice($listPrice);
                        $lineItem->setDiscountPercentage(!empty($xeroLineItem->DiscountRate) ? (double) $xeroLineItem->DiscountRate : 0.00);

                        $unitAmount = !empty($xeroLineItem->UnitAmount) ? (double) $xeroLineItem->UnitAmount : 0.00;
                        $discountRate = !empty($xeroLineItem->DiscountRate) ? (double) $xeroLineItem->DiscountRate : 0.00;
                        $discountAmount = $unitAmount * ($discountRate * 0.01);

                        $lineItem->setDiscount((double) $discountAmount);
                        $lineItem->setTotal($total);
                        $lineItem->setTotalAfterDiscount(!empty($xeroLineItem->LineAmount) ? (double) $total : 0.00);
                        $taxInstance = \ZCRMTax::getInstance($zohoTaxName);
                        $taxInstance->setPercentage($zohoTaxValue);
                        $taxInstance->setValue((double) $taxAmount);
                        $lineItem->addLineTax($taxInstance);
                        $lineItem->setTaxAmount((double)$taxAmount);
                    }
                }

                //Gets the description after the || character
                $marker = strpos($xeroLineItem->Description, "||") ;
                $description = !empty($xeroLineItem->Description) ? trim(substr($xeroLineItem->Description, ($marker != false) ? $marker + 2 : 0)) : '';
                $lineItem->setDescription($description);

                $invCrmRecIns->addLineItem($lineItem);
            }
        }

        return $invCrmRecIns;
    }

    /**
     * Query for tax type details
     * @param $taxType
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    private function getLineItemTaxDetails($taxType){
        $taxType = $this->sanitizeData($taxType);
        $taxDetails= DB::table('xero_to_zoho_tax_rate_mapping')
            ->select('zoho_tax_name', 'zoho_tax_value')
            ->whereRaw("LOWER(`xero_tax_code`) = '$taxType'")
            ->first();

        return $taxDetails;
    }


    /**
     * Process company state
     * @param $poBoxAddress
     * @return mixed|string
     */
    private function processCompanyState($poBoxAddress){
        if(empty($poBoxAddress->Region)){
            return '';
        }

        // Check first country if its not empty and try to find out the region using the country
        if(!empty($poBoxAddress->Country)){
            $poBoxAddressCountryLowerCase = !empty($poBoxAddress->Country) ? $this->sanitizeData(strtolower($poBoxAddress->Country)) : '';
            $countryIsMapped =  $regionIsCountry = DB::table('countries')
                ->whereRaw("LOWER(`country`) = '{$poBoxAddressCountryLowerCase}'")
                ->exists();

            if($countryIsMapped == true){
                $country = DB::table('countries')
                    ->select('region')
                    ->whereRaw("LOWER(`country`) = '{$poBoxAddressCountryLowerCase}'")
                    ->first();

                return $country->region;
            }
        }

        $poBoxAddressRegionLowerCase = !empty($poBoxAddress->Region) ? $this->sanitizeData(strtolower($poBoxAddress->Region)) : null;
        $regionIsCountry = DB::table('countries')
            ->whereRaw("LOWER(`country`) = '{$poBoxAddressRegionLowerCase}'")
            ->exists();

        if($regionIsCountry == true){
            return !empty($poBoxAddress->City) ? $poBoxAddress->City : '';
        }else{
            $regionIsAbbreviated = DB::table('states')
                ->whereRaw("LOWER(`abbreviation`) = '{$poBoxAddressRegionLowerCase}'")
                ->exists();
            if($regionIsAbbreviated == true){
                $state = DB::table('states')->select('state')
                    ->whereRaw("LOWER(`abbreviation`) = '{$poBoxAddressRegionLowerCase}'")
                    ->first();
                return $state->state;
            } else {
                return $poBoxAddress->Region;
            }
        }
    }

    /**
     * Check if the xero item name exist in single word mapping
     * @param $xeroItemName
     * @return bool|mixed
     */
    private function queryInSingleWordMapping($xeroItemName){
        $singleWordMappings = DB::table('single_word_products')->select('mapping_word', 'zoho_product_id')->orderBy('id')->get();
        foreach($singleWordMappings as $product){
            $keyword = trim($product->mapping_word);
            if(stripos($xeroItemName, $keyword) !== false){
                return $product;
            }
        }

        return false;
    }

    /**
     * Process company country
     *
     * @param $poBoxAddress
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|mixed|null|object|string
     */
    private function processCompanyCountry($poBoxAddress){
        $poBoxAddressRegionLowerCase = !empty($poBoxAddress->Region) ? $this->sanitizeData(strtolower($poBoxAddress->Region)) : '';
        $poBoxAddressCityLowerCase = !empty($poBoxAddress->City) ? $this->sanitizeData(strtolower($poBoxAddress->City)) : '';
        $poBoxAddressCountryLowerCase = !empty($poBoxAddress->Country) ? $this->sanitizeData(strtolower($poBoxAddress->Country)) : '';

        if(empty($poBoxAddress->Country)){
            $cityIsCountry = DB::table('countries')
                ->whereRaw("LOWER(`country`) = '{$poBoxAddressCityLowerCase}' OR LOWER(`abbreviation`) = '{$poBoxAddressCityLowerCase}'")
                ->exists();

            $regionIsCountry = DB::table('countries')
                ->whereRaw("LOWER(`country`) = '{$poBoxAddressRegionLowerCase}'")
                ->exists();

            if($cityIsCountry == true){
                $country = DB::table('countries')
                    ->select('country')
                    ->whereRaw("LOWER(`country`) = '{$poBoxAddressCityLowerCase}' OR LOWER(`abbreviation`) = '{$poBoxAddressCityLowerCase}'")
                    ->first();
                return $country->country;
            }

            if($cityIsCountry == false && $regionIsCountry == true){
                return $poBoxAddress->Region;
            }

            return '';
        } else {
            $countryIsAbbreviated = DB::table('countries')
                ->whereRaw("LOWER(`abbreviation`) = '{$poBoxAddressCountryLowerCase}'")
                ->exists();
            if($countryIsAbbreviated == true){
                $country = DB::table('countries')->select('country')
                    ->whereRaw("LOWER(`abbreviation`) = '{$poBoxAddressCountryLowerCase}'")
                    ->first();
                $country = !empty($country->country) ? $country->country : '';
                return $country;
            } else {
                $country = !empty($poBoxAddress->Country) ? $poBoxAddress->Country : '';
                return $country;
            }
        }
    }

    /**
     * Update zoho invoice as deleted
     * @param $accReceivable
     * @return bool
     */
    private function markZohoInvoiceAsDeletedOrVoided($accReceivable){
        try {
            $apInvoiceIns = \ZCRMModule::getInstance("Invoices");
            $criteria = "(Invoice_No1:equals:{$accReceivable->InvoiceNumber})";
            $invoiceQueryResponse = $apInvoiceIns->searchRecordsByCriteria($criteria);
            $record = $invoiceQueryResponse->getResponseJSON();
            if (!empty($record['data'][0]['id'])) {
                $invoiceId = $record['data'][0]['id'];
            }

            $existingInvoice = \ZCRMRecord::getInstance("Invoices", $invoiceId);
            $existingInvoice->setFieldValue('Xero_Invoice_Status', $this->parseInvoiceStatus($accReceivable->Status));
            $existingInvoice->update();
            DB::table('zoho_invoice_update_timestamps')->insert(['invoice_number' => $accReceivable->InvoiceNumber]);
        } catch (\Exception $e) {
            if ($e->getMessage() == 'No Content') {
                return false;
            }
        }
        return true;
    }

    /**
     * Parse xero invoice status
     * @param $invoiceStatus
     * @return string
     */
    private function parseInvoiceStatus($invoiceStatus){
        $invoiceStatus = strtolower($invoiceStatus);
        switch ($invoiceStatus){
            case 'draft':
                $status = 'Draft';
                break;
            case 'submitted':
                $status = 'Awaiting Approval';
                break;
            case 'authorised':
                //$status = 'Approved';
                $status = 'Approved & Sent';
                break;
            case 'paid':
                $status = 'Paid';
                break;
            case 'voided':
                $status = 'Voided';
                break;
            case 'deleted':
                $status = 'Deleted';
                break;
            default:
                $status = '';
                break;
        }

        return $status;
    }

    /**
     * Format date to ymd
     * @param $date
     * @return string
     */
    private function formatDateToYmd($date){
        if(empty($date)){
            return '';
        }
        $dateTime = new \DateTime($date);
        return $dateTime->format('Y-m-d');
    }

    /**
     * Format search criteria
     * @param $content
     * @return string
     */
    private function addSlashToSearchCriteria($content){
        // add slashes to "()" characters since it results exception and records cannot be found in zoho
        $pattern = "/[\(\)]/i";
        $new_content = '';

        for($i = 0; $i < strlen($content); $i++) {
            if(preg_match($pattern, $content[$i])) {
                $new_content .= '\\' . $content[$i];
            } else {
                $new_content .= $content[$i];
            }
        }

        return $new_content;
    }

    /**
     * Get zoho line item description
     * Zoho line item description first line should be the product name
     * If users wish to add something they should add in the 2nd line and so forth
     * @param $zohoLineItem
     * @return string
     */
    private function parseZohoLineItemDescription($zohoLineItem){
        $productDescription = $zohoLineItem->getDescription();

        $productDescExploded = explode("\n", $productDescription);

        // First line should be the product name to xero
        $productName = !empty($productDescExploded[0]) ? $productDescExploded[0] : '';

        return $productName;
    }

    private function sanitizeData($data){
        if($data != ""){
            $data = addslashes($data);
            return $data;
        }

        return $data;
    }
}