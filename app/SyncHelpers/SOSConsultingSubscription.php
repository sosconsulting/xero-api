<?php
namespace SOSZohoXeroIntegration\SyncHelpers;

use Doctrine\Common\Cache\ApcuCache;
use Zoho\Subscription\Client;

class SOSConsultingSubscription extends Client\Client
{
    public function __construct(){
        $token = config('sync.zoho.subscription_oauth_token');
        $organizationId = config('sync.zoho.organization_id');
        $cache = new ApcuCache();
        parent::__construct($token, $organizationId, $cache);
    }

    public function request($uri, $method){
        $request = parent::sendRequest($method,$uri);
        try {
            $response = parent::processResponse($request);
            return $response;
        } catch (\Exception $e) {

        }
        return false;
    }
}