<?php

namespace SOSZohoXeroIntegration\SyncHelpers;

require 'vendor/autoload.php';

use SOSZohoXeroIntegration;
use XeroPHP\Models\Accounting\Address;
use XeroPHP\Models\Accounting\Contact;
use XeroPHP\Models\Accounting\Invoice;
use XeroPHP\Models\Accounting\Phone;
use XeroPHP\Models\Accounting\TrackingCategory;
use Illuminate\Support\Facades\DB;


class ZohoToXeroAccountsReceivable
{
    private $focus = '';
    private $xero = null;
    private $batchNumber = null;
    private $log = [];


    public function processData($data, $xero)
    {
        \ZCRMRestClient::initialize();

        $accReceivable = json_decode($data->record_details);

        $this->focus = $data;
        $this->xero = $xero;
        $this->batchNumber = $data->batch_number;

        $invoiceQuery =  DB::table('zoho_to_xero_xero_invoices')->select('invoice_id')->where('batch_number', $this->batchNumber)->where('invoice_number', $data->invoice_number)->first();

        if ($accReceivable->Status == 'Deleted' || $accReceivable->Status == 'Voided') {
            $this->markXeroInvoiceAsDeletedOrVoided($accReceivable, $invoiceQuery);
            return $this->log;
        }

        if(!empty($accReceivable->Invoice_Sync) && $accReceivable->Invoice_Sync == 'Disabled'){
            $this->log['fatal_error']['invoice'][] = 'Will not sync this record. Invoice Sync is disabled.';
            return $this->log;
        }

        //Patch fix to make sure that the syncapp doesn't stop because the accoutn name is missing
        if(empty($accReceivable->Account_Name) || empty($accReceivable->Account_Name->id)){
            $this->log['fatal_error']['invoice'][] = 'Missing account name on the invoice. Invoice not Processed.';
            return $this->log;
        }

        // Validate if this is a force resync from xero to zoho
        $forceResync = $accReceivable->Force_Resync;
        if (!empty(strtolower($forceResync) && strtolower($forceResync) == 'xero to zoho') && !empty($invoiceQuery)) {
            $this->triggerXeroToZohoQueue($invoiceQuery, $accReceivable);
            return $this->log;
        }

        if(empty($invoiceQuery)){
            $this->createNewInvoice($accReceivable);
        }else{
            $this->updateExistingInvoice($invoiceQuery, $accReceivable);
        }

        return $this->log;
    }

    private function updateExistingInvoice($existingXeroInvoice, $accReceivable){
        $xeroInvoice = $this->xero->loadByGUID(Invoice::class, $existingXeroInvoice->invoice_id);

        $contactId = $this->processContact($accReceivable);

        if(!empty($contactId)){
            $contact = $this->xero->loadByGUID(Contact::class, $contactId);
            $xeroInvoice->setContact($contact);
        }

        $xeroInvoice->setInvoiceNumber($accReceivable->Invoice_No1);
        if(!empty($accReceivable->Invoice_Date)){
            $xeroInvoice->setDate($this->getProperDate($accReceivable->Invoice_Date));
        }
        if(!empty($accReceivable->Due_Date)){
            $xeroInvoice->setDueDate($this->getProperDate($accReceivable->Due_Date));
        }
        if(!empty($accReceivable->Currency)){
            $xeroInvoice->setCurrencyCode($accReceivable->Currency);
        }

        $status = $this->processStatus($accReceivable->Xero_Invoice_Status);
        $xeroInvoice->setStatus($status);

        // Xero line Items
        $xeroLineItemsQuery = $xeroInvoice->getLineItems();
        $xeroLineItems = $this->parseData($xeroLineItemsQuery);

        // Zoho Line Items
        try{
            $invCrmRecIns = \ZCRMRecord::getInstance('Invoices', $accReceivable->id);
            $invEntityApi = \EntityAPIHandler::getInstance($invCrmRecIns);
            $invoiceRecQuery = $invEntityApi->getRecord();
        }catch(\Exception $e){
            $this->log['fatal_error']['invoice'][] = 'Record not processed. Error: '. $e->getMessage();
            return $this->log;
        }

        $zohoLineItems = $invCrmRecIns->getLineItems();
        $zohoTaxType = !empty($invCrmRecIns->getFieldValue('Xero_Tax_Type')) ? $invCrmRecIns->getFieldValue('Xero_Tax_Type') : '';

        if(!empty($zohoLineItems)){

            $existingXeroLineItemIndex = [];
            foreach($xeroLineItems as $key => $xeroLineItem){
                $xeroLineItemDescription = $xeroLineItem['Description'];
                if(empty($xeroLineItemDescription)){
                    continue;
                }
                $existingXeroLineItemIndex[strtolower($xeroLineItemDescription)] = $key;
            }

            $lineItems = $this->updateXeroLineItems($xeroLineItems, $zohoLineItems, $zohoTaxType);

            if(!empty($lineItems)){
                //Remove first existing invoices
                foreach($lineItems as $lineItem){
                    $itemName = !empty($lineItem['Description']) ? strtolower($lineItem['Description']) : '';
                    foreach ($existingXeroLineItemIndex as $item => $index){
                        if($itemName == $item){
                            $xeroInvoice->removeLineItem($index);
                        }

                        // Remove first the rounding in xero
                        if(strtolower($item) == 'rounding'){
                            $xeroInvoice->removeLineItem($index);
                        }
                    }
                }

                foreach($lineItems as $lineItem){
                    $xeroLineItem = new Invoice\LineItem();
                    $xeroLineItem->setDescription($lineItem['Description']);
                    $xeroLineItem->setQuantity($lineItem['Quantity']);
                    $xeroLineItem->setUnitAmount($lineItem['UnitAmount']);
                    $xeroLineItem->setTaxType($lineItem['TaxRate']);
                    $xeroLineItem->setTaxAmount($lineItem['TaxAmount']);
                    $xeroLineItem->setDiscountRate($lineItem['DiscountRate']);
                    $xeroLineItem->setAccountCode($lineItem['AccountCode']);
                    $xeroLineItem->setLineItemID($lineItem['LineItemID']);
                    $xeroLineItem->setItemCode($lineItem['ItemCode']);

                    foreach($lineItem['Tracking'] as $tracker){
                        $xeroLineItem->addTracking($tracker);
                    }

                    $xeroInvoice->addLineItem($xeroLineItem);
                }
            }
        }

        try {
            $invoiceUpdate = $xeroInvoice->save();
            DB::table('xero_invoice_update_timestamps')->insert(['invoice_number' => $accReceivable->Invoice_No1]);
            if($invoiceUpdate->getStatus() == 200){
                $this->log['info']['invoice'][] = "Xero invoice is successfully updated in Xero with with Invoice Number: {$accReceivable->Invoice_No1}";
            }
        } catch (\Exception $exception) {
            $this->log['error']['invoice'][] = "Encountered an error while updating the invoice in Xero, see details: {$exception->getMessage()}";
        }

        // Add rounding line item to match xero total from zoho total
        $xeroGrandTotal = round($xeroInvoice->getTotal(), 2);
        $zohoGrandTotal = round($invCrmRecIns->getFieldValue('Grand_Total'), 2);

        if($zohoGrandTotal != $xeroGrandTotal){
            $roundingUnitPrice = round($zohoGrandTotal - $xeroGrandTotal, 2);

            // Add rounding line item to level xero total from zoho total
            $xeroLineItem = new Invoice\LineItem();
            $xeroLineItem->setDescription('Rounding');
            $xeroLineItem->setQuantity(1);
            $xeroLineItem->setUnitAmount(round($roundingUnitPrice,2));
            $xeroLineItem->setAccountCode('9010');
            $xeroLineItem->setTaxType('BASEXCLUDED');
            $xeroInvoice->addLineItem($xeroLineItem);
        } // End round handling

        $this->checkIfForceUpdateZohoToXero($accReceivable);
    }

    private function updateXeroLineItems($xeroLineItems, $zohoLineItems, $zohoTaxType){
        $accReceivable = json_decode($this->focus->record_details);

        $xeroLineItemNames = [];
        foreach($xeroLineItems as $xeroLineItem){
            //Mapping fields only found on xero
            $xeroLineItemNames[strtolower($xeroLineItem['Description'])] = array(
                "LineItemID" => !empty($xeroLineItem['LineItemID']) ? $xeroLineItem['LineItemID'] : '',
                "ItemCode" => !empty($xeroLineItem['ItemCode']) ? $xeroLineItem['ItemCode'] : '',
                "Tracking" => !empty($xeroLineItem['Tracking']) ? $xeroLineItem['Tracking'] : array(),
            );
        }

        $forSyncLineItems = [];
        foreach($zohoLineItems as $zohoLineItem){

            $lineItemID = $itemCode = null;
            $trackingCategory = array();
            $product = $zohoLineItem->getProduct();

            if (!empty($accReceivable->From_Xero)) {
                if (empty($zohoLineItem->getDescription())) {
                    $description = empty($product->getLookupLabel()) ? (string)$product->getLookupLabel() : null;
                } else {

                    foreach($xeroLineItems as $xeroLineItem){

                        if(strpos($xeroLineItem['Description'], "||") !== false){
                            $zohoDescription = $this->parseZohoLineItemDescription($zohoLineItem);
                            $xeroDescription = substr($xeroLineItem['Description'], strpos($xeroLineItem['Description'], "||") + 2);
                            if(trim(strtolower($xeroDescription)) == trim(strtolower($zohoDescription))){
                                $description = !empty($product->getLookupLabel()) ? (string)$product->getLookupLabel() : null;
                                //Adds a marker to both include the label and description
                                if(!empty($zohoLineItem->getDescription())){
                                    $description .= !empty($description) ? " || " . $this->parseZohoLineItemDescription($zohoLineItem) : $this->parseZohoLineItemDescription($zohoLineItem);
                                }

                            }

                        }else{
                            if(trim(strtolower($xeroLineItem['Description'])) == trim(strtolower($zohoLineItem->getDescription()))){
                                $description = trim($xeroLineItem['Description']);
                            }
                        }
                    }
                    $description = !empty($description) ? $description : $this->parseZohoLineItemDescription($zohoLineItem);
                    $description = !empty($description) ? $description : $product->getLookupLabel();
                }
            } else {
                $description = !empty($product->getLookupLabel()) ? (string)$product->getLookupLabel() : null;
                //Adds a marker to both include the label and description
                if(!empty($zohoLineItem->getDescription())){
                    $description .= !empty($description) ? " || " . $this->parseZohoLineItemDescription($zohoLineItem) : $this->parseZohoLineItemDescription($zohoLineItem);
                }

                if (!empty($description) && strtolower($product->getLookupLabel()) == 'miscellaneous') {
                    $description = $this->parseZohoLineItemDescription($zohoLineItem);
                    $description = !empty($description) ? $description : $product->getLookupLabel();
                }
            }

            if (strtolower(trim($zohoTaxType)) == 'tax inclusive') {
                $quantity = !empty($zohoLineItem->getQuantity()) ? (float)$zohoLineItem->getQuantity() : null;

                $taxAmount = !empty($zohoLineItem->getTaxAmount()) ? (float)$zohoLineItem->getTaxAmount() : null;
                $lineAmount = !empty($zohoLineItem->getTotal()) ? (float)$zohoLineItem->getTotal() : null;
                $zohoListPrice = !empty($zohoLineItem->getListPrice()) ? (float)$zohoLineItem->getListPrice() : 0;
                $discount = $zohoLineItem->getDiscount();
                $discountRate = !empty($discount) ? $this->calculateDiscountRate($discount, $lineAmount) : null;

                if($taxAmount > 0){
                    $taxableAmount = $lineAmount - $discount;
                    $unitAmount = round(((($taxAmount/$taxableAmount)+1) * $zohoListPrice),2);
                }else{
                    $unitAmount = $zohoListPrice;
                }

                $accountCode = $this->parseAccountCode($product);
            } else {
                $quantity = !empty($zohoLineItem->getQuantity()) ? (float)$zohoLineItem->getQuantity() : null;
                $unitAmount = !empty($zohoLineItem->getListPrice()) ? (float)$zohoLineItem->getListPrice() : 0;
                $taxAmount = !empty($zohoLineItem->getTaxAmount()) ? (float)$zohoLineItem->getTaxAmount() : null;
                $lineAmount = !empty($zohoLineItem->getTotal()) ? (float)$zohoLineItem->getTotal() : null;
                $discount = $zohoLineItem->getDiscount();
                $discountRate = !empty($discount) ? $this->calculateDiscountRate($discount, $lineAmount) : null;

                $accountCode = $this->parseAccountCode($product);
            }

            $taxType = $this->parseTaxType($zohoLineItem);

            // Handling for tradegecko issue in xero line item having OUTPUT tax rate but tax is 0 value
            // Override tax details and list price when xero line item is GST(OUTPUT) but tax has 0 value
            if((double)$taxAmount == 0 && $taxType == 'OUTPUT'){
                $taxType = 'EXEMPTOUTPUT';
                $taxAmount = null;
            }

            if((double)$taxAmount != 0 && is_numeric($taxAmount) && $taxType == 'EXEMPTOUTPUT'){
                $taxType = 'OUTPUT';
            } // End handling for tradegecko issue

            if(!empty($description) && array_key_exists(strtolower($description), $xeroLineItemNames)){
                $lineItemID = $xeroLineItemNames[strtolower($description)]['LineItemID'];
                $itemCode = $xeroLineItemNames[strtolower($description)]['ItemCode'];
                $trackingCategory = $this->setTracker($xeroLineItemNames[strtolower($description)]['Tracking']);
            }

            $forSyncLineItems[] = [
                'Description' => $description,
                'Quantity' => $quantity,
                'UnitAmount' => $unitAmount,
                'TaxAmount' => $taxAmount,
                'DiscountRate' => $discountRate,
                'LineItemID' => $lineItemID,
                'ItemCode' => $itemCode,
                'Tracking' => $trackingCategory,
                'AccountCode' => $accountCode,
                'TaxRate' => $taxType
            ];
        }

        return $forSyncLineItems;
    }

    private function setTracker($trackerList)
    {
        if(empty($trackerList))
            return $trackerList;

        $forSyncTracker = array();
        foreach($trackerList as $tracker){
            $trackingCategory = new TrackingCategory();
            $trackingCategory->setTrackingCategoryID($tracker['TrackingCategoryID']);
            $trackingCategory->setName($tracker['Name']);
            $trackingCategory->setOption($tracker['Option']);

            $forSyncTracker[] = $trackingCategory;
        }
        return $forSyncTracker;
    }

    private function createNewInvoice($accReceivable ){
        $contactId = $this->processContact($accReceivable);

        // This executes when this is a resync due to some errors
        if(!empty($this->focus->xero_invoice_id)){
            $existingXeroInvoice = (object)['invoice_id' => $this->focus->xero_invoice_id];
            $this->updateExistingInvoice($existingXeroInvoice, $accReceivable);
            return;
        }

        $newInvoice = new Invoice($this->xero);
        $newInvoice->setType('ACCREC');
        if(!empty($contactId)){
            $contact = $this->xero->loadByGUID(Contact::class, $contactId);
            $newInvoice->setContact($contact);
        }
        $newInvoice->setInvoiceNumber($accReceivable->Invoice_No1);
        if(!empty($accReceivable->Invoice_Date)){
            $newInvoice->setDate($this->getProperDate($accReceivable->Invoice_Date));
        }
        if(!empty($accReceivable->Due_Date)){
            $newInvoice->setDueDate($this->getProperDate($accReceivable->Due_Date));
        }
        if(!empty($accReceivable->Currency)){
            $newInvoice->setCurrencyCode($accReceivable->Currency);
        }

        $status = $this->processStatus($accReceivable->Xero_Invoice_Status);
        $newInvoice->setStatus($status);
        try{
        $invCrmRecIns = \ZCRMRecord::getInstance('Invoices', $accReceivable->id);
            $invEntityApi = \EntityAPIHandler::getInstance($invCrmRecIns);
            $invoiceRecQuery = $invEntityApi->getRecord();
        }catch(\Exception $e){
            $this->log['fatal_error']['invoice'][] = 'Record not processed. Error: '. $e->getMessage();
            return $this->log;
        }

        $zohoLineItems = $invCrmRecIns->getLineItems();

        foreach($zohoLineItems as $zohoLineItem){
            $xeroLineItem = new Invoice\LineItem();
            $product = $zohoLineItem->getProduct();

            if (!empty($accReceivable->From_Xero)) {
                if (empty($zohoLineItem->getDescription())) {
                    $description = empty($product->getLookupLabel()) ? (string)$product->getLookupLabel() : null;
                } else {
                    $description = $this->parseZohoLineItemDescription($zohoLineItem);
                    $description = !empty($description) ? $description : $product->getLookupLabel();
                }
            } else {
                $description = !empty($product->getLookupLabel()) ? (string)$product->getLookupLabel() : null;
                //Adds a marker to both include the label and description
                if(!empty($zohoLineItem->getDescription())) {
                    $description .= !empty($description) ? " || " . $this->parseZohoLineItemDescription($zohoLineItem) : $this->parseZohoLineItemDescription($zohoLineItem);
                }

                if (!empty($description) && strtolower($product->getLookupLabel()) == 'miscellaneous') {
                    $description = $this->parseZohoLineItemDescription($zohoLineItem);
                    $description = !empty($description) ? $description : $product->getLookupLabel();
                }
            }

            $quantity = !empty($zohoLineItem->getQuantity()) ? (string) $zohoLineItem->getQuantity() : null;
            $unitAmount = !empty($zohoLineItem->getListPrice()) ? (float) $zohoLineItem->getListPrice() : 0;
            $taxAmount = !empty($zohoLineItem->getTaxAmount()) ? (float) $zohoLineItem->getTaxAmount() : null;
            $lineAmount = !empty($zohoLineItem->getTotal()) ? (float) $zohoLineItem->getTotal() : null;
            $discount = $zohoLineItem->getDiscount();
            $discountRate = !empty($discount) ? $this->calculateDiscountRate($discount, $lineAmount) : null;

            $xeroLineItem->setDescription($description);
            $xeroLineItem->setQuantity($quantity);
            $xeroLineItem->setDiscountRate($discountRate);
            $xeroLineItem->setTaxAmount($taxAmount);
            $xeroLineItem->setUnitAmount($unitAmount);

            $accountCode = $this->parseAccountCode($product);
            $taxType = $this->parseTaxType($zohoLineItem);

            // Handling for tradegecko issue in xero line item having OUTPUT tax rate but tax is 0 value
            // Override tax details and list price when xero line item is GST(OUTPUT) but tax has 0 value
            if((double)$taxAmount == 0 && $taxType == 'OUTPUT'){
                $taxType = 'EXEMPTOUTPUT';
                $xeroLineItem->setTaxAmount(null);
            }

            if((double)$taxAmount != 0 && is_numeric($taxAmount) && $taxType == 'EXEMPTOUTPUT'){
                $taxType = 'OUTPUT';
            } // End handling for tradegecko issue


            if(!empty($accountCode)){
                $xeroLineItem->setAccountCode($accountCode);
            }

            if(!empty($taxType)){
                $xeroLineItem->setTaxType($taxType);
            }

            $newInvoice->addLineItem($xeroLineItem);
        }

        try {
            $newInvoice = $newInvoice->save();
            DB::table('xero_invoice_update_timestamps')->insert(['invoice_number' => $accReceivable->Invoice_No1]);
            if($newInvoice->getStatus() == 200){
                // Save to db the invoice id in case there is an issue with the syncing and this record will be resync.
                $responseElements = $newInvoice->getElements();
                $invoiceId = $responseElements[0]['InvoiceID'];
                DB::table('zoho_invoice_queue')->where('id', $this->focus->id)->update(['xero_invoice_id' => $invoiceId]);

                $this->log['info']['invoice'][] = "Xero invoice is successfully created in Xero with with Invoice Number: {$accReceivable->Invoice_No1}";
            }
        } catch (\Exception $exception) {
            $this->log['error']['invoice'][] = "Encountered an error while saving the new invoice in Xero, see details: {$exception->getMessage()}";
        }

        $this->checkIfForceUpdateZohoToXero($accReceivable);
    }


    private function parseTaxType($zohoLineItem){
        $zohoLineItemTaxType = '';
        $lineTax = $zohoLineItem->getLineTax();
        if(!empty($lineTax)){
            $zohoLineItemTaxType = array_shift($lineTax)->getTaxName();
        }
        $taxTypeDetails = $this->getLineItemTaxDetails($zohoLineItemTaxType);

        return !empty($taxTypeDetails->xero_tax_code) ? $taxTypeDetails->xero_tax_code : '';
    }

    private function processContact($accReceivable)
    {
        if(!empty($this->focus->xero_contact_id)){
            return $this->focus->xero_contact_id;
        }

        // Check first if the account has already Xero_Contact_ID
        $xeroContactContactId = $this->checkIfXeroContactIdExistInAccount($accReceivable->Account_Name->id);
        if(!empty($xeroContactContactId)){
            return $xeroContactContactId;
        }

        // If account Xero_Contact_ID dont exist then try to check in zoho_to_xero_xero_contacts table if xero contact exist
        $accountName = $this->getZohoAccountCompanyName($accReceivable->Account_Name->id);

        if (!empty($accountName)) {
            $xeroContactContactId = $this->searchExistingXeroContact($accountName);
            if (!empty($xeroContactContactId)) {
                $contactId = $xeroContactContactId;
                DB::table('zoho_invoice_queue')->where('id', $this->focus->id)->update(['xero_contact_id' => $contactId]);
                // $this->updateContact($accReceivable, $contactId);
            } else {
                $contactId = $this->createContact($accReceivable);
            }
        } else {
            $contactId = $this->createContact($accReceivable);
        }

        return $contactId;
    }


    private function getZohoAccountCompanyName($accountId)
    {
        $accountQuery = DB::table('zoho_to_xero_zoho_accounts')->select('record_details')->where('batch_number', $this->batchNumber)->where('account_id', $accountId)->first();

        if (!empty($accountQuery)) {
            $account = json_decode($accountQuery->record_details);
            if (!empty($account->Company_Name)) {
                return $account->Company_Name;
            }
        }

        return false;
    }

    private function checkIfXeroContactIdExistInAccount($accountId){
        if (empty($accountId)) {
            return false;
        }

        $accountQuery = DB::table('zoho_to_xero_zoho_accounts')->select('record_details')->where('batch_number', $this->batchNumber)->where('account_id', $accountId)->first();

        if(!empty($accountQuery)){
            $account = json_decode($accountQuery->record_details);
            if (!empty($account->Xero_Contact_ID)) {
                return $account->Xero_Contact_ID;
            }
        }

        return false;
    }

    private function parseAccountCode($product)
    {
        $accountCode = '41140';

        if (empty($product)) {
            return $accountCode;
        }

        $productId = $product->getEntityId();
        $zohoProductQuery = DB::table('zoho_to_xero_zoho_products')->select('record_details')->where('batch_number', $this->batchNumber)->where('product_id', $productId)->first();

        if (empty($productId) || empty($zohoProductQuery)) {
            return $accountCode;
        }

        $zohoProduct = json_decode($zohoProductQuery->record_details);

        if(empty($zohoProduct->Sales_Account)){
            return $accountCode;
        }else{
            $accountCode = $zohoProduct->Sales_Account;
        }

        return $accountCode;
    }

    private function createContact($accReceivable)
    {
        $contactId = '';
        $queryZohoContact = $this->getZohoContact($accReceivable->Account_Name->id);
        if(!empty($queryZohoContact->record_details)){
            $zohoContact = json_decode($queryZohoContact->record_details);
        }

        $accountQuery = DB::table('zoho_to_xero_zoho_accounts')->select('record_details')->where('batch_number', $this->batchNumber)->where('account_id', $accReceivable->Account_Name->id)->first();


        if(!empty($accountQuery)){
            $zohoAccount = json_decode($accountQuery->record_details);

            $website = !empty($zohoAccount->Website) ? $zohoAccount->Website : '';
            $faxNum = !empty($zohoAccount->Fax) ? $zohoAccount->Fax : '';
            $phoneNum = !empty($zohoAccount->Phone) ? $zohoAccount->Phone : '';

            $phone = new Phone();
            if(!empty($phoneNum)){
                $phone->setPhoneType($phone::PHONE_TYPE_DEFAULT);
                $phone->setPhoneNumber($phoneNum);
            }

            $fax = new Phone();
            if(!empty($faxNum)){
                $fax->setPhoneType($fax::PHONE_TYPE_FAX);
                $fax->setPhoneNumber($faxNum);
            }


            $billingStreet = !empty($zohoAccount->Billing_Street) ? $zohoAccount->Billing_Street : '';
            $billingCity = !empty($zohoAccount->Billing_City) ? $zohoAccount->Billing_City : '';
            $billingRegion = !empty($zohoAccount->Billing_State) ? $zohoAccount->Billing_State : '';
            $billingPostalCode = !empty($zohoAccount->Billing_Code) ? $zohoAccount->Billing_Code : '';
            $billingCountry = !empty($zohoAccount->Billing_Country) ? $zohoAccount->Billing_Country : '';

            $postalAddress = new Address();
            $postalAddress->setAddressType($postalAddress::ADDRESS_TYPE_POBOX);
            !empty($billingStreet) ? $postalAddress->setAddressLine1($billingStreet) : $postalAddress->setAddressLine1('');
            !empty($billingCity) ? $postalAddress->setCity($billingCity) : $postalAddress->setCity('');
            !empty($billingRegion) ? $postalAddress->setRegion($billingRegion) : $postalAddress->setRegion('');
            !empty($billingPostalCode) ? $postalAddress->setPostalCode($billingPostalCode) : $postalAddress->setPostalCode('');
            !empty($billingCountry) ? $postalAddress->setCountry($billingCountry) : $postalAddress->setCountry('');


            $shippingStreet = !empty($zohoAccount->Shipping_Street) ? $zohoAccount->Shipping_Street : '';
            $shippingCity = !empty($zohoAccount->Shipping_City) ? $zohoAccount->Shipping_City : '';
            $shippingRegion = !empty($zohoAccount->Shipping_State) ? $zohoAccount->Shipping_State : '';
            $shippingPostalCode = !empty($zohoAccount->Shipping_Code) ? $zohoAccount->Shipping_Code : '';
            $shippingCountry = !empty($zohoAccount->Shipping_Country) ? $zohoAccount->Shipping_Country : '';

            $shippingAddress = new Address();
            $shippingAddress->setAddressType($shippingAddress::ADDRESS_TYPE_STREET);
            !empty($shippingStreet) ? $shippingAddress->setAddressLine1($shippingStreet) : $shippingAddress->setAddressLine1('');
            !empty($shippingCity) ? $shippingAddress->setCity($shippingCity) : $shippingAddress->setCity('');
            !empty($shippingRegion) ? $shippingAddress->setRegion($shippingRegion) : $shippingAddress->setRegion('');
            !empty($shippingPostalCode) ? $shippingAddress->setPostalCode($shippingPostalCode) : $shippingAddress->setPostalCode('');
            !empty($shippingCountry) ? $shippingAddress->setCountry($shippingCountry) : $shippingAddress->setCountry('');
        }else{
            $website = '';
            $phone = new Phone();
            $fax = new Phone();
            $postalAddress = new Address();
            $shippingAddress = new Address();
        }

        $mobile = new Phone();
        if(!empty($zohoContact->Mobile)){
            $mobile->setPhoneType($mobile::PHONE_TYPE_MOBILE);
            $mobile->setPhoneNumber($zohoContact->Mobile);
        }
        $ddi = new Phone();
        if(!empty($zohoContact->Phone)){
            $ddi->setPhoneType($ddi::PHONE_TYPE_DDI);
            $ddi->setPhoneNumber($zohoContact->Phone);
        }

        $accountName = !empty($zohoAccount->Company_Name) ? $zohoAccount->Company_Name : '';
        $contact = new Contact($this->xero);
        $contact->setName($accountName)
            ->setAccountNumber(!empty($zohoAccount->Account_No) ? $zohoAccount->Account_No : '')
            ->setFirstName(!empty($zohoContact->First_Name) ? $zohoContact->First_Name : '')
            ->setLastName(!empty($zohoContact->Last_Name) ? $zohoContact->Last_Name : '')
            ->setEmailAddress(!empty($zohoContact->Email) ? $zohoContact->Email: '')
            ->setWebsite($website)
            ->addPhone($phone)
            ->addPhone($fax)
            ->addPhone($mobile)
            ->addPhone($ddi)
            ->addAddress($postalAddress)
            ->addAddress($shippingAddress)
            ->setIsCustomer(true);
        try{
            $contactSave = $contact->save();
            $responseElements = $contactSave->getElements();
            $contactId = $responseElements[0]['ContactID'];

            if(!empty($contactId)){
                $this->addContactToZohoXeroContactTable($responseElements[0]);
            }
            if($contactSave->getStatus() == 200){
                DB::table('zoho_invoice_queue')->where('id', $this->focus->id)->update(['xero_contact_id' => $contactId]);
                $this->log['info']['contact'][] = "Xero Contact was successfully created in xero with id: $contactId";
            }
        }catch(\Exception $exception){
            $this->log['fatal_error']['contact'][] = $exception->getMessage();
        }

        return $contactId;
    }


    private function addContactToZohoXeroContactTable($xeroContact){
        $contactQuery = DB::table('zoho_to_xero_xero_contacts')->select('id')->where('batch_number', $this->batchNumber)->where('contact_id', $xeroContact['ContactID'])->first();

        if(empty($contactQuery->id)){
            DB::table('zoho_to_xero_xero_contacts')->insert(
                [
                    'contact_id' => $xeroContact['ContactID'],
                    'email_address' => !empty($xeroContact['EmailAddress']) ? $xeroContact['EmailAddress'] : '',
                    'contact_name' => !empty($xeroContact['Name']) ? $xeroContact['Name'] : '',
                    'batch_number' => $this->batchNumber,
                ]
            );
        }
    }

    private function searchExistingXeroContact($zohoAccountName)
    {
        $zohoAccountName = $this->sanitizeData($zohoAccountName);
        $xeroContactQuery = DB::table('zoho_to_xero_xero_contacts')->select('contact_id')->where('batch_number', $this->batchNumber)->whereRaw("LOWER(contact_name) = LOWER('{$zohoAccountName}')")->first();

        if (!empty($xeroContactQuery->contact_id)) {
            return $xeroContactQuery->contact_id;
        }

        return false;
    }

    private function getZohoContact($accountId)
    {
        if (empty($accountId)) {
            return false;
        }

        $accountQuery = DB::table('zoho_to_xero_zoho_accounts')->select('record_details')->where('batch_number', $this->batchNumber)->where('account_id', $accountId)->first();

        if(!empty($accountQuery)){
            $account = json_decode($accountQuery->record_details);
            if (!empty($account->Primary_Contact->id)) {
                $contactId = $account->Primary_Contact->id;
                $contactQuery = DB::table('zoho_to_xero_zoho_contacts')->select(['email_address', 'record_details'])->where('batch_number', $this->batchNumber)->where('contact_id', $contactId)->first();
                if (!empty($contactQuery->record_details)) {
                    return $contactQuery;
                }
            } else {
                return false;
            }
        }

        return false;
    }

    private function processStatus($zohoStatus){
        switch($zohoStatus){
            case 'Awaiting Approval':
                $status = 'SUBMITTED';
                break;
            case 'Approved':
            case 'Approved & Sent':
                $status = 'AUTHORISED';
                break;
            case 'Paid':
                $status = 'PAID';
                break;
            case 'Deleted':
                $status = 'DELETED';
                break;
            case 'Cancelled':
            case 'Voided':
                $status = 'VOIDED';
                break;
            default:
                $status = 'DRAFT';
        }

        return $status;
    }

    private function parseData($record)
    {
        $jsonEncodedData = json_encode($record);
        return json_decode($jsonEncodedData, true);
    }

    private function getProperDate($date){
        try{
            $dateTime = new \DateTime($date);
            return $dateTime;
        }catch(\Exception $exception){
            return '';
        }
    }

    private function calculateDiscountRate($discount, $lineAmount){
        if(empty($discount) || empty($lineAmount)){
            return null;
        }

        $percentage = 100 * ((float)$discount / (float)$lineAmount);
        return $percentage;
    }

    private function markXeroInvoiceAsDeletedOrVoided($accReceivable, $invoiceQuery){
        if(!empty($invoiceQuery)){
            $xeroInvoice = $this->xero->loadByGUID(Invoice::class, $invoiceQuery->invoice_id);
            $status = $this->processStatus($accReceivable->Status);
            $xeroInvoice->setStatus($status);
            try {
                $invoiceUpdate = $xeroInvoice->save();
                DB::table('xero_invoice_update_timestamps')->insert(['invoice_number' => $accReceivable->Invoice_No1]);
                if($invoiceUpdate->getStatus() == 200){
                    $this->log['info']['invoice'][] = "Xero invoice is successfully updated in Xero with with Invoice Number: {$accReceivable->Invoice_No1}";
                }
            } catch (\Exception $exception) {
                $this->log['error']['invoice'][] = "Encountered an error while updating the invoice in Xero, see details: {$exception->getMessage()}";
            }
        }
    }

    private function triggerXeroToZohoQueue($invoiceQuery, $accReceivable){
        try{
            $xeroInvoice = $this->xero->loadByGUID('Accounting\\Invoice', $invoiceQuery->invoice_id);
        }catch (\Exception $e){

            $this->log['error']['invoice'][] = "Encountered an error while triggering forced xero to zoho queue, see details: {$e->getMessage()}";

            $code = $e->getCode();
            $this->xeroExceptionParser($code);

            return false;
        }

        $batchNumber = DB::table('config')->where('category', 'Xero Sync Config')->where('name','batch_number')->first();
        if(empty($batchNumber->value)){
            $queryRecentBatchId = DB::table('xero_invoice_queue')->max('batch_number');
            $recentBatchId = !empty($queryRecentBatchId) ? $queryRecentBatchId : 0;
            $batchNumber = $recentBatchId + 1;
            DB::table('config')->insert(
                [
                    'category' => 'Xero Sync Config',
                    'name' => 'batch_number',
                    'value' => $batchNumber,
                    'platform' => 'base',
                ]
            );
        }else{
            $batchNumber = $batchNumber->value + 1;
            // Update Batch Id Number in config
            DB::table('config')->where('category', 'Xero Sync Config')->where('name', 'batch_number')->update(['value' => $batchNumber]);
        }

        $jsonEncodedInvoice = json_encode($xeroInvoice);
        $data = json_decode($jsonEncodedInvoice);

        DB::table('xero_invoice_queue')->insert(
            [
                'invoice_type' =>  !empty($data->Type) ? $data->Type : '',
                'invoice_number' => !empty($data->InvoiceNumber) ? $data->InvoiceNumber : '',
                'batch_number' => $batchNumber,
                'record_updated_date' =>  !empty($data->UpdatedDateUTC) ? $data->UpdatedDateUTC : '',
                'record_details' => json_encode($xeroInvoice),
                'status' => 'waiting_related_records'
            ]
        );

        try{
            $xeroContact = $this->xero->loadByGUID('Accounting\\Contact', $xeroInvoice->Contact->ContactID);
        }catch (\Exception $e){
            $code = $e->getCode();
            $this->log['error']['invoice'][] = "Encountered an error while triggering forced xero to zoho queue, see details: {$e->getMessage()}";
            $this->xeroExceptionParser($code);

            return false;
        }


        $jsonEncodedContact = json_encode($xeroContact);
        $data = json_decode($jsonEncodedContact);

        DB::table('xero_contacts')->insert(
            [
                'contact_id' => $data->ContactID,
                'batch_number' => $batchNumber,
                'website' => !empty($data->website) ? $data->website : '',
                'record_details' => json_encode($xeroContact),
            ]
        );

        DB::table('xero_invoice_queue')->where('batch_number', $batchNumber)->update(['status' => 'queue']);

        $zohoInvoice = \ZCRMRecord::getInstance("Invoices", $accReceivable->id);
        $zohoInvoice->setFieldValue('Force_Resync', '');
        $zohoInvoice->setFieldValue('Force_Sync_Request_Timestamp', '');

        // Update zoho invoice force resync fields to acknowledge that the request has been granted
        try {
            $updateAPInvoiceResponse = $zohoInvoice->update();
            if ($updateAPInvoiceResponse->getStatus() == 'success' && $updateAPInvoiceResponse->getMessage() == 'record updated') {
                $this->log['info']['invoice'][] = "Successfully triggered a Force Sync from xero to zoho invoice: $accReceivable->id";
                DB::table('zoho_invoice_update_timestamps')->insert(['invoice_number' => $accReceivable->Invoice_No1]);
            }
        } catch (\Exception $e) {
            $this->log['error']['invoice'][] = "Encountered an error while triggering forced xero to zoho queue, see details: {$e->getMessage()}";
            $this->zohoExceptionParser($e->getMessage());
        }
        $this->log['info']['invoice'][] = "Forced Xero to zoho queue is successfully triggered: {$accReceivable->Invoice_No1}";
    }

    private function checkIfForceUpdateZohoToXero($accReceivable){
        if (!empty($accReceivable->Force_Resync) && strtolower($accReceivable->Force_Resync) == 'zoho to xero') {
            $zohoInvoice = \ZCRMRecord::getInstance("Invoices", $accReceivable->id);
            $zohoInvoice->setFieldValue('Force_Resync', '');
            $zohoInvoice->setFieldValue('Force_Sync_Request_Timestamp', '');

            try {
                $updateAPInvoiceResponse = $zohoInvoice->update();
                if ($updateAPInvoiceResponse->getStatus() == 'success' && $updateAPInvoiceResponse->getMessage() == 'record updated') {
                    $this->log['info']['invoice'][] = "Forced Zoho to Xero sync is successful: $accReceivable->id";
                    DB::table('zoho_invoice_update_timestamps')->insert(['invoice_number' => $accReceivable->Invoice_No1]);
                }
            } catch (\Exception $e) {
                $this->log['error']['invoice'][] = "Encountered an error while updating forced resync fields in zoho, see details: {$e->getMessage()}";
                $this->zohoExceptionParser($e->getMessage());
            }
        }
    }

    private function parseZohoLineItemDescription($zohoLineItem){
        $productDescription = $zohoLineItem->getDescription();

        $productDescExploded = explode("\n", $productDescription);

        // First line should be the product name to xero
        $productName = !empty($productDescExploded[0]) ? $productDescExploded[0] : '';

        //Remove spaces before and after the description
        return trim($productName);
    }

    private function xeroExceptionParser($code){
        switch((int)$code){
            case 401:
            case 503:
                $requeueRecord = true;
                break;
            default:
                $requeueRecord = false;
        }

        if(!empty($requeueRecord)){
            DB::table('zoho_invoice_queue')->where('id', $this->focus->id)->update(['status' => 'queue']);
            exit();
        }
    }

    private function zohoExceptionParser($log){
        $invalidOauth = "invalid oauth token";
        $internalServerError = "internal server error";
        $refreshTokenNotProvided = "refresh token is not provided";
        $undefinedOffset = "undefined offset 1";

        $log = strtolower($log);
        if (strpos($log, $invalidOauth) == false
            && strpos($log, $internalServerError) == false
            && strpos($log, $refreshTokenNotProvided) == false
            && strpos($log, $undefinedOffset) == false) {
        } else {
            // if true then requeue
            DB::table('zoho_invoice_queue')->where('id', $this->focus->id)->update(['status' => 'queue']);
            exit();
        }

        return false;
    }

    public function parseXeroData($record){
        $jsonEncodedData = json_encode($record);
        return json_decode($jsonEncodedData);
    }

    private function getLineItemTaxDetails($taxType){
        $taxDetails = DB::table('xero_to_zoho_tax_rate_mapping')
            ->select('zoho_tax_name', 'zoho_tax_value', 'xero_tax_code')
            ->whereRaw("LOWER(`zoho_tax_name`) = '$taxType'")
            ->first();

        return $taxDetails;
    }

    private function sanitizeData($data){
        if($data != ""){
            $data = addslashes($data);
            return $data;
        }

        return $data;
    }
}