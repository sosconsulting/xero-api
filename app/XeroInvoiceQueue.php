<?php

namespace SOSZohoXeroIntegration;

use Illuminate\Database\Eloquent\Model;

class XeroInvoiceQueue extends Model
{
    protected $casts = [
        'record_details' => 'array',
    ];

    protected $table = 'xero_invoice_queue';
    protected $guarded = [];
}
