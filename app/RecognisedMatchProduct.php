<?php

namespace SOSZohoXeroIntegration;

use Illuminate\Database\Eloquent\Model;

class RecognisedMatchProduct extends Model
{
    protected $fillable = [
        'product_name',
        'zoho_product_id'
    ];
}
