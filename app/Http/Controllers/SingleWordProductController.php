<?php

namespace SOSZohoXeroIntegration\Http\Controllers;

use Illuminate\Http\Request;
use SOSZohoXeroIntegration\SingleWordProduct;

class SingleWordProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = SingleWordProduct::latest()->paginate(10);

        return view('single-word-products.index',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('single-word-products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'mapping_word' => 'required',
            'zoho_product_id' => 'required'
        ]);

        $product = new SingleWordProduct([
            'mapping_word' => $request->get('mapping_word'),
            'zoho_product_id' => $request->get('zoho_product_id')
        ]);

        $product->save();
        return redirect('/single-word-products')->with('success', 'Mapping word is successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = SingleWordProduct::find($id);

        return view('single-word-products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = SingleWordProduct::find($id);

        return view('single-word-products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'mapping_word' => 'required',
            'zoho_product_id' => 'required'
        ]);

        $product = SingleWordProduct::find($id);
        $product->mapping_word = $request->get('mapping_word');
        $product->zoho_product_id = $request->get('zoho_product_id');

        $product->save();
        return redirect('/single-word-products')->with('success', 'Mapping word has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = SingleWordProduct::find($id);
        $product->delete();

        return redirect('/single-word-products')->with('success', 'Mapping word has been deleted Successfully');
    }
}
