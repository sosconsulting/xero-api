<?php

namespace SOSZohoXeroIntegration\Http\Controllers;

use Illuminate\Http\Request;
use SOSZohoXeroIntegration\State;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $states = State::latest()->paginate(10);

        return view('states.index',compact('states'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('states.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'abbreviation' => 'required',
            'state' => 'required',
            'country' => 'required'
        ]);

        $country = new State([
            'abbreviation' => $request->get('abbreviation'),
            'state' => $request->get('state'),
            'country' => $request->get('country')
        ]);

        $country->save();
        return redirect('/states')->with('success', 'State Mapping is added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $state = State::find($id);

        return view('states.show', compact('state'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $state = State::find($id);

        return view('states.edit', compact('state'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'abbreviation' => 'required',
            'state' => 'required',
            'country' => 'required'
        ]);

        $state = State::find($id);
        $state->abbreviation = $request->get('abbreviation');
        $state->state = $request->get('state');
        $state->country = $request->get('country');

        $state->save();
        return redirect('/states')->with('success', 'State Mapping has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $state = State::find($id);
        $state->delete();

        return redirect('/states')->with('success', 'State mapping has been deleted Successfully');
    }
}
