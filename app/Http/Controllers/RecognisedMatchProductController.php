<?php

namespace SOSZohoXeroIntegration\Http\Controllers;

use Illuminate\Http\Request;
use SOSZohoXeroIntegration\RecognisedMatchProduct;

class RecognisedMatchProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = RecognisedMatchProduct::latest()->paginate(10);

        return view('recognised-match-products.index',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('recognised-match-products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_name' => 'required',
            'zoho_product_id' => 'required'
        ]);

        $product = new RecognisedMatchProduct([
           'product_name' => $request->get('product_name'),
           'zoho_product_id' => $request->get('zoho_product_id')
        ]);

        $product->save();
        return redirect('/recognised-match-products')->with('success', 'Product added as Recognised Match');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = RecognisedMatchProduct::find($id);

        return view('recognised-match-products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = RecognisedMatchProduct::find($id);

        return view('recognised-match-products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'product_name' => 'required',
            'zoho_product_id' => 'required'
        ]);

        $product = RecognisedMatchProduct::find($id);
        $product->product_name = $request->get('product_name');
        $product->zoho_product_id = $request->get('zoho_product_id');

        $product->save();
        return redirect('/recognised-match-products')->with('success', 'Product has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = RecognisedMatchProduct::find($id);
        $product->delete();

        return redirect('/recognised-match-products')->with('success', 'Product has been deleted Successfully');
    }
}
