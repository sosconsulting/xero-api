<?php

namespace SOSZohoXeroIntegration\Http\Controllers;

use Illuminate\Http\Request;
use SOSZohoXeroIntegration\CompanyAttribute;

class CompanyAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attributes = CompanyAttribute::latest()->paginate(10);

        return view('company-attributes.index',compact('attributes'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company-attributes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'keyword' => 'required',
            'industry' => 'required',
            'channel' => 'required',
            'category' => 'required'
        ]);

        $attribute = new CompanyAttribute([
            'keyword' => $request->get('keyword'),
            'industry' => $request->get('industry'),
            'channel' => $request->get('channel'),
            'category' => $request->get('category')
        ]);

        $attribute->save();
        return redirect('/company-attributes')->with('success', 'Company Attribute is added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $attribute = CompanyAttribute::find($id);

        return view('company-attributes.show', compact('attribute'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $attribute = CompanyAttribute::find($id);

        return view('company-attributes.edit', compact('attribute'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'keyword' => 'required',
            'industry' => 'required',
            'channel' => 'required',
            'category' => 'required'
        ]);

        $attribute = CompanyAttribute::find($id);
        $attribute->keyword = $request->get('keyword');
        $attribute->industry = $request->get('industry');
        $attribute->channel = $request->get('channel');
        $attribute->category = $request->get('category');

        $attribute->save();
        return redirect('/company-attributes')->with('success', 'Company Attribute has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attribute = CompanyAttribute::find($id);
        $attribute->delete();

        return redirect('/company-attributes')->with('success', 'Company attribute has been deleted Successfully');
    }
}
