<?php

namespace SOSZohoXeroIntegration\Http\Controllers;

use Illuminate\Http\Request;

class ZohoGrantToken extends Controller
{
    public function index(Request $request)
    {
        return !empty($request->input('code')) ? "Zoho CRM Grant Token: " . $request->input('code') : 'No Token Retrieved';
    }
}
