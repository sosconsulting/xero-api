<?php

namespace SOSZohoXeroIntegration\Http\Controllers;

use Illuminate\Http\Request;
use SOSZohoXeroIntegration\Country;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::latest()->paginate(10);

        return view('countries.index',compact('countries'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('countries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'country' => 'required',
            'region' => 'required',
            'abbreviation' => 'required'
        ]);

        $country = new Country([
            'country' => $request->get('country'),
            'region' => $request->get('region'),
            'abbreviation' => $request->get('abbreviation')
        ]);

        $country->save();
        return redirect('/countries')->with('success', 'Country Mapping is added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $country = Country::find($id);

        return view('countries.show', compact('country'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country= Country::find($id);

        return view('countries.edit', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'country' => 'required',
            'region' => 'required',
            'abbreviation' => 'required'
        ]);

        $country = Country::find($id);
        $country->country = $request->get('country');
        $country->region = $request->get('region');
        $country->abbreviation = $request->get('abbreviation');

        $country->save();
        return redirect('/countries')->with('success', 'Country Mapping has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $country = Country::find($id);
        $country->delete();

        return redirect('/countries')->with('success', 'Country mapping has been deleted Successfully');
    }
}
