<?php

namespace SOSZohoXeroIntegration;

use Illuminate\Database\Eloquent\Model;

class CompanyAttribute extends Model
{
    protected $fillable = [
        'keyword',
        'industry',
        'channel',
        'category',
    ];
}
