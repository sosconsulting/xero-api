<?php

namespace SOSZohoXeroIntegration;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = 'config';
    protected $guarded = [];

    public $incrementing = false;
}
