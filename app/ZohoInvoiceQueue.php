<?php

namespace SOSZohoXeroIntegration;

use Illuminate\Database\Eloquent\Model;

class ZohoInvoiceQueue extends Model
{
    protected $casts = [
        'record_details' => 'array',
    ];

    protected $table = 'zoho_invoice_queue';
    protected $guarded = [];
}
