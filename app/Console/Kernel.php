<?php

namespace SOSZohoXeroIntegration\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('Subscription')->daily();
        $schedule->command('SendXeroToZohoSyncReport')->daily();
        $schedule->command('SendZohoToXeroSyncReport')->daily();

        // // DB Cleanup
        $schedule->command('UpdateInvoicesStockedInProcessing')->everyThirtyMinutes();
        //$schedule->command('SoftDeleteProcessedInvoices')->daily();
        //$schedule->command('PermanentlyDeleteInvoice')->daily();
        //$schedule->command('CleanupXeroInvoicesRelatedTables')->daily();
        //$schedule->command('CleanupApXeroInvoicesStatusUpdateRelatedTables')->daily();
        //$schedule->command('CleanupZohoInvoicesRelatedTables')->daily();

        $subscriptionBasic = DB::table('config')->where('category', 'Subscription')->where('name', 'basic')->first();

        if(!empty($subscriptionBasic->value) && $subscriptionBasic->value == true){
            $schedule->command('InitializeZohoToken')->everyFiveMinutes();
            $schedule->command('GatherXeroInvoices')->everyFifteenMinutes();
            $schedule->command('SyncXeroToZoho')->everyFifteenMinutes();
            $schedule->command('GatherZohoInvoices')->everyFifteenMinutes();
            $schedule->command('SyncZohoToXero')->everyFifteenMinutes();
            $schedule->command('GatherZohoAPInvoicesForXeroStatusUpdate')->everyFifteenMinutes();
            $schedule->command('UpdateXeroApStatus')->everyFifteenMinutes();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
