<?php

namespace SOSZohoXeroIntegration\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SoftDeleteProcessedInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SoftDeleteProcessedInvoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Soft deletes the invoices that are already processed by syncapp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $xeroInvoices2DaysOld = DB::table('xero_invoice_queue')->select('id')
            ->where(function($query){
                $query->where('status', '!=','queue');
                $query->where('status', '!=', 'processing');
            })
            ->where(function($query){
                $query->whereNull('sync_fatal_errors');
                $query->orWhere('sync_fatal_errors', '=', '');
            })
            ->where(function($query){
                $query->whereNotNull('process_start_time');
                $query->orWhere('process_start_time', '=', '');

            })
            ->where(function($query){
                $query->whereNotNull('process_end_time');
                $query->orWhere('process_end_time', '=', '');

            })
            ->whereRaw('created_at < DATE_SUB(NOW(), INTERVAL 1 DAY)')
            ->get()->toArray();

        $zohoInvoices2DaysOld = DB::table('zoho_invoice_queue')->select('id')
            ->where(function($query){
                $query->where('status', '!=','queue');
                $query->where('status', '!=', 'processing');
            })
            ->where(function($query){
                $query->whereNull('sync_fatal_errors');
                $query->orWhere('sync_fatal_errors', '=', '');
            })
            ->where(function($query){
                $query->whereNotNull('process_start_time');
                $query->orWhere('process_start_time', '=', '');

            })
            ->where(function($query){
                $query->whereNotNull('process_end_time');
                $query->orWhere('process_end_time', '=', '');

            })
            ->whereRaw('created_at < DATE_SUB(NOW(), INTERVAL 1 DAY)')
            ->get()->toArray();

        $xeroAPInvoicesStatusUpdate2DaysOld = DB::table('ap_zoho_invoice_for_xero_status_update')->select('id')
            ->where(function($query){
                $query->where('status', '!=','queue');
                $query->where('status', '!=', 'processing');
            })
            ->where(function($query){
                $query->whereNull('sync_fatal_errors');
                $query->orWhere('sync_fatal_errors', '=', '');
            })
            ->where(function($query){
                $query->whereNotNull('process_start_time');
                $query->orWhere('process_start_time', '=', '');

            })
            ->where(function($query){
                $query->whereNotNull('process_end_time');
                $query->orWhere('process_end_time', '=', '');

            })
            ->whereRaw('created_at < DATE_SUB(NOW(), INTERVAL 1 DAY)')
            ->get()->toArray();

        $this->softDelete($xeroInvoices2DaysOld, 'xero_invoice_queue');
        $this->softDelete($zohoInvoices2DaysOld, 'zoho_invoice_queue');
        $this->softDelete($xeroAPInvoicesStatusUpdate2DaysOld, 'ap_zoho_invoice_for_xero_status_update');

        return true;
    }


    public function softDelete($invoices, $table){
        foreach($invoices as $invoice){
            DB::table($table)->where('id', $invoice->id)->update(['deleted' => 1]);
        }
    }
}
