<?php
namespace SOSZohoXeroIntegration\Console\Commands;

require 'vendor/autoload.php';
use SOSZohoXeroIntegration\SyncHelpers;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use XeroPHP\Application\PrivateApplication;

class ZohoSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SyncXeroToZoho';

    /**
     * Limit of records to process
     * @var int
     */
    private $queryLimit = 30;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync gathered data from xero into zoho crm';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws \ZohoOAuthException
     */
    public function handle()
    {
        \ZCRMRestClient::initialize();
        $xero = new PrivateApplication(config('sync'));

        $recordsToProcess = [];
        $excludedIds = [];
        $onQueueRecords = $this->getAllQueueCount($excludedIds);
        $queryXeroTableQueue = $this->getXeroRecords($excludedIds);

        while (
            count($recordsToProcess) < $this->queryLimit
            && count($onQueueRecords) > 0
        ) {
            foreach ($queryXeroTableQueue as $focus) {
                if (count($recordsToProcess) == $this->queryLimit) {
                    continue;
                }

                if ($focus->invoice_type == 'ACCPAY') {
                    // DISABLE PROCESSING ACCOUNTS PAYABLE SINCE THIS IS NOT NEEDED IN ASSA ACCOUNT
                    // continue;
                    array_push($recordsToProcess, $focus);
                } else {
                    $processNow = $this->checkIfProcessingARNow($focus);

                    if (!empty($processNow)) {
                        array_push($recordsToProcess, $focus);
                    }
                }

                array_push($excludedIds, $focus->id);
            }
            $onQueueRecords = $this->getAllQueueCount($excludedIds);
            $queryXeroTableQueue = $this->getXeroRecords($excludedIds);
        }

        $maxBatchNumber = DB::table('xero_to_zoho_recently_processed')->max('batch_number');

        if($maxBatchNumber == 2){
            DB::table('xero_to_zoho_recently_processed')->truncate();
        }

        $recordBatchNumber = (empty($maxBatchNumber) || $maxBatchNumber == 2) ? 1 : 2;

        foreach ($recordsToProcess as $record) {
            if(!empty($record->id) && !empty($record->record_details)){
                $sameInvoiceNumberExist = DB::table('xero_to_zoho_recently_processed')->where('invoice_number', $record->invoice_number)->first();
                switch ($record->invoice_type){
                    case 'ACCPAY':
                        // DISABLE PROCESSING ACCOUNTS PAYABLE SINCE THIS IS NOT NEEDED IN ASSA ACCOUNT
                        // continue;
                        // Do not immediately process records with same invoice number available in recently processed invoice.
                        // This is to prevent invoice duplicate since there is a delay in zoho API for their search index.
                        if(!empty($sameInvoiceNumberExist)){
                            continue;
                        }

                        $this->insertToRecentlyProcessed($record->invoice_number, 'ACCPAY', $recordBatchNumber);

                        DB::table('xero_invoice_queue')->where('id', $record->id)->update(['status' => 'processing', 'process_start_time' => DB::raw('CURRENT_TIMESTAMP')]);
                        $accPay = new SyncHelpers\XeroToZohoAccountsPayable();
                        $syncLog = $accPay->processData($record, $xero);
                        $this->processLogs($syncLog, $record);
                        break;
                    case 'ACCREC':
                        // Do not immediately process records with same invoice number available in recently processed invoice.
                        // This is to prevent invoice duplicate since there is a delay in zoho API for their search index.
                        if(!empty($sameInvoiceNumberExist)){
                            continue;
                        }

                        DB::table('xero_invoice_queue')->where('id', $record->id)->update(['status' => 'processing', 'process_start_time' => DB::raw('CURRENT_TIMESTAMP')]);
                        $accRec = new SyncHelpers\XeroToZohoAccountsReceivable();
                        $syncLog = $accRec->processData($record, $xero);
                        $this->processLogs($syncLog, $record);
                        $this->insertToRecentlyProcessed($record->invoice_number, 'ACCREC', $recordBatchNumber);
                        break;
                }
            }
        }

        return true;
    }

    /**
     * Validate if we will sync xero invoice to zoho invoice by checking the updated timestamp
     * If updated timestamp of xero invoice is
     * @param $xero
     * @param $record
     * @return bool
     */
    private function checkIfProcessingARNow($record){
        $zohoInvoicesQueue = DB::table('zoho_invoice_queue')
            ->where('invoice_number', "{$record->invoice_number}")
            ->where(function ($query){
                $query->where('status', 'queue')
                    ->orWHere('status', 'processing');
            })
            ->oldest()
            ->get()
            ->toArray();

        if(empty($zohoInvoicesQueue)){
            return true;
        }

        $dtXeroRecordUpdatedDate = new \DateTime($record->record_updated_date);
        $dtXeroRecordUpdatedDate->setTimezone(new \DateTimeZone("UTC"));
        $xeroRecordUpdatedDate = $dtXeroRecordUpdatedDate->format('Y-m-d H:i:s');

        foreach($zohoInvoicesQueue as $invoice){
            $dtZohoRecordUpdatedDate = new \DateTime($invoice->record_updated_date);
            $dtZohoRecordUpdatedDate->setTimezone(new \DateTimeZone("UTC"));
            $zohoRecordUpdatedDate = $dtZohoRecordUpdatedDate->format('Y-m-d H:i:s');

            if ($xeroRecordUpdatedDate > $zohoRecordUpdatedDate){
                return false;
            }

        }

        return true;
    }

    private function processLogs($syncDetails, $record){
        if(!empty($syncDetails['fatal_error'])){
            $status = 'not_processed';
            $syncStatus = 'failed';
            $processEndTime = DB::raw('CURRENT_TIMESTAMP');
            $syncToday = 1;

            // Trigger resync for known errors
            $resync = $this->exceptionParser($syncDetails['fatal_error']);
            if (!empty($resync)) {
                $status = 'queue';
                $syncStatus = 'resync';
                $syncToday = 0;
                $processEndTime = null;
            }

            $notFatalError = !empty($syncDetails['error']) ? $syncDetails['error'] : [];
            $fatalError = !empty($syncDetails['fatal_error']) ? $syncDetails['fatal_error'] : [];

            DB::table('xero_invoice_queue')->where('id', $record->id)->update([
                'status' => $status,
                'sync_errors' => json_encode($notFatalError),
                'sync_fatal_errors' => json_encode($fatalError),
                'process_end_time' => $processEndTime,
                'sync_status' => $syncStatus,
                'synced_today' => $syncToday
            ]);
            return;
        }

        $notFatalError = !empty($syncDetails['error'])  ? json_encode($syncDetails['error']) : null;
        $info = !empty($syncDetails['info']) ? json_encode($syncDetails['info']) : null;

        DB::table('xero_invoice_queue')->where('id', $record->id)->update([
            'status' => 'synced',
            'sync_errors' => $notFatalError,
            'sync_fatal_errors' => '',
            'sync_log' => $info,
            'process_end_time' => DB::raw('CURRENT_TIMESTAMP'),
            'sync_status' => 'success',
            'synced_today' => 1
        ]);
    }

    private function insertToRecentlyProcessed($invoiceNumber, $invoiceType, $batchNumber){
        DB::table('xero_to_zoho_recently_processed')->insert(
            [
                'invoice_number' => $invoiceNumber,
                'invoice_type' => $invoiceType,
                'batch_number' => $batchNumber,
            ]
        );
    }

    private function getXeroRecords($excludedIds)
    {
        if (!empty($excludedIds)) {
            $result = DB::table('xero_invoice_queue')->where('status', 'queue')->whereNotIn('id', $excludedIds)->orderBy('record_updated_date')->limit($this->queryLimit)->get()->toArray();
        } else {
            $result = DB::table('xero_invoice_queue')->where('status', 'queue')->orderBy('record_updated_date')->limit($this->queryLimit)->get()->toArray();
        }

        return $result;
    }

    private function getAllQueueCount($excludedIds)
    {
        if (!empty($excludedIds)) {
            $result = DB::table('xero_invoice_queue')->where('status', 'queue')->whereNotIn('id', $excludedIds)->orderBy('record_updated_date')->get()->toArray();
        } else {
            $result = DB::table('xero_invoice_queue')->where('status', 'queue')->orderBy('record_updated_date')->get()->toArray();
        }

        return $result;
    }

    /**
     * Validate if the error is qualified for resync
     * @param $logs
     * @return bool
     */
    private function exceptionParser($logs){
        $invalidOauth = "invalid oauth token";
        $internalServerError = "internal server error";
        $refreshTokenNotProvided = "refresh token is not provided";
        $undefinedOffset = "undefined offset: 1";
        foreach($logs as $moduleName => $moduleLogs){
            foreach($moduleLogs as $log){
                if (empty($log)){
                    continue;
                }
                $log = strtolower($log);
                if (strpos($log, $invalidOauth) == false
                    && strpos($log, $internalServerError) == false
                    && strpos($log, $refreshTokenNotProvided) == false
                    && strpos($log, $undefinedOffset) == false) {
                    continue;
                } else {
                    return true;
                }
            }
        }

        return false;
    }
}
