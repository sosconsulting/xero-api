<?php

namespace SOSZohoXeroIntegration\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UpdateInvoicesStockedInProcessing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateInvoicesStockedInProcessing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update invoices that are stocked in processing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $xeroInvoicesInProcessing = $result = DB::table('xero_invoice_queue')->select(['id', 'process_start_time', 'process_end_time', 'sync_fatal_errors', 'sync_errors'])->where('process_end_time', null)->where('status', 'processing')->orderByDesc('process_start_time')->get()->toArray();
        $zohoInvoicesInProcessing = $result = DB::table('zoho_invoice_queue')->select(['id', 'process_start_time', 'process_end_time', 'sync_fatal_errors', 'sync_errors'])->where('process_end_time', null)->where('status', 'processing')->orderByDesc('process_start_time')->get()->toArray();

        $this->updateStatus($xeroInvoicesInProcessing, 'xero_invoice_queue');
        $this->updateStatus($zohoInvoicesInProcessing, 'zoho_invoice_queue');

        return true;
    }

    public  function updateStatus($recordsInProcessing, $tableName){
        if(empty($recordsInProcessing)){
            return;
        }

        $currentDateTime = Carbon::now();

        foreach($recordsInProcessing as $invoice){
            $date = Carbon::parse($invoice->process_start_time);
            $diff = $date->diffInMinutes($currentDateTime);

            if($diff > 45){
                    DB::table($tableName)->where('id', $invoice->id)->update(['status' => 'queue']);
            }
        }
    }
}
