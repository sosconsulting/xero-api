<?php

namespace SOSZohoXeroIntegration\Console\Commands;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use SOSZohoXeroIntegration\Mail\ZohoToXeroSyncReport;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendZohoToXeroSyncReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendZohoToXeroSyncReport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $attachmentFileName = $this->processSyncReport();

        if(!empty($attachmentFileName)){
            $receiverEmailAdd = env('SYNC_REPORT_MAIN_RECIPIENT_EMAIL');
            $cc = explode(",", env('SYNC_REPORT_EMAIL_CC'));
            Mail::to($receiverEmailAdd)
                ->cc($cc)
                ->send(new ZohoToXeroSyncReport($attachmentFileName));
        }

        DB::table('zoho_invoice_queue')->where('synced_today', 1)->update([
            'synced_today' => 0,
        ]);

        return true;
    }

    private function processSyncReport(){
        $recordsProcessedToday = DB::table('zoho_invoice_queue')->where('synced_today', 1)->oldest()->get()->toArray();

        if(empty($recordsProcessedToday)){
            return false;
        }

        $unSyncedRecords = [];
        $syncedRecords = [];
        foreach($recordsProcessedToday as $record){
            if($record->sync_status == 'failed'){
                $unSyncedRecords[] = $record;
            }

            if($record->sync_status == 'success'){
                $syncedRecords[] = $record;
            }
        }

        $syncStartDateTime = Carbon::now()->toDateTimeString();

        $attachmentFileName = 'Zoho to Xero SyncReport' . $syncStartDateTime . '.html';

        if(!empty($recordsProcessedToday[0])){
            $successCount = 0;
            $failCount = 0;
            $html = "<html>";
            $html .= "<head><link href='https://fonts.googleapis.com/css?family=Noto+Sans' rel='stylesheet' type='text/css'></head>";
            $html .= "<style>.content {max-width:1000px;margin: 0 auto; *{font-family: 'Noto Sans', sans-serif;}}</style><body>";
            $html .= "<body ><div class='content'>";
            $html .= "<h2>SOS Consulting: Zoho to Xero Sync Report for $syncStartDateTime</h2>";
            $html .= "<br></h2>";
            $html .= "<table border='1' cellpadding='5' style='border-collapse: collapse'>";
            $html .= "<tr><th style='white-space: nowrap;'>Invoice Number</th><th style='white-space: nowrap;'>Invoice Type</th><th style='white-space: nowrap;'>Sync Status</th><th style='white-space: nowrap;'>Info Log</th><th style='white-space: nowrap;'>Non Fatal Error Log</th><th style='white-space: nowrap;'>Fatal Error Log</th></tr>";
            foreach($unSyncedRecords as $unSyncedRecord){
                $html .= "<tr>";
                $html .= "<td align='center'>{$unSyncedRecord->invoice_number}</td>";
                $html .= "<td align='center'>{$unSyncedRecord->invoice_type}</td>";
                $html .= "<td align='center'>{$unSyncedRecord->sync_status}</td>";
                $html .= "<td align='left'>{$this->parseLog($unSyncedRecord->sync_log)}</td>";
                $html .= "<td align='left'>{$this->parseLog($unSyncedRecord->sync_errors)}</td>";
                $html .= "<td align='left'>{$this->parseLog($unSyncedRecord->sync_fatal_errors)}</td>";
                $html .= "</tr>";
                $failCount++;
            }

            foreach($syncedRecords as $syncedRecord){
                $html .= "<tr>";
                $html .= "<td align='center'>{$syncedRecord->invoice_number}</td>";
                $html .= "<td align='center'>{$syncedRecord->invoice_type}</td>";
                $html .= "<td align='center'>{$syncedRecord->sync_status}</td>";
                $html .= "<td align='left'>{$this->parseLog($syncedRecord->sync_log)}</td>";
                $html .= "<td align='left'>{$this->parseLog($syncedRecord->sync_errors)}</td>";
                $html .= "<td align='left'>{$this->parseLog($syncedRecord->sync_fatal_errors)}</td>";
                $html .= "</tr>";
                $successCount++;
            }

            $html .= "</table>";
            $html .= "<br><br><b> Success Count:</b> {$successCount}";
            $html .= "<br><b> Fail Count:</b> {$failCount}";
            $total = (int)$successCount + (int)$failCount;
            $html .= "<br><br><b> Total Processed:</b> {$total}";
            $html .= "</div></body>";
            $html .= "</html>";

            Storage::disk('local')->put($attachmentFileName, $html);

            return $attachmentFileName;
        }

        return false;
    }


    private function parseLog($log){
        if(empty($log)){
            return;
        }

        $logArray = json_decode($log, true);
        $logHtml = '';
        foreach($logArray as $logKey => $logDetail){
            $category = ucfirst($logKey);
            $html = "<b>{$category}</b></br>";
            $html .= "<ul>";
            foreach($logDetail as $detail){
                $html .= "<li>{$detail}</li>";
            }
            $html .= "</ul>";
            $logHtml .= $html;
        }

        return $logHtml;
    }
}
