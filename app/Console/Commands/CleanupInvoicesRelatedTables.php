<?php

namespace SOSZohoXeroIntegration\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CleanupInvoicesRelatedTables extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CleanupInvoicesRelatedTables';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $xeroInvoiceRelatedTables = [
            'xero_contacts',
            'ap_xero_to_zoho_zoho_contacts',
            'ap_xero_to_zoho_zoho_po_receipts',
            'ap_xero_to_zoho_zoho_vendors',
        ];

        $zohoInvoiceRelatedTables = [
            'zoho_to_xero_xero_contacts',
            'zoho_to_xero_xero_invoices',
            'zoho_to_xero_zoho_accounts',
            'zoho_to_xero_zoho_contacts',
        ];

        $xeroAPInvoiceStatusUpdateRelatedTables = [
            'ap_xero_invoices_for_xero_status_update'
        ];

        $this->cleanupDb($xeroInvoiceRelatedTables, 'xero_invoice_queue');
        $this->cleanupDb($zohoInvoiceRelatedTables, 'zoho_invoice_queue');
        $this->cleanupDb($xeroAPInvoiceStatusUpdateRelatedTables, 'ap_zoho_invoice_for_xero_status_update');

        return true;
    }

    public function cleanupDb($relatedTables, $parentTable){
        foreach($relatedTables as $relatedTable){
            $tableBatchNumbers = DB::table($relatedTable)->select('batch_number')->groupBy('batch_number')->get()->toArray();

            if(!empty($tableBatchNumbers)){
                foreach($tableBatchNumbers as $batchNumber){
                    $batchNumberInUseInParent = DB::table($parentTable)->where('batch_number', '=', $batchNumber->batch_number)->count();

                    if(empty($batchNumberInUseInParent)){
                        DB::table($relatedTable)->select('id')->where('batch_number', '=', $batchNumber->batch_number)->delete();
                    }
                }
            }
        }
    }
}
