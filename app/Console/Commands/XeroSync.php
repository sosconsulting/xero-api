<?php

namespace SOSZohoXeroIntegration\Console\Commands;

require 'vendor/autoload.php';
use Illuminate\Console\Command;
use SOSZohoXeroIntegration\SyncHelpers;
use Illuminate\Support\Facades\DB;
use XeroPHP\Application\PrivateApplication;

class XeroSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SyncZohoToXero';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Limit of records to process
     * @var int
     */
    private $queryLimit = 25;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \ZCRMRestClient::initialize();
        $xero = new PrivateApplication(config('sync'));

        $recordsToProcess = [];
        $excludedIds = [];
        $onQueueRecords = $this->getAllQueueCount($excludedIds);
        $queryZohoTableQueue = $this->getZohoRecords($excludedIds);

        while (
            count($recordsToProcess) < $this->queryLimit
            && count($onQueueRecords) > 0
        ) {
            foreach ($queryZohoTableQueue as $focus) {
                if (count($recordsToProcess) == $this->queryLimit) {
                    continue;
                }

                $processNow = $this->checkIfProcessingARNow($focus);

                if (!empty($processNow)) {
                    array_push($recordsToProcess, $focus);
                }

                array_push($excludedIds, $focus->id);
            }
            $onQueueRecords = $this->getAllQueueCount($excludedIds);
            $queryZohoTableQueue = $this->getZohoRecords($excludedIds);
        }

        foreach ($recordsToProcess as $record) {
            if(!empty($record->id) && !empty($record->record_details)){
                switch ($record->invoice_type){
                    case 'ACCREC':
                        DB::table('zoho_invoice_queue')->where('id', $record->id)->update(['status' => 'processing', 'process_start_time' => DB::raw('CURRENT_TIMESTAMP')]);
                        $accRec= new SyncHelpers\ZohoToXeroAccountsReceivable();
                        $syncLog = $accRec->processData($record, $xero);
                        $this->processLogs($syncLog, $record);
                        break;
                }
            }
        }

        return true;
    }

    private function checkIfProcessingARNow($record){
        $xeroInvoicesQueue = DB::table('xero_invoice_queue')
            ->where('invoice_number', "{$record->invoice_number}")
            ->where(function ($query){
                $query->where('status', 'queue')
                    ->orWHere('status', 'processing');
            })
            ->oldest()
            ->get()
            ->toArray();

        if(empty($xeroInvoicesQueue)){
            return true;
        }

        $dtZohoRecordUpdatedDate = new \DateTime($record->record_updated_date);
        $dtZohoRecordUpdatedDate->setTimezone(new \DateTimeZone("UTC"));
        $zohoRecordUpdatedDate = $dtZohoRecordUpdatedDate->format('Y-m-d H:i:s');

        foreach($xeroInvoicesQueue as $invoice){
            $dtXeroRecordUpdatedDate = new \DateTime($invoice->record_updated_date);
            $dtXeroRecordUpdatedDate->setTimezone(new \DateTimeZone("UTC"));
            $xeroRecordUpdatedDate = $dtXeroRecordUpdatedDate->format('Y-m-d H:i:s');

            if ($zohoRecordUpdatedDate > $xeroRecordUpdatedDate){
                return false;
            }
        }

        return true;
    }

    private function processLogs($syncDetails, $record){
        if(!empty($syncDetails['fatal_error'])){
            $notFatalError = !empty($syncDetails['error']) ? $syncDetails['error'] : [];
            $fatalError = !empty($syncDetails['fatal_error']) ? $syncDetails['fatal_error'] : [];

            DB::table('zoho_invoice_queue')->where('id', $record->id)->update([
                'status' => 'not_processed',
                'sync_errors' => json_encode($notFatalError),
                'sync_fatal_errors' => json_encode($fatalError),
                'process_end_time' => DB::raw('CURRENT_TIMESTAMP'),
                'sync_status' => 'failed',
                'synced_today' => 1
            ]);
            return;
        }

        $notFatalError = !empty($syncDetails['error'])  ? json_encode($syncDetails['error']) : null;
        $info = !empty($syncDetails['info']) ? json_encode($syncDetails['info']) : null;

        DB::table('zoho_invoice_queue')->where('id', $record->id)->update([
            'status' => 'synced',
            'sync_errors' => $notFatalError,
            'sync_log' => $info,
            'process_end_time' => DB::raw('CURRENT_TIMESTAMP'),
            'sync_status' => 'success',
            'synced_today' => 1
        ]);
    }


    private function getZohoRecords($excludedIds)
    {
        if (!empty($excludedIds)) {
            $result = DB::table('zoho_invoice_queue')->where('status', 'queue')->whereNotIn('id', $excludedIds)->orderBy('record_updated_date')->limit($this->queryLimit)->get()->toArray();
        } else {
            $result = DB::table('zoho_invoice_queue')->where('status', 'queue')->orderBy('record_updated_date')->limit($this->queryLimit)->get()->toArray();
        }

        return $result;
    }

    private function getAllQueueCount($excludedIds)
    {
        if (!empty($excludedIds)) {
            $result = DB::table('zoho_invoice_queue')->where('status', 'queue')->whereNotIn('id', $excludedIds)->orderBy('record_updated_date')->get()->toArray();
        } else {
            $result = DB::table('zoho_invoice_queue')->where('status', 'queue')->orderBy('record_updated_date')->get()->toArray();
        }

        return $result;
    }
}
