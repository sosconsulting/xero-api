<?php

namespace SOSZohoXeroIntegration\Console\Commands;

require 'vendor/autoload.php';
use SOSZohoXeroIntegration;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use XeroPHP\Application\PrivateApplication;
use XeroPHP\Models\Accounting\Contact;

class GatherRecordFromXero extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GatherXeroInvoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Invoices from Xero and Load it in temporary table';

    /**
     * Queuing batch number
     * @var bool
     */
    protected $batchNumber = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return bool
     * @throws \Exception
     */
    public function handle()
    {
        $lastRecordGatheringDate = DB::table('config')->where('category', 'Xero Sync Config')->where('name','last_record_gathering_date')->first();
        $currentTimeStamp = gmdate("Y-m-d\TH:i:s");

        // Initialize date for gathering records and exit since this is the initialization
        if (empty($lastRecordGatheringDate->value)) {
            $lastRecordGatheringDate = (object) ['value' => $currentTimeStamp];
            DB::table('config')->insert(
                [
                    'category' => 'Xero Sync Config',
                    'name' => 'last_record_gathering_date',
                    'value' => $currentTimeStamp,
                    'platform' => 'base',
                ]
            );
        }

        $batchNumber = DB::table('config')->where('category', 'Xero Sync Config')->where('name','batch_number')->first();
        if(empty($batchNumber->value)){
            $queryRecentBatchId = DB::table('xero_invoice_queue')->max('batch_number');
            $recentBatchId = !empty($queryRecentBatchId) ? $queryRecentBatchId : 0;
            $batchNumber = $recentBatchId + 1;
            DB::table('config')->insert(
                [
                    'category' => 'Xero Sync Config',
                    'name' => 'batch_number',
                    'value' => $batchNumber,
                    'platform' => 'base',
                ]
            );
        }else{
            $batchNumber = $batchNumber->value + 1;
            // Update Batch Id Number in config
            DB::table('config')->where('category', 'Xero Sync Config')->where('name', 'batch_number')->update(['value' => $batchNumber]);
        }

        $this->batchNumber = $batchNumber;
        $invoiceDateLimit = DB::table('config')->where('category', 'Xero Sync Config')->where('name','invoice_date_limit')->first();

        $xero = new PrivateApplication(config('sync'));

        // 2018-11-22T06:02:58 $lastRecordGatheringDate->value)
        try{
            $xeroInvoices = $xero->load('Accounting\\Invoice')->modifiedAfter(new \DateTime($lastRecordGatheringDate->value))->execute();
        }
        catch(\Exception $e){
            DB::table('xero_invoice_queue')
                ->where('batch_number', $batchNumber)
                ->update([
                    'status' => 'latest_updated_invoices_not_completely_retrieved',
                    'sync_fatal_errors' => 'Will not queue this record, retrieving of Xero invoices failed, some of it are missing. Will try again later to gather complete recently updated xero invoices'
                ]);
            exit();
        }

        $recordInsertedInQueue = 0;

        foreach ($xeroInvoices as $record) {
            $data = $this->parseXeroData($record);

            // DISABLE PROCESSING ACCOUNTS PAYABLE SINCE THIS IS NOT NEEDED IN ASSA ACCOUNT
            //if($data->Type == 'ACCPAY'){
            //    continue;
            //}

            //Skips through draft invoices that aren't Accounts Receivable
            if(strtolower($data->Status) == "draft" && $data->Type == 'ACCREC'){
                continue;
            }

            //Skips through records whose invoice date is earlier than the designated date
            if(!empty($invoiceDateLimit->value) && $data->Date < $invoiceDateLimit->value){
                continue;
            }
            
            if(empty($data->InvoiceNumber)){
                continue;
            }

            $insert = $this->checkIfLoopUpdate($data->InvoiceNumber, $data->UpdatedDateUTC);
            if($insert){
                $recordInsertedInQueue++;
                DB::table('xero_invoice_queue')->insert(
                    [
                        'invoice_type' =>  !empty($data->Type) ? $data->Type : '',
                        'invoice_number' => !empty($data->InvoiceNumber) ? $data->InvoiceNumber : '',
                        'batch_number' => $batchNumber,
                        'record_updated_date' =>  !empty($data->UpdatedDateUTC) ? $data->UpdatedDateUTC : '',
                        'record_details' => json_encode($record),
                        'status' => 'waiting_related_records'
                    ]
                );
            }
        }

        if($recordInsertedInQueue > 0){
            try{
                $xeroContacts = $xero->load('Accounting\\Contact')->execute();
            }
            catch(\Exception $e){
                DB::table('xero_invoice_queue')
                    ->where('batch_number', $batchNumber)
                    ->update([
                        'status' => 'related_records_was_not_retrieved',
                        'sync_fatal_errors' => 'Will not queue this record, retrieving of Xero Contacts failed. Will try again later to gather and queue latest updated xero invoices"'
                    ]);

                exit();
            }

            foreach ($xeroContacts as $record) {
                $data = $this->parseXeroData($record);
                DB::table('xero_contacts')->insert(
                    [
                        'contact_id' => $data->ContactID,
                        'batch_number' => $batchNumber,
                        'website' => !empty($data->website) ? $data->website : '',
                        'record_details' => json_encode($data),
                    ]
                );
            }

             \ZCRMRestClient::initialize();
             $this->fetchZohoVendors($batchNumber);
             $this->fetchZohoContacts($batchNumber);
             $this->fetchZohoPOReceipts($batchNumber);

            // Now set the invoice to queue since related records is already fetched
            DB::table('xero_invoice_queue')->where('batch_number', $batchNumber)->update(['status' => 'queue']);
        }

        // Update last_record_gathering_date in db
        DB::table('config')->where('category', 'Xero Sync Config')->where('name', 'last_record_gathering_date')->update(['value' => $currentTimeStamp]);

        return true;
    }


    private function fetchZohoVendors($batchNumber){
        $response = $this->fetchZohoRecords('Vendors', 1);

        if(!empty($response['bulk_data_json']['data'])){
            foreach($response['bulk_data_json']['data'] as $data){
                DB::table('ap_xero_to_zoho_zoho_vendors')->insert(
                    [
                        'vendor_id' => !empty($data['id']) ? $data['id'] : '',
                        'xero_contact_id' => !empty($data['Xero_Contact_ID']) ? $data['Xero_Contact_ID'] : '',
                        'vendor_name' => !empty($data['Vendor_Name']) ? $data['Vendor_Name'] : '',
                        'batch_number' => $batchNumber,
                        'record_details' => json_encode($data),
                    ]
                );
            }
        }

        while(!empty($response['more_records'])){
            $pageNumber = $response['page_number'] + 1;
            $response = $this->fetchZohoRecords('Vendors', $pageNumber);
            if(!empty($response['bulk_data_json']['data'])){
                foreach($response['bulk_data_json']['data'] as $data){
                    DB::table('ap_xero_to_zoho_zoho_vendors')->insert(
                        [
                            'vendor_id' => !empty($data['id']) ? $data['id'] : '',
                            'xero_contact_id' => !empty($data['Xero_Contact_ID']) ? $data['Xero_Contact_ID'] : '' ,
                            'vendor_name' => !empty($data['Vendor_Name']) ? $data['Vendor_Name'] : '',
                            'batch_number' => $batchNumber,
                            'record_details' => json_encode($data),
                        ]
                    );
                }
            }
        }
    }

    private function fetchZohoContacts($batchNumber){
        $response = $this->fetchZohoRecords('Contacts', 1);

        if(!empty($response['bulk_data_json']['data'])){
            foreach($response['bulk_data_json']['data'] as $data){
                DB::table('ap_xero_to_zoho_zoho_contacts')->insert(
                    [
                        'contact_id' => $data['id'],
                        'email_address' => !empty($data['Email']) ? $data['Email'] : '',
                        'batch_number' => $batchNumber,
                        'record_details' => json_encode($data),
                    ]
                );
            }
        }

        while(!empty($response['more_records'])){
            $pageNumber = $response['page_number'] + 1;
            $response = $this->fetchZohoRecords('Contacts', $pageNumber);
            if(!empty($response['bulk_data_json']['data'])){
                foreach($response['bulk_data_json']['data'] as $data){
                    DB::table('ap_xero_to_zoho_zoho_contacts')->insert(
                        [
                            'contact_id' => $data['id'],
                            'email_address' => !empty($data['Email']) ? $data['Email'] : '',
                            'batch_number' => $batchNumber,
                            'record_details' => json_encode($data),
                        ]
                    );
                }
            }
        }
    }

    private function fetchZohoPOReceipts($batchNumber){
        $response = $this->fetchZohoRecords('PO_Receipts', 1);

        if(!empty($response['bulk_data_json']['data'])){
            foreach($response['bulk_data_json']['data'] as $data){
                DB::table('ap_xero_to_zoho_zoho_po_receipts')->insert(
                    [
                        'po_receipts_id' => $data['id'],
                        'invoice_number' => !empty($data['Invoice_Number']) ? $data['Invoice_Number'] : '',
                        'batch_number' => $batchNumber,
                        'record_details' => json_encode($data),
                    ]
                );
            }
        }

        while(!empty($response['more_records'])){
            $pageNumber = $response['page_number'] + 1;
            $response = $this->fetchZohoRecords('PO_Receipts', $pageNumber);
            if(!empty($response['bulk_data_json']['data'])){
                foreach($response['bulk_data_json']['data'] as $data){
                    DB::table('ap_xero_to_zoho_zoho_po_receipts')->insert(
                        [
                            'po_receipts_id' => $data['id'],
                            'invoice_number' => !empty($data['Invoice_Number']) ? $data['Invoice_Number'] : '',
                            'batch_number' => $batchNumber,
                            'record_details' => json_encode($data),
                        ]
                    );
                }
            }
        }
    }

    private function fetchZohoRecords($module, $pageNo){
        $response = [];

        try{
            $zohoModuleIns = \ZCRMModule::getInstance($module);
            $bulkAPIResponse=$zohoModuleIns->getRecords(null, null, null, $pageNo, 1000, null);
            $bulkAPIJsonResponse = $bulkAPIResponse->getResponseJSON();

            $responseInfo = $bulkAPIResponse->getInfo();
            $response['more_records'] = $responseInfo->getMoreRecords();
            $response['page_number'] = $responseInfo->getPageNo();
            $response['bulk_data_json'] = $bulkAPIJsonResponse;

            return $response;
        }catch(\Exception $e){
            if ($e->getMessage() == 'Undefined index: data') {
                return $response;
            }
            $this->failedRetrievingRelatedRecords('Zoho '. $module);
        }
    }

    private function failedRetrievingRelatedRecords($module){
        DB::table('xero_invoice_queue')
            ->where('batch_number', $this->batchNumber)
            ->update([
                "status" => "related_records_was_not_retrieved",
                "sync_fatal_errors" => "Will not queue this record, retrieving of $module failed. Will try again later to gather and queue latest updated zoho invoices"
            ]);
        exit();

    }

    public function parseXeroData($record){
        $jsonEncodedData = json_encode($record);
        return json_decode($jsonEncodedData);
    }


    /**
     * Prevent loop update
     * Don't insert in queue table if the update was triggered by syncapp because it will loop.
     * Queue record update only if it was triggered by the xero application
     *
     * @param $invoiceNumber
     * @param $modifiedDate
     * @return bool
     * @throws \Exception
     */
    private function checkIfLoopUpdate($invoiceNumber, $modifiedDate){
        if(empty($invoiceNumber) || empty($modifiedDate)){
            return true;
        }

        $modifiedDt = new \DateTime($modifiedDate);
        $modifiedDt->setTimezone(new \DateTimeZone("UTC"));
        $modifiedDateInUtc = $modifiedDt->format("Y-m-d H:i:s");

        $sameInvoiceNumberInRecentSyncappUpdatesTable = DB::table('xero_invoice_update_timestamps')->select('updated_date')->where('invoice_number', $invoiceNumber)->whereRaw("Date(updated_date) = CURDATE()")->get()->toArray();

        foreach($sameInvoiceNumberInRecentSyncappUpdatesTable as $syncappXeroRecordUpdateTimeStamp){
            $xeroUpdateTimeStampDt = new \DateTime($syncappXeroRecordUpdateTimeStamp->updated_date);

            // Define time range
            $xeroUpdateTimeStampDt->add(new \DateInterval('PT2M'));
            $xeroUpdateTimetStamPlus2Mins = $xeroUpdateTimeStampDt->format("Y-m-d H:i:s");
            $xeroUpdateTimeStampDt->sub(new \DateInterval('PT3M'));
            $xeroUpdateTimetStamMinus1Min = $xeroUpdateTimeStampDt->format("Y-m-d H:i:s");

            // Check if updated date is in between the recent update minus 1 minute and recent update plus 2 minutes
            if (($xeroUpdateTimetStamMinus1Min <= $modifiedDateInUtc) && ($xeroUpdateTimetStamPlus2Mins >= $modifiedDateInUtc)){
                return false;
            }
        }

        return true;
    }
}
