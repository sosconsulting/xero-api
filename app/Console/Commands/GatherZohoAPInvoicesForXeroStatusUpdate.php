<?php

namespace SOSZohoXeroIntegration\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use XeroPHP\Application\PrivateApplication;
use XeroPHP\Models\Accounting\Invoice;

class GatherZohoAPInvoicesForXeroStatusUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GatherZohoAPInvoicesForXeroStatusUpdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get zoho AP Invoices for updating Xero Status';

    /**
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Method to get the AP Invoices
     * @return bool
     * @throws \XeroPHP\Remote\Exception
     */
    public function handle()
    {
        \ZCRMRestClient::initialize();
        $xero = new PrivateApplication(config('sync'));

        $lastRecordGatheringDate = DB::table('config')->where('category', 'Zoho AP Status Sync Config')->where('name','last_ap_invoice_gathering_date')->first();


        $datetime = new \DateTime();
        $currentTimeStamp =  $datetime->format(\DateTime::ATOM);

        // Initialize date for gathering records and exit since this is the initialization
        if(empty($lastRecordGatheringDate->value)) {
            $lastRecordGatheringDate = ['value' => $currentTimeStamp];
            DB::table('config')->insert(
                [
                    'category' => 'Zoho AP Status Sync Config',
                    'name' => 'last_ap_invoice_gathering_date',
                    'value' => $currentTimeStamp,
                    'platform' => 'base',
                ]
            );
        }

        DB::table('config')->where('category', 'Zoho AP Status Sync Config')->where('name', 'last_ap_invoice_gathering_date')->update(['value' => $currentTimeStamp]);

        $batchNumber = DB::table('config')->where('category', 'Zoho AP Status Sync Config')->where('name','batch_number')->first();

        if(empty($batchNumber->value)){
            $queryRecentBatchId = DB::table('ap_zoho_invoice_for_xero_status_update')->max('batch_number');
            $recentBatchId = !empty($queryRecentBatchId) ? $queryRecentBatchId : 0;
            $batchNumber = $recentBatchId + 1;
            DB::table('config')->insert(
                [
                    'category' => 'Zoho AP Status Sync Config',
                    'name' => 'batch_number',
                    'value' => $batchNumber,
                    'platform' => 'base',
                ]
            );
        }else{
            $batchNumber = $batchNumber->value + 1;
            // Update Batch Id Number in config
            DB::table('config')->where('category', 'Zoho AP Status Sync Config')->where('name', 'batch_number')->update(['value' => $batchNumber]);
        }


        $toSyncRecords = [];
        try {
            $response = $this->fetchZohoInvoices(1, $lastRecordGatheringDate->value);

            if(!empty($response['bulk_data_json']['data'])){
                foreach($response['bulk_data_json']['data'] as $data){
                    $insert = $this->checkIfLoopUpdate($data['Invoice_Number'], $data['Modified_Time']);
                    if($insert){
                        // Don't insert if record has no receipt status or no invoice number
                        if(empty($data['Receipt_Status']) || empty($data['Invoice_Number'])){
                            continue;
                        }
                        array_push($toSyncRecords, $data);
                        // else insert
                        DB::table('ap_zoho_invoice_for_xero_status_update')->insert(
                            [
                                'invoice_number' => $data['Invoice_Number'],
                                'invoice_receipt_status' => $data['Receipt_Status'],
                                'batch_number' => $batchNumber,
                                'record_updated_date' => $data['Modified_Time'],
                                'record_details' => json_encode($data),
                                'status' => 'waiting_related_records'
                            ]
                        );
                    }
                }
            }

            while(!empty($response['more_records'])){
                $pageNumber = $response['page_number'] + 1;
                $response = $this->fetchZohoInvoices($pageNumber, $lastRecordGatheringDate->value);
                if(!empty($response['bulk_data_json']['data'])){
                    foreach($response['bulk_data_json']['data'] as $data){
                        $insert = $this->checkIfLoopUpdate($data['Invoice_Number'], $data['Modified_Time']);
                        if($insert){
                            // Don't insert if record has no receipt status or no invoice number
                            if(empty($data['Receipt_Status']) || empty($data['Invoice_Number'])){
                                continue;
                            }
                            array_push($toSyncRecords, $data);
                            // else insert
                            DB::table('ap_zoho_invoice_for_xero_status_update')->insert(
                                [
                                    'invoice_number' => $data['Invoice_Number'],
                                    'invoice_receipt_status' => $data['Receipt_Status'],
                                    'batch_number' => $batchNumber,
                                    'record_updated_date' => $data['Modified_Time'],
                                    'record_details' => json_encode($data),
                                    'status' => 'waiting_related_records'
                                ]
                            );
                        }
                    }
                }
            }

        }catch (\Exception $e){
            if ($e->getMessage() == 'Undefined index: data') {
                return false;
            }
        }

        if(!empty($toSyncRecords)){
            $xeroApInvoiceQuery = $xero->load(Invoice::class)
                ->where('Type', 'ACCPAY')
                ->execute();

            foreach ($xeroApInvoiceQuery as $record) {
                $data = $this->parseXeroData($record);
                // Don't insert if record is deleted or has an empty invoice number
                if($data->Status == 'DELETED' || empty($data->InvoiceNumber)){
                    continue;
                }

                // else insert
                DB::table('ap_xero_invoices_for_xero_status_update')->insert(
                    [
                        'invoice_number' => !empty($data->InvoiceNumber) ? $data->InvoiceNumber : '',
                        'batch_number' => $batchNumber,
                        'invoice_type' => $data->Type,
                        'invoice_status' => $data->Status,
                        'record_details' => json_encode($record),
                    ]
                );
            }

            DB::table('ap_zoho_invoice_for_xero_status_update')->where('batch_number', $batchNumber)->update(['status' => 'queue']);
        }
    }

    private function fetchZohoInvoices($pageNo, $lastRecordGatheringDate){
        $response = [];

        try{
            $zohoModuleIns = \ZCRMModule::getInstance('PO_Receipts');
            $bulkAPIResponse=$zohoModuleIns->getRecords(null, null, null, $pageNo, 1000, ['If-Modified-Since' => $lastRecordGatheringDate]);
            $bulkAPIJsonResponse = $bulkAPIResponse->getResponseJSON();

            $responseInfo = $bulkAPIResponse->getInfo();
            $response['more_records'] = $responseInfo->getMoreRecords();
            $response['page_number'] = $responseInfo->getPageNo();
            $response['bulk_data_json'] = $bulkAPIJsonResponse;

            return $response;
        }catch(\Exception $e){
            if ($e->getMessage() == 'Undefined index: data') {
                return $response;
            }
        }

        return $response;
    }

    private function checkIfLoopUpdate($invoiceNumber, $modifiedDate){
        if(empty($invoiceNumber) || empty($modifiedDate)){
            return false;
        }

        $modifiedDt = new \DateTime($modifiedDate);
        $modifiedDt->setTimezone(new \DateTimeZone("UTC"));
        $modifiedDateInUtc = $modifiedDt->format("Y-m-d H:i:s");

        $sameInvoiceNumberInRecentSyncappUpdatesTable = DB::table('zoho_invoice_update_timestamps')->select('updated_date')->where('invoice_number', $invoiceNumber)->whereRaw("Date(updated_date) = CURDATE()")->get()->toArray();

        foreach($sameInvoiceNumberInRecentSyncappUpdatesTable as $syncappZohoRecordUpdateTimeStamp){
            $zohoUpdateTimeStampDt = new \DateTime($syncappZohoRecordUpdateTimeStamp->updated_date);

            // Define time range
            $zohoUpdateTimeStampDt->add(new \DateInterval('PT2M'));
            $zohoUpdateTimetStamPlus2Mins = $zohoUpdateTimeStampDt->format("Y-m-d H:i:s");
            $zohoUpdateTimeStampDt->sub(new \DateInterval('PT3M'));
            $zohoUpdateTimetStamMinus1Min = $zohoUpdateTimeStampDt->format("Y-m-d H:i:s");

            // Check if updated date is in between the recent update minus 1 minute and recent update plus 2minutes
            if (($zohoUpdateTimetStamMinus1Min <= $modifiedDateInUtc) && ($zohoUpdateTimetStamPlus2Mins >= $modifiedDateInUtc  )){
                return false;
            }
        }

        return true;
    }

    public function parseXeroData($record){
        $jsonEncodedData = json_encode($record);
        return json_decode($jsonEncodedData);
    }
}

