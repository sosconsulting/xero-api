<?php

namespace SOSZohoXeroIntegration\Console\Commands;

require 'vendor/autoload.php';
use SOSZohoXeroIntegration;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use XeroPHP\Application\PrivateApplication;
use XeroPHP\Models\Accounting\Invoice;

class UpdateZohoApPaymentDetails extends Command
{
    private $log = [];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateZohoApPaymentDetails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates Payment Details of Zoho AP Invoices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *  Execute the console command.
     *
     * @return bool
     * @throws \XeroPHP\Exception
     * @throws \XeroPHP\Remote\Exception
     * @throws \XeroPHP\Remote\Exception\NotFoundException
     */
    public function handle()
    {

        $lastDateOfDataGathering = DB::table('config')->where('category', 'Xero Sync Config')->where('name','last_record_gathering_ap_payment_details')->first();
        $currentTimeStamp = gmdate("Y-m-d\TH:i:s");

        // Initialize date for gathering records and exit since this is the initialization
        if (empty($lastDateOfDataGathering->value)) {
            DB::table('config')->insert(
                [
                    'category' => 'Xero Sync Config',
                    'name' => 'last_record_gathering_ap_payment_details',
                    'value' => $currentTimeStamp,
                    'platform' => 'base',
                ]
            );
            return true;
        }

        // Update last_record_gathering_date in db
        DB::table('config')->where('category', 'Xero Sync Config')->where('name', 'last_record_gathering_ap_payment_details')->update(['value' => $currentTimeStamp]);

        $xero = new PrivateApplication(config('sync'));
        \ZCRMRestClient::initialize();

        // 2018-11-22T06:02:58 $lastDateOfDataGathering->value)
        $recordsToSync = $xero->load('Accounting\\Payment')
            ->where('PaymentType', 'ACCPAYPAYMENT')
            ->modifiedAfter(new \DateTime($lastDateOfDataGathering->value))
            ->execute();

        foreach ($recordsToSync as $record) {
            $data = $this->parseXeroData($record);
            $invoiceId = $data->Invoice->InvoiceID;
            if(empty($invoiceId)){
                continue;
            }

            $xeroApInvoiceQuery = $xero->loadByGUID(Invoice::class, $invoiceId);
            $xeroApInvoiceData = $this->parseXeroData($xeroApInvoiceQuery);
            $zohoInvoiceId = $this->searchForZohoInvoice($data->Invoice->InvoiceNumber);

            try{
                $zohoInvoice = \ZCRMRecord::getInstance("PO_Receipts", $zohoInvoiceId);
                $zohoInvoice->setFieldValue('Due_Date', $xeroApInvoiceData->DueDate);
                $zohoInvoice->setFieldValue('Total_Excluding_GST', $xeroApInvoiceData->SubTotal);
                $zohoInvoice->setFieldValue('GST_Value', $xeroApInvoiceData->TotalTax);
                $zohoInvoice->setFieldValue('Amount_Paid', $xeroApInvoiceData->AmountPaid);
                $zohoInvoice->setFieldValue('Last_Date_Paid', $data->Date);
                $updateRecordResponse = $zohoInvoice->update();

                if($updateRecordResponse->getStatus() == 'success' && $updateRecordResponse->getMessage() == 'record updated'){
                    $this->log['good'][] = "AP Invoice Record is updated with id: {$invoiceId}";
                }else{
                    $this->log['bad'][] = "AP Invoice Record is not updated with id: {$invoiceId}";
                }
            }catch(\Exception $e){

            }
        }
    }

    private function searchForZohoInvoice($xeroInvoiceNumber){
        $invoiceId = '';
        $apInvoiceIns = \ZCRMModule::getInstance("PO_Receipts");
        $criteria = "(Invoice_Number:equals:$xeroInvoiceNumber)";
        $invoiceQueryResponse = $apInvoiceIns->searchRecordsByCriteria($criteria);
        $record = $invoiceQueryResponse->getResponseJSON();
        if (!empty($record['data'][0]['id'])) {
            $invoiceId = $record['data'][0]['id'];
        }
        return $invoiceId;
    }


    public function parseXeroData($record){
        $jsonEncodedData = json_encode($record);
        return json_decode($jsonEncodedData);
    }
}
