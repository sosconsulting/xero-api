<?php

namespace SOSZohoXeroIntegration\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class InitializeZohoToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'InitializeZohoToken';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize zoho token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \ZCRMRestClient::initialize();

        $tokenInitialized = DB::table('config')->where('category', 'Zoho Sync Config')->where('name','token_initialized')->first();

        if ($tokenInitialized != true || empty($tokenInitialized->value)){
            $this->initializeTokens();
        }
    }

    private function initializeTokens(){

        $oAuthClient = \ZohoOAuth::getClientInstance();
        $grantToken = config('sync.zoho.grant_token');
        $oAuthTokens = $oAuthClient->generateAccessToken($grantToken);

        if(!empty($oAuthTokens->getRefreshToken())){
            $tokenInitialized = DB::table('config')->where('category', 'Zoho Sync Config')->where('name','token_initialized')->first();

            if (!empty($tokenInitialized->value)) {
                DB::table('config')->where('category', 'Zoho Sync Config')->where('name',
                    'token_initialized')->update(['value' => '1']);
            } else {
                DB::table('config')->insert(
                    [
                        'category' => 'Zoho Sync Config',
                        'name' => 'token_initialized',
                        'value' => true,
                        'platform' => 'base',
                    ]
                );
            }
        }
    }
}
