<?php

namespace SOSZohoXeroIntegration\Console\Commands;

require 'vendor/autoload.php';

use function GuzzleHttp\Psr7\str;
use SOSZohoXeroIntegration;
use Illuminate\Console\Command;
use XeroPHP\Application\PrivateApplication;
use XeroPHP\Models\Accounting\Invoice;
use Illuminate\Support\Facades\DB;

class UpdateXeroApStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateXeroApStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates Xero AP Invoices status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @throws \XeroPHP\Exception
     */
    public function handle()
    {
        \ZCRMRestClient::initialize();
        $xero = new PrivateApplication(config('sync'));

        $zohoInvoices = DB::table('ap_zoho_invoice_for_xero_status_update')->where('status', 'queue')->orderByDesc('record_updated_date')->get()->toArray();

        $invoicesToUpdate = [];
        foreach($zohoInvoices as $zohoInvoice){
            $this->updateRecordQueueStatus($zohoInvoice->id, 'processing');
            $status = null;

            $zohoDetail = json_decode($zohoInvoice->record_details);
            $xeroFocus = $this->queryXeroFocus($zohoDetail->Invoice_Number, $zohoInvoice->batch_number);

            if(empty($xeroFocus->id)){
                $this->updateRecordQueueStatus($zohoInvoice->id, 'not_processed', array('error' => "Invoice doesn't exist in xero"));
                continue;
            }

            $xeroDetail = json_decode($xeroFocus->record_details);

            $receiptStatus = $zohoDetail->Receipt_Status;
            switch(strtolower($receiptStatus)){
                case 'new':
                case 'missing po':
                    if($xeroDetail->Status != 'DRAFT' && !empty($xeroDetail->InvoiceID)){
                        $status = 'DRAFT';
                    }
                    break;
                case 'invalid details':
                case 'need supplier licence':
                    if($xeroDetail->Status == 'DRAFT' && !empty($xeroDetail->InvoiceID) && !empty($zohoDetail->Purchase_Order)){
                        $status = 'SUBMITTED';
                    }
                    break;
                case 'receipted':
                    if(($xeroDetail->Status == 'DRAFT' || $xeroDetail->Status == 'SUBMITTED')
                        && !empty($xeroDetail->InvoiceID)
                        && !empty($zohoDetail->Purchase_Order)
                    ){
                        $status = 'AUTHORISED';
                    }
            }

            if (!empty($status)) {
                $this->updateRecordQueueStatus($zohoInvoice->id, 'processed', array('info' => "Update was processed, xero will handle if the record is ok for update or not"));
                $xeroInvoice = new Invoice($xero);
                $xeroInvoice->setInvoiceID($xeroDetail->InvoiceID);
                $xeroInvoice->setStatus($status);

                $invoicesToUpdate[] = $xeroInvoice;
            }else{
                $this->updateRecordQueueStatus($zohoInvoice->id, 'processed', array('info' => "Processed, no action required"));
            }
        }
        if(!empty($invoicesToUpdate)){
            $response = $xero->saveAll($invoicesToUpdate);
        }
    }

    public function queryXeroFocus($invoiceNumber, $batchNumber){
        $xeroInvoice = DB::table('ap_xero_invoices_for_xero_status_update')->where('invoice_number', $invoiceNumber)->where('batch_number', $batchNumber)->first();
        return $xeroInvoice;
    }

    public function updateRecordQueueStatus($id, $status, $logs = array()){
        if(empty($id)){
            return;
        }

        $error = '';
        $info = '';
        $syncStatus = '';
        if(!empty($logs)){
            foreach($logs as $key => $log){
                if($key == 'error'){
                    $error = $log;
                    $syncStatus = 'failed';
                }

                if($key == 'info'){
                    $info = $log;
                    $syncStatus = 'processed';
                }
            }
        }

        switch($status){
            case 'processed':
            case 'not_processed':
                $processTime = 'process_end_time';
                break;
            default:
                $processTime = 'process_start_time';
        }

        DB::table('ap_zoho_invoice_for_xero_status_update')->where('id', $id)
            ->update([
                $processTime => DB::raw('CURRENT_TIMESTAMP'),
                'status' => $status,
                'sync_log' => $info,
                'sync_errors' => $error,
                'sync_status' => $syncStatus,
            ]);
    }
}
