<?php

namespace SOSZohoXeroIntegration\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use XeroPHP\Models\Accounting\Contact;
use XeroPHP\Models\Accounting\Invoice;
use XeroPHP\Application\PrivateApplication;

class GatherRecordFromZoho extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GatherZohoInvoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Invoices from Zoho and Load it in temporary table';

    /**
     * Queuing batch number
     * @var bool
     */
    protected $batchNumber = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \ZCRMRestClient::initialize();
        $lastRecordGatheringDate = DB::table('config')->where('category', 'Zoho Sync Config')->where('name','last_record_gathering_date')->first();

        $datetime = new \DateTime('now', new \DateTimeZone('Australia/ACT'));
        $currentTimeStamp =  $datetime->format(\DateTime::ATOM);

        // Initialize date for gathering records and exit since this is the initialization
        if(empty($lastRecordGatheringDate->value)) {
            $lastRecordGatheringDate = ['value' => $currentTimeStamp];
            DB::table('config')->insert(
                [
                    'category' => 'Zoho Sync Config',
                    'name' => 'last_record_gathering_date',
                    'value' => $currentTimeStamp,
                    'platform' => 'base',
                ]
            );
        }

        $batchNumber = DB::table('config')->where('category', 'Zoho Sync Config')->where('name','batch_number')->first();
        if(empty($batchNumber->value)){
            $queryRecentBatchId = DB::table('zoho_invoice_queue')->max('batch_number');
            $recentBatchId = !empty($queryRecentBatchId) ? $queryRecentBatchId : 0;
            $batchNumber = $recentBatchId + 1;
            DB::table('config')->insert(
                [
                    'category' => 'Zoho Sync Config',
                    'name' => 'batch_number',
                    'value' => $batchNumber,
                    'platform' => 'base',
                ]
            );
        }else{
            $batchNumber = $batchNumber->value + 1;
            // Update Batch Id Number in config
            DB::table('config')->where('category', 'Zoho Sync Config')->where('name', 'batch_number')->update(['value' => $batchNumber]);
        }

        $this->batchNumber = $batchNumber;
        $invoiceDateLimit = DB::table('config')->where('category', 'Zoho Sync Config')->where('name','invoice_date_limit')->first();

        try {
            $response = $this->fetchZohoInvoices(1, $lastRecordGatheringDate->value);
            $toSyncRecords = [];

            if(!empty($response['bulk_data_json']['data'])){
                foreach($response['bulk_data_json']['data'] as $data){
                    //Skips through empty, draft and requested from accounts invoices
                    if(empty($data["Status"]) || strtolower($data["Status"]) == "draft" || strtolower($data["Status"]) == "requested from accounts"){
                        continue;
                    }

                    //Skips through records whose invoice date is earlier than the designated date
                    if(!empty($invoiceDateLimit->value) && $data['Invoice_Date'] < $invoiceDateLimit->value){
                        continue;
                    }

                    $insert = $this->checkIfLoopUpdate($data['Invoice_No1'], $data['Modified_Time']);
                    if($insert){
                        array_push($toSyncRecords, $data);
                        DB::table('zoho_invoice_queue')->insert(
                            [
                                'invoice_type' => 'ACCREC',
                                'invoice_number' => !empty($data['Invoice_No1']) ? $data['Invoice_No1'] : '',
                                'batch_number' => $batchNumber,
                                'record_updated_date' => $data['Modified_Time'],
                                'record_details' => json_encode($data),
                                'status' => 'waiting_related_records'
                            ]
                        );
                    }
                }
            }

            while(!empty($response['more_records'])){
                $pageNumber = $response['page_number'] + 1;
                $response = $this->fetchZohoInvoices($pageNumber, $lastRecordGatheringDate->value);
                if(!empty($response['bulk_data_json']['data'])){
                    foreach($response['bulk_data_json']['data'] as $data){
                        //Skips through empty, draft and requested from accounts invoices
                        if(empty($data["Status"]) || strtolower($data["Status"]) == "draft" || strtolower($data["Status"]) == "requested from accounts"){
                            continue;
                        }

                        //Skips through records whose invoice date is earlier than the designated date
                        if(!empty($invoiceDateLimit->value) && $data['Invoice_Date'] < $invoiceDateLimit->value){
                            continue;
                        }

                        $insert = $this->checkIfLoopUpdate($data['Invoice_No1'], $data['Modified_Time']);
                        if($insert){
                            array_push($toSyncRecords, $data);
                            DB::table('zoho_invoice_queue')->insert(
                                [
                                    'invoice_type' => 'ACCREC',
                                    'invoice_number' => !empty($data['Invoice_No1']) ? $data['Invoice_No1'] : '',
                                    'batch_number' => $batchNumber,
                                    'record_updated_date' => $data['Modified_Time'],
                                    'record_details' => json_encode($data),
                                    'status' => 'waiting_related_records'
                                ]
                            );
                        }
                    }
                }
            }

        }catch (\Exception $e){
            if ($e->getMessage() == 'Undefined index: data') {
                return false;
            }
        }


        if(!empty($toSyncRecords)){
            $xero = new PrivateApplication(config('sync'));

            $this->fetchXeroInvoices($batchNumber, $xero);
            $this->fetchXeroContacts($batchNumber, $xero);
            $this->fetchZohoAccounts($batchNumber);
            $this->fetchZohoContacts($batchNumber);
            $this->fetchZohoProducts($batchNumber);

            // Now set the invoice to queue since related records is already fetched
            DB::table('zoho_invoice_queue')->where('batch_number', $batchNumber)->update(['status' => 'queue']);
        }

        DB::table('config')->where('category', 'Zoho Sync Config')->where('name', 'last_record_gathering_date')->update(['value' => $currentTimeStamp]);

        return true;
    }

    private function fetchZohoInvoices($pageNo, $lastRecordGatheringDate){
        $response = [];

        try{
            $zohoModuleIns = \ZCRMModule::getInstance('Invoices');
            $bulkAPIResponse=$zohoModuleIns->getRecords(null, null, null, $pageNo, 1000, ['If-Modified-Since' => $lastRecordGatheringDate]);
            $bulkAPIJsonResponse = $bulkAPIResponse->getResponseJSON();

            $responseInfo = $bulkAPIResponse->getInfo();
            $response['more_records'] = $responseInfo->getMoreRecords();
            $response['page_number'] = $responseInfo->getPageNo();
            $response['bulk_data_json'] = $bulkAPIJsonResponse;

            return $response;
        }catch(\Exception $e){
            if ($e->getMessage() == 'Undefined index: data') {
                return $response;
            }
            DB::table('zoho_invoice_queue')
                ->where('batch_number', $this->batchNumber)
                ->update([
                    'status' => 'latest_updated_invoices_not_completely_retrieved',
                    'sync_fatal_errors' => 'Will not queue this record, retrieving of Zoho invoices failed, some of it are missing. Will try again later to gather complete recently updated zoho invoices'
                ]);
            exit();
        }
    }

    /**
     * Prevent loop update
     * Don't insert in queue table if the update was triggered by syncapp because it will loop.
     * Queue record update only if it was triggered by the zoho application
     *
     * @param $invoiceNumber
     * @param $modifiedDate
     * @return bool
     * @throws \Exception
     */
    private function checkIfLoopUpdate($invoiceNumber, $modifiedDate){
        if(empty($invoiceNumber) || empty($modifiedDate)){
            return false;
        }

        $modifiedDt = new \DateTime($modifiedDate);
        $modifiedDt->setTimezone(new \DateTimeZone("UTC"));
        $modifiedDateInUtc = $modifiedDt->format("Y-m-d H:i:s");

        $sameInvoiceNumberInRecentSyncappUpdatesTable = DB::table('zoho_invoice_update_timestamps')->select('updated_date')->where('invoice_number', $invoiceNumber)->whereRaw("Date(updated_date) = CURDATE()")->get()->toArray();

        foreach($sameInvoiceNumberInRecentSyncappUpdatesTable as $syncappZohoRecordUpdateTimeStamp){
            $zohoUpdateTimeStampDt = new \DateTime($syncappZohoRecordUpdateTimeStamp->updated_date);

            // Define time range
            $zohoUpdateTimeStampDt->add(new \DateInterval('PT2M'));
            $zohoUpdateTimetStamPlus2Mins = $zohoUpdateTimeStampDt->format("Y-m-d H:i:s");
            $zohoUpdateTimeStampDt->sub(new \DateInterval('PT3M'));
            $zohoUpdateTimetStamMinus1Min = $zohoUpdateTimeStampDt->format("Y-m-d H:i:s");

            // Check if updated date is in between the recent update minus 1 minute and recent update plus 2minutes
            if (($zohoUpdateTimetStamMinus1Min <= $modifiedDateInUtc) && ($zohoUpdateTimetStamPlus2Mins >= $modifiedDateInUtc  )){
                return false;
            }
        }

        return true;
    }


    private function fetchXeroInvoices($batchNumber, $xero){
        try {
            $xeroInvoices = $xero->load(Invoice::class)->execute();
        } catch (\Exception $e) {
            $this->failedRetrievingRelatedRecords('Xero Invoices', $e->getMessage());
        }

        foreach ($xeroInvoices as $record) {
            $data = $this->parseXeroData($record);
            if(
                (!empty($data->Status) && strtolower($data->Status) == 'voided')
                || (!empty($data->Status) && strtolower($data->Status) == 'deleted')
                || (!empty($data->Xero_Invoice_Status) &&  strtolower($data->Xero_Invoice_Status) == 'voided')
                || (!empty($data->Xero_Invoice_Status) &&  strtolower($data->Xero_Invoice_Status) == 'deleted')
            ){
                continue;
            }

            //if($data->Type == 'ACCPAY'){
            //    continue;
            //}

            DB::table('zoho_to_xero_xero_invoices')->insert(
                [
                    'invoice_id' => !empty($data->InvoiceID) ? $data->InvoiceID : '',
                    'invoice_number' => !empty($data->InvoiceNumber) ? $data->InvoiceNumber : '',
                    'batch_number' => $batchNumber,
                ]
            );
        }
    }


    private function fetchXeroContacts($batchNumber, $xero){
        try {
            $xeroContacts = $xero->load(Contact::class)->execute();
        } catch (\Exception $e) {
            $this->failedRetrievingRelatedRecords('Xero Contacts', $e->getMessage());
        }

        foreach ($xeroContacts as $record) {
            $data = $this->parseXeroData($record);

            DB::table('zoho_to_xero_xero_contacts')->insert(
                [
                    'contact_id' => !empty($data->ContactID) ? $data->ContactID : '',
                    'email_address' => !empty($data->EmailAddress) ? $data->EmailAddress : '',
                    'contact_name' => !empty($data->Name) ? $data->Name : '',
                    'batch_number' => $batchNumber,
                ]
            );
        }
    }


    private function fetchZohoAccounts($batchNumber){
        $response = $this->fetchZohoRecords('Accounts', 1);

        if(!empty($response['bulk_data_json']['data'])){
            foreach($response['bulk_data_json']['data'] as $data){
                DB::table('zoho_to_xero_zoho_accounts')->insert(
                    [
                        'account_id' => $data['id'],
                        'batch_number' => $batchNumber,
                        'record_details' => json_encode($data),
                    ]
                );
            }
        }

        while(!empty($response['more_records'])){
            $pageNumber = $response['page_number'] + 1;
            $response = $this->fetchZohoRecords('Accounts', $pageNumber);
            if(!empty($response['bulk_data_json']['data'])){
                foreach($response['bulk_data_json']['data'] as $data){
                    DB::table('zoho_to_xero_zoho_accounts')->insert(
                        [
                            'account_id' => $data['id'],
                            'batch_number' => $batchNumber,
                            'record_details' => json_encode($data),
                        ]
                    );
                }
            }
        }
    }


    private function fetchZohoProducts($batchNumber){
        $response = $this->fetchZohoRecords('Products', 1);

        if(!empty($response['bulk_data_json']['data'])){
            foreach($response['bulk_data_json']['data'] as $data){
                DB::table('zoho_to_xero_zoho_products')->insert(
                    [
                        'product_id' => $data['id'],
                        'batch_number' => $batchNumber,
                        'record_details' => json_encode($data),
                    ]
                );
            }
        }

        while(!empty($response['more_records'])){
            $pageNumber = $response['page_number'] + 1;
            $response = $this->fetchZohoRecords('Products', $pageNumber);
            if(!empty($response['bulk_data_json']['data'])){
                foreach($response['bulk_data_json']['data'] as $data){
                    DB::table('zoho_to_xero_zoho_products')->insert(
                        [
                            'product_id' => $data['id'],
                            'batch_number' => $batchNumber,
                            'record_details' => json_encode($data),
                        ]
                    );
                }
            }
        }
    }


    private function fetchZohoContacts($batchNumber){
        $response = $this->fetchZohoRecords('Contacts', 1);

        if(!empty($response['bulk_data_json']['data'])){
            foreach($response['bulk_data_json']['data'] as $data){
                DB::table('zoho_to_xero_zoho_contacts')->insert(
                    [
                        'contact_id' => $data['id'],
                        'email_address' => !empty($data['Email']) ? $data['Email'] : '',
                        'batch_number' => $batchNumber,
                        'record_details' => json_encode($data),
                    ]
                );
            }
        }

        while(!empty($response['more_records'])){
            $pageNumber = $response['page_number'] + 1;
            $response = $this->fetchZohoRecords('Contacts', $pageNumber);
            if(!empty($response['bulk_data_json']['data'])){
                foreach($response['bulk_data_json']['data'] as $data){
                    DB::table('zoho_to_xero_zoho_contacts')->insert(
                        [
                            'contact_id' => $data['id'],
                            'email_address' => !empty($data['Email']) ? $data['Email'] : '',
                            'batch_number' => $batchNumber,
                            'record_details' => json_encode($data),
                        ]
                    );
                }
            }
        }
    }


    private function fetchZohoRecords($module, $pageNo){
        $response = [];

        try{
            $zohoModuleIns = \ZCRMModule::getInstance($module);
            $bulkAPIResponse=$zohoModuleIns->getRecords(null, null, null, $pageNo, 1000, null);
            $bulkAPIJsonResponse = $bulkAPIResponse->getResponseJSON();

            $responseInfo = $bulkAPIResponse->getInfo();
            $response['more_records'] = $responseInfo->getMoreRecords();
            $response['page_number'] = $responseInfo->getPageNo();
            $response['bulk_data_json'] = $bulkAPIJsonResponse;

            return $response;
        }catch(\Exception $e){
            if ($e->getMessage() == 'Undefined index: data') {
                return $response;
            }
            $this->failedRetrievingRelatedRecords('Zoho '. $module, $e->getMessage());
        }
    }


    private function failedRetrievingRelatedRecords($module, $error){
        DB::table('zoho_invoice_queue')
            ->where('batch_number', $this->batchNumber)
            ->update([
                "status" => "related_records_was_not_retrieved",
                "sync_fatal_errors" => "Will not queue this record, retrieving of $module failed: $error. Will try again later to gather and queue latest updated zoho invoices"
        ]);
        exit();

    }

    public function parseXeroData($record){
        $jsonEncodedData = json_encode($record);
        return json_decode($jsonEncodedData);
    }
}
