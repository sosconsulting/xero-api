<?php

namespace SOSZohoXeroIntegration\Console\Commands;

use Illuminate\Console\Command;
use SOSZohoXeroIntegration\SyncHelpers\SOSConsultingSubscription;
use Illuminate\Support\Facades\DB;

class SubscriptionValidate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Subscription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Validates if the customer is still subscribing to the integration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $customerId = config('sync.zoho.subscription_customer_id');
        $subscriptionApi = new SOSConsultingSubscription();
        $subscriptions = $subscriptionApi->request("subscriptions?customer_id={$customerId}&filter_by=SubscriptionStatus.All",'GET');

        $basic = false;
        $productMapping = false;
        $countryMapping = false;
        $companyAttributes = false;

        if(!empty($subscriptions['subscriptions'])){
            foreach($subscriptions['subscriptions'] as $subscription){
                switch ($subscription['name']){
                    case 'Xero and Zoho Integration-Basic':
                        $basic = $subscription['status'] == 'live' ? true : false;
                        break;
                    case 'Xero and Zoho Integration-Product Mapping':
                        $productMapping = $subscription['status'] == 'live' ? true : false;
                        break;
                    case 'Xero and Zoho Integration-Country Mapping':
                        $countryMapping = $subscription['status'] == 'live' ? true : false;
                        break;
                    case 'Xero and Zoho Integration-Company Attributes':
                        $companyAttributes = $subscription['status'] == 'live' ? true : false;
                        break;
                }
            }
        }

        $subscriptionStatus = [
            'basic' => $basic,
            'product_mapping' => $productMapping,
            'country_mapping' => $countryMapping,
            'company_attributes' => $companyAttributes,
        ];

        foreach($subscriptionStatus as $subscription => $value){
            $subscriptionConfig = DB::table('config')->where('category', 'Subscription')->where('name', $subscription)->first();
            if (isset($subscriptionConfig->value)) {
                DB::table('config')->where('category', 'Subscription')->where('name', $subscription)->update(['value' => $value]);
            } else {
                DB::table('config')->insert(
                    [
                        'category' => 'Subscription',
                        'name' => $subscription,
                        'value' => $value,
                        'platform' => 'base',
                    ]
                );
            }
        }

        return true;
    }
}
