<?php

namespace SOSZohoXeroIntegration\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PermanentlyDeleteInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'PermanentlyDeleteInvoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Permanently Delete Invoices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('xero_invoice_queue')->where('deleted', '=', 1)->whereRaw('created_at < DATE_SUB(NOW(), INTERVAL 2 DAY)')->delete();
        DB::table('zoho_invoice_queue')->where('deleted', '=', 1)->whereRaw('created_at < DATE_SUB(NOW(), INTERVAL 2 DAY)')->delete();
        DB::table('ap_zoho_invoice_for_xero_status_update')->where('deleted', '=', 1)->whereRaw('created_at < DATE_SUB(NOW(), INTERVAL 2 DAY)')->delete();

        // This maybe needed to be disabled when bulk syncing
        // Delete invoices that were created 3 days ago and still not deleted after 2 days.
        // Basically syncing will only last for 1-2 days
        DB::table('xero_invoice_queue')->whereRaw('created_at < DATE_SUB(NOW(), INTERVAL 3 DAY)')->delete();
        DB::table('zoho_invoice_queue')->whereRaw('created_at < DATE_SUB(NOW(), INTERVAL 3 DAY)')->delete();
        DB::table('ap_zoho_invoice_for_xero_status_update')->whereRaw('created_at < DATE_SUB(NOW(), INTERVAL 3 DAY)')->delete();


        // Delete also invoices that are missing related records
        $this->deleteInvoicesWithMissingRelatedRecords();

        return true;
    }

    private function deleteInvoicesWithMissingRelatedRecords(){
        DB::table('zoho_invoice_queue')->where('deleted', '=', 1)->where('status', 'latest_updated_invoices_not_completely_retrieved')->delete();
        DB::table('zoho_invoice_queue')->where('deleted', '=', 1)->where('status', 'related_records_was_not_retrieved')->delete();
        DB::table('xero_invoice_queue')->where('deleted', '=', 1)->where('status', 'latest_updated_invoices_not_completely_retrieved')->delete();
        DB::table('xero_invoice_queue')->where('deleted', '=', 1)->where('status', 'related_records_was_not_retrieved')->delete();
    }
}
