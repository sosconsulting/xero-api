<?php

namespace SOSZohoXeroIntegration;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $fillable = [
        'abbreviation',
        'state',
        'country'
    ];
}
