<?php
require_once 'vendor/autoload.php';
use SOSXeroZohoSyncApp\Sync;

$sync = new Sync();
$sync->execute();
