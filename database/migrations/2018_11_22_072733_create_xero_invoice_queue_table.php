<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateXeroInvoiceQueueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xero_invoice_queue', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('batch_number')->nullable();
            $table->string('invoice_type')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('record_updated_date')->nullable();
            $table->dateTime('process_start_time')->nullable();
            $table->dateTime('process_end_time')->nullable();
            $table->longText('record_details');
            $table->tinyInteger('deleted')->default(0);
            $table->string('status')->default('queue');
            $table->string('sync_status')->nullable();
            $table->longText('sync_fatal_errors')->nullable();
            $table->longText('sync_errors')->nullable();
            $table->longText('sync_log')->nullable();
            $table->tinyInteger('synced_today')->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('ap_vendor_id')->nullable();
            $table->string('ap_contact_id')->nullable();
            $table->string('ap_po_receipt_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_queue');
    }
}
