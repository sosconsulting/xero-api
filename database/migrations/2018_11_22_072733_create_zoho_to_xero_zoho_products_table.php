<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateZohoToXeroZohoProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zoho_to_xero_zoho_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('batch_number')->nullable();
            $table->string('product_id')->nullable();
            $table->longText('record_details');
            $table->tinyInteger('deleted')->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->index(['product_id', 'batch_number'], 'zoho_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zoho_to_xero_zoho_products');
    }
}
