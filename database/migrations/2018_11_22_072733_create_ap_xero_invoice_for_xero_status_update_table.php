<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateAPXeroInvoiceForXeroStatusUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ap_xero_invoices_for_xero_status_update', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('batch_number')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('invoice_type')->nullable();
            $table->string('invoice_status')->nullable();
            $table->longText('record_details');
            $table->tinyInteger('deleted')->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ap_xero_invoices_for_xero_status_update');
    }
}
