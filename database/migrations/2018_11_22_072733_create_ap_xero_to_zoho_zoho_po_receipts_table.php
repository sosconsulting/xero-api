<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateAPXeroToZohoZohoPOReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ap_xero_to_zoho_zoho_po_receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('batch_number')->nullable();
            $table->string('po_receipts_id')->nullable();
            $table->string('invoice_number')->nullable();
            $table->longText('record_details');
            $table->tinyInteger('deleted')->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->index(['invoice_number'], 'zoho_po_receipts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ap_xero_to_zoho_zoho_po_receipts');
    }
}
