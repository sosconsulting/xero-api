<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateXeroToZohoTaxRateMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xero_to_zoho_tax_rate_mapping', function (Blueprint $table) {
            $table->string('xero_tax_code')->nullable();
            $table->string('xero_tax_name')->nullable();
            $table->string('zoho_tax_name')->nullable();
            $table->float('zoho_tax_value')->nullable();
            $table->index(['xero_tax_code', 'zoho_tax_name'], 'xero_to_zoho_tax_rate_mapping');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xero_to_zoho_tax_rate_mapping');
    }
}
