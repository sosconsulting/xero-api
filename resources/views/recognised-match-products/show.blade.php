@extends('layouts.app')
@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card uper">
                    <div class="card-header">
                        <div class="pull-left">
                            <h2>Product Details</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('recognised-match-products.index') }}"><i class="fas fa-chevron-left"></i> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Product Name:</strong>
                                {{ $product->product_name }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Zoho Product ID:</strong>
                                {{ $product->zoho_product_id }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection