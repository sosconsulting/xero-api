@extends('layouts.app')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card uper">
                    <div class="card-header card-header">
                        <h2>Edit Product</h2>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('recognised-match-products.index') }}"><i class="fas fa-chevron-left"></i> Back</a>
                        </div>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                        <form method="POST" action="{{ route('recognised-match-products.update', $product->id) }}">
                            @method('PUT')
                            @csrf
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="product_name"><strong>Zoho Product ID:</strong></label>
                                        <input type="text" name="product_name" class="form-control" value="{{ old('product_name', $product->product_name) }}"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="zoho_product_id"><strong>Zoho Product ID:</strong></label>
                                        <input type="text" class="form-control" name="zoho_product_id" value="{{ old('zoho_product_id', $product->zoho_product_id) }}" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection