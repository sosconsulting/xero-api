@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card border-info mb-3">
                <div class="card-header text-primary font-weight-bold"><i class="fas fa-cog"></i> App Configuration</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="card-group">
                        <div class="card">
                            <div class="card-body">
                                <a class="btn btn-outline-primary btn-block" href="{{ route('recognised-match-products.index') }}"><i class="fas fa-box-open"></i> Recognised Match Products</a>
                                <p class="card-text font-weight-light"><small class="text-muted">* Configure Recognised Products.<br> * Use this config to add, modify and delete a product.</small></p>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <a class="btn btn-outline-primary btn-block" href="{{ route('single-word-products.index') }}"><i class="far fa-file-word"></i> Single Word Product Mapping</a>
                                <p class="card-text"><small class="text-muted">* Configure Single Word Mapping for Products.<br> * Use this config to add, modify and delete a product.</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="card-group">
                        <div class="card">
                            <div class="card-body">
                                <a class="btn btn-outline-primary btn-block" href="{{ route('countries.index') }}"><i class="far fa-flag"></i> Country Mapping</a>
                                <p class="card-text"><small class="text-muted">* Configure Country Mapping.<br> * Use this config to add, modify and delete a country.</small></p>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <a class="btn btn-outline-primary btn-block" href="{{ route('states.index') }}"><i class="fas fa-flag-usa"></i> State Mapping</a>
                                <p class="card-text"><small class="text-muted">* Configure State Mapping.<br> * Use this config to add, modify and delete a state.</small></p>
                            </div>
                        </div>
                    </div>
                        <div class="card-group">
                            <div class="card">
                                <div class="card-body">
                                    <a class="btn btn-outline-primary btn-block" href="{{ route('company-attributes.index') }}"><i class="far fa-building"></i> Company Attributes Mapping</a>
                                    <p class="card-text"><small class="text-muted">* Configure Company Attributes.<br> * Use this config to add, modify and delete a company attribute.</small></p>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    {{--r--}}
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
