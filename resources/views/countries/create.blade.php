@extends('layouts.app')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card uper">
                    <div class="card-header">
                        <h2>Add Country</h2>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('countries.index') }}"><i class="fas fa-chevron-left"></i> Back</a>
                        </div>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                        <form method="post" action="{{ route('countries.store') }}">
                            @csrf
                            <div class="form-group">
                                <label for="country">Country:</label>
                                <input type="text" class="form-control" name="country"  value="{{ old('country') }}"/>
                            </div>
                            <div class="form-group">
                                <label for="region">Region:</label>
                                <input type="text" class="form-control" name="region" value="{{ old('region') }}"/>
                            </div>
                            <div class="form-group">
                                <label for="abbreviation">Abbreviation:</label>
                                <input type="text" class="form-control" name="abbreviation" value="{{ old('abbreviation') }}"/>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection