@extends('layouts.app')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="uper">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                        <br />
                    @endif
                    <h2>Countries Mapping</h2>
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('countries.create') }}"><i class="fas fa-plus-circle"></i> Add New Country</a>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-hover table-light ">
                            <thead class="thead-dark">
                            <th>ID</th>
                            <th>Country</th>
                            <th>Region</th>
                            <th>Abbreviation</th>
                            <th colspan="2">Action</th>
                            </thead>
                            <tbody>
                            @if (count($countries) === 0)
                                <tr>
                                    <td colspan="5">
                                        <div class="alert alert-warning text-center" role="alert">
                                            No Records Available
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            @foreach($countries as $country)
                                <tr>
                                    <td>{{$country->id}}</td>
                                    <td>{{$country->country}}</td>
                                    <td>{{$country->region}}</td>
                                    <td>{{$country->abbreviation}}</td>
                                    <td>
                                        <form action="{{ route('countries.destroy',$country->id) }}" method="POST">
                                            <a class="btn btn-info" href="{{ route('countries.show',$country->id) }}"><i class="far fa-eye"></i></a>
                                            <a class="btn btn-primary" href="{{ route('countries.edit',$country->id) }}"><i class="far fa-edit"></i></a>
                                            @csrf
                                            @method('DELETE')
                                            <button onclick="return confirm('Are you sure you want to delete this item?')" type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {!! $countries->links() !!}
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('home') }}"><i class="fas fa-home"></i> Home</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection