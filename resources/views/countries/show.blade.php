@extends('layouts.app')
@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card uper">
                    <div class="card-header">
                        <div class="pull-left">
                            <h2>Country Details</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('countries.index') }}"><i class="fas fa-chevron-left"></i> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Country:</strong>
                                {{ $country->country }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Region:</strong>
                                {{ $country->region }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Abbreviation:</strong>
                                {{ $country->abbreviation }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection