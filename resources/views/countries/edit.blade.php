@extends('layouts.app')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card uper">
                    <div class="card-header card-header">
                        <h2>Edit Country Mapping</h2>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('countries.index') }}"><i class="fas fa-chevron-left"></i> Back</a>
                        </div>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                        <form method="POST" action="{{ route('countries.update', $country->id) }}">
                            @method('PUT')
                            @csrf
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="country"><strong>Country:</strong></label>
                                        <input type="text" name="country" class="form-control" value="{{  old('country', $country->country) }}"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="region"><strong>Region:</strong></label>
                                        <input type="text" class="form-control" name="region" value="{{ old('region', $country->region) }}"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="abbreviation"><strong>Abbreviation:</strong></label>
                                        <input type="text" class="form-control" name="abbreviation" value="{{ old('abbreviation', $country->abbreviation) }}"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection