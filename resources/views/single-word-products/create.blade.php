@extends('layouts.app')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card uper">
                    <div class="card-header">
                        <h2>Add Product for Single Word Mapping</h2>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('single-word-products.index') }}"><i class="fas fa-chevron-left"></i> Back</a>
                        </div>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                        <form method="post" action="{{ route('single-word-products.store') }}">
                            @csrf
                            <div class="form-group">
                                <label for="mapping_word">Mapping Word:</label>
                                <input type="text" class="form-control" name="mapping_word" value="{{ old('mapping_word') }}"/>
                            </div>
                            <div class="form-group">
                                <label for="zoho_product_id">Zoho Product ID:</label>
                                <input type="text" class="form-control" name="zoho_product_id" value="{{ old('zoho_product_id') }}"/>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection