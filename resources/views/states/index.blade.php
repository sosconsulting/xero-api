@extends('layouts.app')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
        td {
            vertical-align: middle;
        }
    </style>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="uper">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                        <br />
                    @endif
                    <h2>States Mapping</h2>
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('states.create') }}"><i class="fas fa-plus-circle"></i> Add New State</a>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-hover table-light ">
                            <thead class="thead-dark">
                            <th>ID</th>
                            <th>Abbreviation</th>
                            <th>State</th>
                            <th>Country</th>
                            <th colspan="2">Action</th>
                            </thead>
                            <tbody>
                            @if (count($states) === 0)
                                <tr>
                                    <td colspan="5">
                                        <div class="alert alert-warning text-center" role="alert">
                                            No Records Available
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            @foreach($states as $state)
                                <tr>
                                    <td>{{$state->id}}</td>
                                    <td>{{$state->abbreviation}}</td>
                                    <td>{{$state->state}}</td>
                                    <td>{{$state->country}}</td>
                                    <td>
                                        <form action="{{ route('states.destroy',$state->id) }}" method="POST">
                                            <a class="btn btn-info" href="{{ route('states.show',$state->id) }}"><i class="far fa-eye"></i></a>
                                            <a class="btn btn-primary" href="{{ route('states.edit',$state->id) }}"><i class="far fa-edit"></i></a>
                                            @csrf
                                            @method('DELETE')
                                            <button onclick="return confirm('Are you sure you want to delete this item?')" type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {!! $states->links() !!}
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('home') }}"><i class="fas fa-home"></i> Home</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection