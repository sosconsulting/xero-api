@component('mail::message')
    Hi <b> {{$name}}</b>,
    <br/>
    <br/>
    Attached is the Xero to Zoho Sync Report!
@endcomponent