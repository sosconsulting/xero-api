@component('mail::message')
    Hi <b> {{$name}}</b>,
    <br/>
    <br/>
    Attached is the Zoho to Xero Sync Report!
@endcomponent