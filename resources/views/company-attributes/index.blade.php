@extends('layouts.app')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="uper">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                        <br />
                    @endif
                    <h2>Company Attributes</h2>
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('company-attributes.create') }}"><i class="fas fa-plus-circle"></i> Add New Company Attribute</a>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-hover table-light ">
                            <thead class="thead-dark">
                            <th>ID</th>
                            <th>Keyword</th>
                            <th>Industry</th>
                            <th>Channel</th>
                            <th>Category</th>
                            <th colspan="2">Action</th>
                            </thead>
                            <tbody>
                            @if (count($attributes) === 0)
                                <tr>
                                    <td colspan="6">
                                        <div class="alert alert-warning text-center" role="alert">
                                            No Records Available
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            @foreach($attributes as $attribute)
                                <tr>
                                    <td>{{$attribute->id}}</td>
                                    <td>{{$attribute->keyword}}</td>
                                    <td>{{$attribute->industry}}</td>
                                    <td>{{$attribute->channel}}</td>
                                    <td>{{$attribute->category}}</td>
                                    <td>
                                        <form action="{{ route('company-attributes.destroy',$attribute->id) }}" method="POST">
                                            <a class="btn btn-info" href="{{ route('company-attributes.show',$attribute->id) }}"><i class="far fa-eye"></i></a>
                                            <a class="btn btn-primary" href="{{ route('company-attributes.edit',$attribute->id) }}"><i class="far fa-edit"></i></a>
                                            @csrf
                                            @method('DELETE')
                                            <button onclick="return confirm('Are you sure you want to delete this item?')" type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {!! $attributes->links() !!}
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('home') }}"><i class="fas fa-home"></i> Home</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection