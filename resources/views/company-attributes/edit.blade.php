@extends('layouts.app')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card uper">
                    <div class="card-header card-header">
                        <h2>Edit Company Attribute</h2>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('company-attributes.index') }}"><i class="fas fa-chevron-left"></i> Back</a>
                        </div>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                        <form method="POST" action="{{ route('company-attributes.update', $attribute->id) }}">
                            @method('PUT')
                            @csrf
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="keyword"><strong>Keyowrd:</strong></label>
                                        <input type="text" name="keyword"  class="form-control" value="{{ old('keyword',  $attribute->keyword) }}"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="industry"><strong>Industry:</strong></label>
                                        <input type="text" class="form-control" name="industry" value="{{ old('industry', $attribute->industry) }}"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="channel"><strong>Channel:</strong></label>
                                        <input type="text" class="form-control" name="channel" value="{{  old('channel', $attribute->channel) }}"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="category"><strong>Category:</strong></label>
                                        <input type="text" class="form-control" name="category" value="{{  old('category', $attribute->category) }}"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection