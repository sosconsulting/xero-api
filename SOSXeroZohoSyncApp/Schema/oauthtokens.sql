SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for oauthtokens
-- ----------------------------
DROP TABLE IF EXISTS `oauthtokens`;
CREATE TABLE `oauthtokens` (
  `useridentifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accesstoken` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `refreshtoken` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expirytime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
