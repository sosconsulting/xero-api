<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});

Auth::routes(['register' => false]);
Route::get('changepassword','Auth\ChangePasswordController@showChangePasswordForm')->name('changepassword');
Route::post('changepassword','Auth\ChangePasswordController@changePassword');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/zohoGrantToken', 'ZohoGrantToken@index')->name('zohoGrantToken');
Route::resource('recognised-match-products', 'RecognisedMatchProductController')->middleware('auth');
Route::resource('single-word-products', 'SingleWordProductController')->middleware('auth');
Route::resource('countries', 'CountryController')->middleware('auth');
Route::resource('states', 'StateController')->middleware('auth');
Route::resource('company-attributes', 'CompanyAttributeController')->middleware('auth');
